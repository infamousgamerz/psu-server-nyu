﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetworkStreamInterceptor
{
    public partial class formEditFilterItem : Form
    {
        formMain mainForm;

        public formEditFilterItem(formMain mainForm, string inSelectedItemKey)
        {
            this.mainForm = mainForm;
            InitializeComponent();
            this.textBoxCommand.Text = inSelectedItemKey;
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            mainForm.Logging.packetFilter.Remove(textBoxCommand.Text);
            if ((this.textBoxCommand.Text != string.Empty) && (this.textBoxCommandDescription.Text != string.Empty))
            {
                // Add new item
                try
                {
                    mainForm.Logging.packetFilter.Add(textBoxCommand.Text.ToUpper(), textBoxCommandDescription.Text);
                }
                catch
                {
                    Console.WriteLine("Item already exists");
                }
            }
            mainForm.updateListView();
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            mainForm.updateListView();
            this.Close();
        }

        private void formEditFilterItem_Load(object sender, EventArgs e)
        {
            textBoxCommandDescription.Text = mainForm.Logging.packetFilter[textBoxCommand.Text].ToString();
        }
    }
}
