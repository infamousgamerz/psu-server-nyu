﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Xml;
using System.Threading;

namespace NetworkStreamInterceptor
{
    public class Logger :IDisposable
    {
        // Current Log Level
        public LogLevelEnum OutputLogLevel = LogLevelEnum.Everything;
        public bool showPacketContents = false;
        public bool PacketContents { get { return showPacketContents; } set { this.showPacketContents = value; } }
        public FileStream traceListenerFile;
        public TextWriterTraceListener traceListenerConsole;

        // Hashtable for filtering
        public Hashtable packetFilter;

        // Public Methods
        public Logger(LogLevelEnum inLogLevel)
        {
            // Initialize Logger with specified LogLevel
            this.OutputLogLevel = inLogLevel;
            showPacketContents = false;

            traceListenerFile = new FileStream("log--" + DateTime.Now.ToString("yyyy-MM-dd--HH-mm-ss") + ".log", FileMode.OpenOrCreate);
            traceListenerConsole = new TextWriterTraceListener(Console.Out);
            Trace.Listeners.Add(traceListenerConsole);
            Trace.Listeners.Add(new TextWriterTraceListener(traceListenerFile));

            Trace.AutoFlush = true;


            // create new packetFilter hashtable
            packetFilter = new Hashtable();

            //serializationTest(); // used to create a simple file

            // Deserialize data
            FileStream fs = new FileStream("commandlist.soap", FileMode.OpenOrCreate);
            try // error handling :3
            {
                SoapFormatter formatter = new SoapFormatter();

                // deserialize data to packetFilter
                packetFilter = (Hashtable)formatter.Deserialize(fs);
            }
            catch (SerializationException e)
            {
                Trace.WriteLine("Failed to deserialize. Reason: " + e.Message);
                //throw;
            }
            catch (XmlException e)
            {
                Trace.WriteLine("Failed to deserialize. Reason: " + e.Message);
                //throw;
            }
            finally
            {
                fs.Close();
            }


        }

        // Used to create a sample SOAP file. Worked flawlessly :)
        private void serializationTest()
        {
            packetFilter.Add("00000000", "Null Packet");
            packetFilter.Add("02020300", "Server Hello - Login");
            packetFilter.Add("080E0218", "Client Hello - Login");
            packetFilter.Add("02190277", "Login Request");
            packetFilter.Add("02250308", "MOTD/License");
            packetFilter.Add("02160300", "Redirect to new Server");
            packetFilter.Add("02020308", "Server Hello - Game");
            packetFilter.Add("080E0277", "Client Hello and system identification - Game");

            FileStream fs = new FileStream("commandlist.soap", FileMode.OpenOrCreate);
            SoapFormatter formatter = new SoapFormatter();
            try
            {
                formatter.Serialize(fs, packetFilter);
            }
            catch (SerializationException e)
            {
                Trace.WriteLine("Failed to serialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }
        }

        public void saveFilter()
        {
            FileStream fs = new FileStream("commandlist.soap", FileMode.OpenOrCreate);
            SoapFormatter formatter = new SoapFormatter();
            try
            {
                formatter.Serialize(fs, packetFilter);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                fs.Flush();
                fs.Close();
            }
        }

        private Object thisLock = new Object();

        public void Add(object a)
        {
            lock (thisLock) // Lock Thread
            {
                LoggerThreadPoolParameters threadInfo = (LoggerThreadPoolParameters)a;

                //Thread.Sleep(100);
                if (true)//(Level <= OutputLogLevel)
                {
                    uint tempFunction = (uint)System.Net.IPAddress.HostToNetworkOrder(BitConverter.ToInt32(threadInfo.Details, 4));

                    // Check Hashtable for known values
                    if (packetFilter.ContainsKey(tempFunction.ToString("X8")))
                        outputHeader((uint)threadInfo.Length, tempFunction, packetFilter[tempFunction.ToString("X8")].ToString(), threadInfo.Direction);
                    else
                        outputHeader((uint)threadInfo.Length, tempFunction, threadInfo.Direction);

                    //if (Level == LogLevelEnum.Unimplemented)
                    /*if (inDirection == Interceptor.Direction.ToServer)
                        Console.WriteLine("Client: 0x{0:X8} - " + Message, tempFunction);
                    else
                        Console.WriteLine("Server: 0x{0:X8} - " + Message, tempFunction);*/
                    //else
                    //Console.WriteLine(Message); 

                    //if ((showPacketContents == true) && (inLength < 0x1000))
                    if (showPacketContents == true)
                    {
                        //outputFormattedOutput(threadInfo.Details, threadInfo.Length);
                    }
                }
                Trace.Flush();
                traceListenerFile.Flush();
            }

            // New Code
        }

        public void Add(string inText)
        {
            lock (thisLock)
            {
                // Output line to tracer
                Trace.WriteLine(inText);
                Trace.Flush();
                traceListenerFile.Flush();
            }
        }

        private void outputHeader(uint inLength, uint inFunction, string inFunctionDescription, Interceptor.Direction inDirection)
        {
            //if (inDirection == Interceptor.Direction.ToClient)
                //Trace.WriteLine(String.Format("Server -> Client, Command 0x{0:X8} {2}, Length {1:X4}b, {1} bytes", inFunction, inLength, inFunctionDescription));
            //else
                //Trace.WriteLine(String.Format("Client -> Server, Command 0x{0:X8} {2}, Length {1:X4}b, {1} bytes", inFunction, inLength, inFunctionDescription));
        }

        private void outputHeader(uint inLength, uint inFunction, Interceptor.Direction inDirection)
        {
            outputHeader(inLength, inFunction, "!Not Defined!", inDirection);
        }

        private void outputFormattedOutput(byte[] inBytes, int inLength)
        {
            return;

            // Prepare
            string tempString = "";
            for (int i = 0; i < inLength; i = i + 0x10) // Iterate each line
            {
                tempString = String.Format("{0:X8} | ", i);

                for (int j = i; j < (i + 16); j++)
                {
                    if (j < inLength)
                        tempString = String.Format(tempString + BitConverter.ToString(inBytes, j, 1) + " ");
                    else
                        tempString = String.Format(tempString + "   "); // Three spaces
                }

                tempString = String.Format("{0}| {1}" , tempString, cleanupOutputString(inBytes, i, inLength - i));
                Trace.WriteLine(tempString);
                tempString = "";
            }

            // Output
        }

        private string cleanupOutputString(byte[] inBytes, int inOffset, int inLength)
        {
            char[] tempChars = new char[16];
            int tempLength = 0;

            if (inLength > 16)
                tempLength = 16;
            else
                tempLength = inLength;

            tempChars = ASCIIEncoding.ASCII.GetChars(inBytes, inOffset, tempLength);

            for (int i = 0; (i < inLength) && (i < 16); i++)
            {
                if ((tempChars[i] < (char)0x20) || (tempChars[i] > (char)0x7E))
                    tempChars[i] = '.';
            }
            return new string(tempChars);
        }

        // Enum of logging levels






        #region IDisposable Members

        public void Dispose()
        {
            traceListenerFile.Close();
        }

        #endregion
    }

    public class LoggerThreadPoolParameters
    {
        // public void Add(string Message, byte[] Details, Interceptor.Direction inDirection, int inLength, LogLevelEnum Level)
        public string Message;
        public byte[] Details;
        public Interceptor.Direction Direction;
        public int Length;
        public LogLevelEnum Level;
    }

    public enum LogLevelEnum : int
    {
        Errors = 1,
        Status = 2,
        Chat = 3,
        Warnings = 4,
        Notes = 6,
        Unimplemented = 5,
        RawPacketStream = 99,
        Everything = 100
    }



        
}