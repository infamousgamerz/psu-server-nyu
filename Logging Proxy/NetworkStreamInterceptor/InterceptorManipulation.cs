﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
namespace NetworkStreamInterceptor
{
    partial class Interceptor
    {
        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        //public delegate void SendChatMessageDelegate(string inMessage);
        public void SendChatMessage(string inName, string inMessage, uint inGCNumber)
        {
            // Prepare Packet
            int padding = (inMessage.Length * 2) % 4;

            PacketBuilder packet = new PacketBuilder((uint)(0x84 + (inMessage.Length * 2) + 4 - padding), new byte[4] { 0x03, 0x04, 0x02, 0x00 });
            packet.WriteQuadByte(new byte[4] { 0x00, 0x00, 0x12, 0x00 }, 0x2C);
            packet.WriteUInt(inGCNumber, 0x30);

            // write name
            packet.WriteUnicodeString(inName, 0x44);

//            packet.WriteQuadByte(new byte[4] { 0xFF, 0xFE, 0x55, 0x55 }, (int)packet.PacketSize - 4);
            packet.WriteByte(0x00, 0x84 + inMessage.Length * 2);
            packet.WriteByte(0x00, 0x84 + inMessage.Length * 2+1);

            packet.WriteUnicodeString(inMessage, 0x84);

            packet.WriteQuadByte(new byte[4] { 0x00, 0x00, 0x00, 0x06 }, 0x3C);
            packet.WriteQuadByte(new byte[4] { 0x00, 0x00, 0x00, 0x16 }, 0x40);


            // do stuff
            try
            {
                serverSslStream.Write(packet.RawPacket);
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.Message);
            }
        }

        public void SendRawPacket(byte[] inPacket, uint inLength, Direction inDirection)
        {
            // Check for correct length, if incorrect just do nothing
            if (inLength < (uint)8) //|| ((inLength % (uint)2) == (uint)1)) // testing purposes onry
                return;

            Array.Copy(BitConverter.GetBytes(inLength), inPacket, 4);

            // Send out packet
            if (inDirection == Direction.ToServer)
                serverSslStream.Write(inPacket, 0, (int)inLength);
            else
                clientSslStream.Write(inPacket, 0, (int)inLength);

        }
    }
}
