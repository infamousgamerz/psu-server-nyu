using System;
using System.Text;
using System.IO;
using System.Net;
using System.Threading;
using System.Net.Sockets;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.AccessControl;
using System.Security.Authentication;
using System.Reflection;
using System.Diagnostics;

namespace NetworkStreamInterceptor
{
    public partial class Interceptor
    {
        // Client and Server Objects
        static private TcpListener tcpListener;
        // TODO: Client object
        static SslStream clientSslStream = null;
        static SslStream serverSslStream = null;
        static TcpClient tcpPSUServer;

        // Connect to HERE:
        static string PSUServerIP = "202.51.6.30";
        static int PSUServerPort = 12030;
        static byte clientLang = 0x00;  // Language setting - 0x00 JP - 0x01 EN - 0x03 FR - 0x04 DE

        //static string PSUServerIP = "188.165.56.119";
        //static int PSUServerPort = 12230; // psuus
        //static byte clientLang = 0x01;  // Language setting - 0x00 JP - 0x01 EN - 0x03 FR - 0x04 DE

        static byte[] localIP;


        // buffers
        static byte[] clientBuffer = new byte[0xFFFF];
        static byte[] serverBuffer = new byte[0xFFFF];

        static byte[] clientTempBuffer = new byte[0xFFFFFF];
        static uint clientTempBufferSize = 0;

        static byte[] serverTempBuffer = new byte[0xFFFFFF];
        static uint serverTempBufferSize = 0;

        // Certification Objects
        static X509Certificate serverCert = null;
        static X509Certificate clientCert = null;
        static X509CertificateCollection certificateCollection = new X509Certificate2Collection();


        // Binlogger
        private  Logging binaryLogger;

        public enum Direction
        {
            ToServer,
            ToClient
        }

        public nblDumper nblFileDumper;

        // Things for Threading
        static formMain Roots;


        // Initialize Interceptor class
        public Interceptor(formMain RootForm)
        {
            Roots = RootForm;

            // Binlogger
            binaryLogger = new Logging();

            nblFileDumper = new nblDumper();

            // Initialize tcpListener
            tcpListener = new TcpListener(IPAddress.Any, PSUServerPort);
            //TcpListener tcpListenerJPPC = new TcpListener(IPAddress.Any, 12030);
            //TcpListener tcpListenerUSPS2 = new TcpListener(IPAddress.Any, 12230);

            //localIP = Dns.GetHostAddresses(Dns.GetHostName())[2].GetAddressBytes();
            // Only allow loopback connections
            localIP = new byte[] { 127, 0, 0, 1 };

            Trace.WriteLine("++ Loading Server Certificate");
            serverCert = new X509Certificate2(NetworkStreamInterceptor.Properties.Resources.test, "alpha");

            Trace.WriteLine("++ Loading Client Certificate");
            clientCert = new X509Certificate2(NetworkStreamInterceptor.Properties.Resources.PSUClient, "alpha");
            certificateCollection.Add(clientCert);

            // Initialization done and starting thread
            Trace.WriteLine("Waiting for clients...");

            tcpListener.Start();
            tcpListener.BeginAcceptTcpClient(PSUClientBeginListeningCallback, tcpListener);
            //tcpListenerJPPC.Start();
            //tcpListenerJPPC.BeginAcceptTcpClient(PSUClientBeginListeningCallback, tcpListenerJPPC);
        }


        public void PSUClientBeginListeningCallback(IAsyncResult AsyncCall)
        {
            TcpListener tcpListener = (TcpListener)AsyncCall.AsyncState;
            // Set the event to nonsignaled state.
            TcpClient tcpPSUClient = tcpListener.EndAcceptTcpClient(AsyncCall);

            //tcpListener.Stop();

            Trace.WriteLine("++ PSUClient connected " + tcpListener.LocalEndpoint.ToString());

            // initialize clientSslStream
            clientSslStream = new SslStream(tcpPSUClient.GetStream(), false);
            clientSslStream.AuthenticateAsServer(serverCert, false, SslProtocols.Ssl3, false);
            // TODO: Broken in Mono, should get this bug fixed...it's vital to our success :(
            Trace.WriteLine("++ PSUClient successfully authenticated, begin listening");
            clientSslStream.BeginRead(clientBuffer, 0, 0xFFFF, new AsyncCallback(PSUClientReadCallback), clientSslStream);


            // establishing a connection to PSUServer
            Trace.WriteLine("++ Establishing connection to PSUServer");
            tcpPSUServer = new TcpClient(PSUServerIP, PSUServerPort);

            // SSL magic
            serverSslStream = new SslStream(tcpPSUServer.GetStream(), false, new RemoteCertificateValidationCallback(ValidateServerCertificate), new LocalCertificateSelectionCallback(SelectLocalCertificate));
            serverSslStream.AuthenticateAsClient("PSUServer", certificateCollection, SslProtocols.Ssl3, true);
            Trace.WriteLine("++ PSUServer successfully authenticated, begin listening");
            serverSslStream.BeginRead(serverBuffer, 0, 0xFFFF, new AsyncCallback(PSUServerReadCallback), serverSslStream);

            Trace.Flush();

            localIP = ((IPEndPoint)tcpPSUClient.Client.LocalEndPoint).Address.GetAddressBytes();

        }

        #region Read messages and pass along
        public void PSUClientReadCallback(IAsyncResult AsyncCall)
        {
            // Get the clientSslStream object
            SslStream clientSslStream = (SslStream)AsyncCall.AsyncState;

            // initializing needed things
            ASCIIEncoding encoder = new ASCIIEncoding();
            int bytesRead = 0;

            try
            {
                // set bytesRead
                bytesRead = clientSslStream.EndRead(AsyncCall);
                if (bytesRead != 0)
                {
                    Array.Copy(clientBuffer, 0, clientTempBuffer, clientTempBufferSize, bytesRead);
                    clientTempBufferSize += (uint)bytesRead;

                    if (clientTempBufferSize >= BitConverter.ToUInt32(clientTempBuffer, 0))
                    {
                        lockwait();
                        byte[] tempBuffer = ProcessPacket(clientTempBuffer, (int)clientTempBufferSize, Direction.ToServer);

                        // No packet should be only 1 byte long so...
                        if (tempBuffer.Length != 0)
                         serverSslStream.Write(tempBuffer);


                        clientTempBufferSize = 0;
                    }

                    // fetch more data :3
                    clientSslStream.BeginRead(clientBuffer, 0, 0xFFFF, new AsyncCallback(PSUClientReadCallback), clientSslStream);

                }
                else
                {
                    Trace.WriteLine("!! PSUClient disconnected");
                    serverSslStream.Close();
                    clientSslStream.Close();
                   // binaryLogger.Close();
                    tcpListener.Start();
                }
            }
            catch (Exception readException)
            {
                serverSslStream.Close();
                binaryLogger.Close();
                Exception e = readException;
                Trace.WriteLine("##### ERROR IN PSUClientReadCallback() ######");
                Trace.WriteLine(e);
                return;
            }
            tcpListener.BeginAcceptTcpClient(PSUClientBeginListeningCallback, tcpListener);

            //bool complete = true;
        }
        public void PSUServerReadCallback(IAsyncResult AsyncCall)
        {
            // Get the clientSslStream object
            SslStream serverSslStream = (SslStream)AsyncCall.AsyncState;

            // initializing needed things
            ASCIIEncoding encoder = new ASCIIEncoding();
            int bytesRead = 0;

            try
            {
                // set bytesRead
                bytesRead = serverSslStream.EndRead(AsyncCall);

                if (bytesRead != 0)
                {

                    Array.Copy(serverBuffer, 0, serverTempBuffer, serverTempBufferSize, bytesRead);
                    serverTempBufferSize += (uint)bytesRead;

                    if (serverTempBufferSize == BitConverter.ToUInt32(serverTempBuffer, 0))
                    {
                        lockwait();
                        byte[] tempBuffer = ProcessPacket(serverTempBuffer, (int)serverTempBufferSize, Direction.ToClient);

                        clientSslStream.Write(tempBuffer);

                        serverTempBufferSize = 0;
                    }

                    // fetch more data :3
                    serverSslStream.BeginRead(serverBuffer, 0, 0xFFFF, new AsyncCallback(PSUServerReadCallback), serverSslStream);

                }
                else
                {
                    Trace.WriteLine("!! PSUServer disconnected");
                    serverSslStream.Close();
                    tcpPSUServer.Close();
                    clientSslStream.Close();
                    binaryLogger.Close();
                }
            }
            catch (Exception readException)
            {
                Exception e = readException;
                Trace.WriteLine("##### ERROR IN PSUServerReadCallback() ######");
                Trace.WriteLine(e);
                return;
            }

            //bool complete = true;
        }
        #endregion





        public byte[] ProcessPacket(byte[] buffer, int length, Direction inDirection)
        {
            int processedSize = 0;
            byte[] temp = new byte[length];
            Array.Copy(buffer, temp, length);
            PacketBuilder packet;

            while (processedSize < length)
            {
                // parse recieved message packet by packet
                packet = new PacketBuilder(processedSize, temp);

                // Log output

                LoggerThreadPoolParameters threadInfo = new LoggerThreadPoolParameters();
                threadInfo.Message = "PACKET";
                threadInfo.Details = packet.RawPacket;
                threadInfo.Direction = inDirection;
                threadInfo.Length = (int)packet.PacketSize;
                threadInfo.Level = LogLevelEnum.Errors;
                ThreadPool.QueueUserWorkItem(new WaitCallback(Roots.Logging.Add), threadInfo);

                packet = ParsePacket(packet, inDirection);
                Array.Copy(packet.RawPacket, 0, temp, processedSize, packet.PacketSize);
                //Trace.WriteLine("bump");
                processedSize += (int)packet.PacketSize;
            }

            return temp;
        }

        private PacketBuilder ParsePacket(PacketBuilder packet, Direction inDirection)
        {
            return ParseMessage(packet, inDirection);
            // huge switch case for packet parsing :3

        }



        object waitlock = new object();

        private void lockwait()
        {
            lock (waitlock)
            {
                //Trace.WriteLine("HIT!");
                Thread.Sleep(1);
            }
        }



        public string GetTempPath()
        {
            string path =
               System.Environment.GetEnvironmentVariable("TEMP");
            if (!path.EndsWith("\\"))
            {
                path += "\\";
            }
            return path;
        }

        public void LogMessageToFile(string message)
        {
            System.IO.StreamWriter sw =
               System.IO.File.AppendText(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "Logfile.txt"); // Change filename
            try
            {
                string logLine =
                   System.String.Format(
                      "{0:G}: {1}.", System.DateTime.Now, message);
                sw.WriteLine(logLine);
            }
            finally
            {
                sw.Close();
                binaryLogger.Close();
            }
        }

        #region Certificate bullshit
        public static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        public static X509Certificate SelectLocalCertificate(object sender, string targetHost, X509CertificateCollection localCertificates, X509Certificate remoteCertificate, string[] acceptableIssuers)
        {
            Trace.WriteLine("Client is selecting a local certificate.");
            if (acceptableIssuers != null && acceptableIssuers.Length > 0 && localCertificates != null && localCertificates.Count > 0)
            {
                // Use the first certificate that is from an acceptable issuer.
                foreach (X509Certificate certificate in localCertificates)
                {
                    //string issuer = certificate.Issuer;
                    //if (Array.IndexOf(acceptableIssuers, issuer) != -1)
                    return certificate;
                }
            }
            if (localCertificates != null && localCertificates.Count > 0)
                return localCertificates[0];
            return null;
        }

        #endregion


    }
}
