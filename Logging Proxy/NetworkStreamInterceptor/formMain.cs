﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

namespace NetworkStreamInterceptor
{

    public partial class formMain : Form
    {
        // interceptor class
        public static Thread interceptorInstance;
        public static Interceptor interceptorClass;
        public Logger Logging;
        public uint GCNumber { get; set; }

        static void Main(string[] args)
        {

            // Opening Screen
            Trace.WriteLine("NeSI for Phantasy Star Universe - r0");

            // Enabling the bells and whistles
            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.Run(new NetworkStreamInterceptor.formMain());


        }

        public formMain()
        {
            InitializeComponent();

            Logging = new Logger(LogLevelEnum.Notes);

            // Create Logging Component

            interceptorInstance = new Thread(new ThreadStart( delegate() {
                try
                {
                    interceptorClass = new Interceptor(this);
                }
                catch //(ThreadAbortException AbortException)
                {
                    Trace.WriteLine("ABORTEDLULZ!");
                }
            }));
            interceptorInstance.Start();

            checkRawPackets_CheckedChanged(new object(), new EventArgs());
        }

        public void startInterceptorThread()
        {
            interceptorClass = new Interceptor(this);
        }
        
        public delegate void AddChatMessageDelegate(string Username, string Message);
        public void AddChatMessage(string Username, string Message)
        {
            if (this.textChatlog.InvokeRequired)
                this.textChatlog.Invoke(new AddChatMessageDelegate(this.AddChatMessage), new object[] { Username, Message });
            else
            {

                // Put together and update chatlog
                textChatlog.AppendText(Environment.NewLine + "<" + Username + "> " + Message);
            }
        }

        public delegate void SetGCNumberDelegate(uint inGCNumber);
        public void SetGCNumber(uint inGCNumber)
        {
            if (this.textChatlog.InvokeRequired)
                this.textChatlog.Invoke(new SetGCNumberDelegate(this.SetGCNumber), new object[] { inGCNumber });
            else
            {
                GCNumber = inGCNumber;
            }
        }

        public delegate void SetCharacterNameDelegate(string inCharacterName);
        public void SetCharacterName(string inCharacterName)
        {
            if (this.textChatlog.InvokeRequired)
                this.textChatlog.Invoke(new SetCharacterNameDelegate(this.SetCharacterName), new object[] { inCharacterName });
            else
            {
                textCharacterName.Text = inCharacterName;
            }
        }

        public delegate void SetLocationDelegate(string inLocationName);
        public void SetLocationName(string inLocationName)
        {
            if (this.textChatlog.InvokeRequired)
                this.textChatlog.Invoke(new SetLocationDelegate(this.SetLocationName), new object[] { inLocationName });
            else
            {
                textCurrentLocation.Text = inLocationName;
            }
        }

        private void formMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            interceptorInstance.Abort();
            interceptorInstance.Join();
            Application.Exit();
        }

        private void buttonSendChat_Click(object sender, EventArgs e)
        {
            interceptorClass.SendChatMessage(textCharacterName.Text, textChatMessage.Text, GCNumber);
            //textChatMessage.Text = "";
        }

        private void checkRawPackets_CheckedChanged(object sender, EventArgs e)
        {
            Logging.showPacketContents = checkRawPackets.Checked;
        }

        private void buttonPacketSend_Click(object sender, EventArgs e)
        {
            byte[] tempArray = Interceptor.StringToByteArray(textManualPacket.Text.Replace("-","").Replace(" ",""));
            interceptorClass.SendRawPacket(tempArray, (uint)tempArray.Length, Interceptor.Direction.ToServer);
        }

        private void buttonPacketSendToClient_Click(object sender, EventArgs e)
        {
            byte[] tempArray = Interceptor.StringToByteArray(textManualPacket.Text.Replace("-", "").Replace(" ", ""));
            interceptorClass.SendRawPacket(tempArray, (uint)tempArray.Length, Interceptor.Direction.ToClient);
        }

        // ListView Update Code
        public void updateListView()
        {
            listViewCommands.Items.Clear();

            foreach (DictionaryEntry entry in Logging.packetFilter)
            {
                ListViewItem tempItem = new ListViewItem(entry.Key.ToString());
                tempItem.SubItems.Add(entry.Value.ToString());
                listViewCommands.Items.Add(tempItem);
            }
        }

        private void buttonCommandAdd_Click(object sender, EventArgs e)
        {
            //updateListView();
            formAddFilterItem subForm = new formAddFilterItem(this);
            subForm.ShowDialog();
        }

        private void buttonButtonCommandRemove_Click(object sender, EventArgs e)
        {
            try
            {
                Logging.packetFilter.Remove(listViewCommands.SelectedItems[0].Text);
            }
            catch { }
        }

        private void formMain_Load(object sender, EventArgs e)
        {
            updateListView();
        }

        private void buttonCommandEdit_Click(object sender, EventArgs e)
        {
            try
            {
                formEditFilterItem subForm = new formEditFilterItem(this, listViewCommands.SelectedItems[0].Text);
                subForm.ShowDialog();
            }
            catch { }
        }

        private void formMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            interceptorClass.nblFileDumper.SafeFiles();
            Logging.saveFilter();
            Logging.Dispose();
        }


    }
}
