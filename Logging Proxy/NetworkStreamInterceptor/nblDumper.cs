﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Diagnostics;

namespace NetworkStreamInterceptor
{
    public class nblDumper
    {
        public static Dictionary<uint, nblFile> Files;

        public nblDumper()
        {
            Files = new Dictionary<uint, nblFile>();
        }

        public void RegisterFile(uint ID, int filesize)
        {
            if (!Files.ContainsKey(ID))
                // Create new item otherwise use the existing item
                Files.Add(ID, new nblFile(filesize));

        }
        public void AddFile(byte[] content, uint ID, int offset)
        {
            if (Files.ContainsKey(ID))
                Files[ID].AddContent(content, offset);
            
        }

        public void SafeFiles()
        {
            foreach (KeyValuePair<uint, nblFile> item in Files)
            {
                try
                {
                    using (FileStream stream = new FileStream("maps\\" + item.Value.KeySeed + ".nbl", FileMode.OpenOrCreate))
                    {
                        using (BinaryWriter writer = new BinaryWriter(stream))
                        {
                            writer.Write(item.Value.NBLFile);
                            writer.Flush();
                            writer.Close();
                        }
                    }
                }
                catch(Exception e)
                {
                    // TODO ERROR MESSAGE HERE
                    Trace.WriteLine("# Error writing map data to disk");
                    Trace.WriteLine(e);
                }

            }
        }
    }

    public struct nblFile
    {
        // Raw Bytes
        private byte[] content;
        public byte[] Content { get { return content; } }
        public byte[] NBLFile
        {
            get
            {
                byte[] temp = new byte[content.Length - 0x34];
                Array.Copy(content, 0x34, temp, 0, temp.Length);
                return temp;
            }
        }
        public string KeySeed { get { return BitConverter.ToUInt32(NBLFile, 0x1C).ToString("X8"); } }

        public int Size { get { return content.Length; } }

        public nblFile(int Size)
        {
            content = new byte[Size];
        }

        public void AddContent(byte[] inContent, int offset)
        {
            Array.Copy(inContent, 0, content, offset, inContent.Length);
        }
    }
}
