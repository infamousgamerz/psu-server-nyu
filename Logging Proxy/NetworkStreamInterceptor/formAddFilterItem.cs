﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetworkStreamInterceptor
{
    public partial class formAddFilterItem : Form
    {
        formMain mainForm;

        public formAddFilterItem(formMain mainForm)
        {
            this.mainForm = mainForm;
            InitializeComponent();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if ((this.textBoxCommand.Text != string.Empty) && (this.textBoxCommandDescription.Text != string.Empty))
            {
                // Add new item
                try
                {
                    mainForm.Logging.packetFilter.Add(textBoxCommand.Text.ToUpper(), textBoxCommandDescription.Text);
                }
                catch
                {
                    Console.WriteLine("Item already exists");
                }
            }
            mainForm.updateListView();
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            mainForm.updateListView();
            this.Close();
        }
    }
}
