﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetworkStreamInterceptor
{
    public struct PacketBuilder
    {
        // Raw Bytes
        private byte[] packet;
        public byte[] RawPacket { get { return packet; } }

        public byte[] Command { get { return new byte[4] { packet[4], packet[5], packet[6], packet[7] }; } }

        // Used for creating packets on the server side
        public PacketBuilder(uint PacketSize, byte[] Command)
        {
            // Set Size of new packet
            packet = new byte[(int)PacketSize + (PacketSize % 4)];

            // Write Size of new Packet and command inside the byte array
            Array.Copy(BitConverter.GetBytes(PacketSize), 0, packet, 0, 4);
            Array.Copy(Command, 0, packet, 4, 4);
        }

        // Used for reading packets recieved from the client
        public PacketBuilder(byte[] rawPacket)
        {
            // Copy recieved Packet into this object 
            uint rawPacketSize = BitConverter.ToUInt32(rawPacket, 0);
            packet = new byte[(int)rawPacketSize];
            Array.Copy(rawPacket, 0, packet, 0, rawPacketSize);
        }

        public PacketBuilder(int offset, byte[] rawPacket)
        {
            // Copy recieved Packet into this object 
            uint rawPacketSize = BitConverter.ToUInt32(rawPacket, offset);
            packet = new byte[(int)rawPacketSize];
            Array.Copy(rawPacket, offset, packet, 0, rawPacketSize);
        }

        // :V
        public uint PacketSize { get { return BitConverter.ToUInt32(packet, 0); } }

        // Integer
        public int ReadInt(int offset)
        {
            return BitConverter.ToInt32(packet, offset);
        }
        public void WriteInt(int value, int offset)
        {
            Array.Copy(BitConverter.GetBytes(value), 0, packet, offset, 4);
        }

        // unsigned Integer
        public uint ReadUInt(int offset)
        {
            return BitConverter.ToUInt32(packet, offset);
        }
        public void WriteUInt(uint value, int offset)
        {
            Array.Copy(BitConverter.GetBytes(value), 0, packet, offset, 4);
        }

        // Float
        public float ReadFloat(int offset)
        {
            return BitConverter.ToSingle(packet, offset);
        }
        public void WriteFloat(float value, int offset)
        {
            Array.Copy(BitConverter.GetBytes(value), 0, packet, offset, 4);
        }



        // Double
        public double ReadDouble(int offset)
        {
            return BitConverter.ToDouble(packet, offset);
        }
        public void WriteDouble(double value, int offset)
        {
            Array.Copy(BitConverter.GetBytes(value), 0, packet, offset, 4);
        }


        // Int64
        public long ReadInt64(int offset)
        {
            return BitConverter.ToInt64(packet, offset);
        }
        public void WriteInt64(long value, int offset)
        {
            Array.Copy(BitConverter.GetBytes(value), 0, packet, offset, 4);
        }

        // Bytes
        public byte ReadByte(int offset)
        {
            return packet[offset];
        }
        public void WriteByte(byte value, int offset)
        {
            packet[offset] = value;
        }

        // Quad Bytes
        public byte[] ReadQuadByte(int offset)
        {
            byte[] bytes = new byte[4];
            Array.Copy(packet, offset, bytes, 0, 4);
            return bytes;
        }
        public void WriteQuadByte(byte[] bytes, int offset)
        {
            Array.Copy(bytes, 0, packet, offset, 4);
        }

        // Strings
        public string ReadUnicodeString(int offset)
        {
            string tempString = Encoding.Unicode.GetString(packet, offset, (int)this.PacketSize - offset);
            return tempString.Substring(0, tempString.IndexOf((char)0x00));
        }
        public void WriteUnicodeString(string message, int offset)
        {
            byte[] temp = Encoding.Unicode.GetBytes(message);
            Array.Copy(temp, 0, packet, offset, temp.Length);
        }
        public string ReadASCIIIString(int offset)
        {
            string tempString = Encoding.ASCII.GetString(packet, offset, (int)this.PacketSize - offset);
            return tempString.Substring(0, tempString.IndexOf((char)0x00));
        }
        public void WriteASCIIString(string message, int offset)
        {
            byte[] temp = Encoding.ASCII.GetBytes(message);
            Array.Copy(temp, 0, packet, offset, temp.Length);
        }
    }
}
