﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetworkStreamInterceptor
{
    public class LoggerItem
    {
        // private Fields
        private string description;
        private int items;

        //public properties
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public int Items
        {
            get { return items; }
        }

    }
}
