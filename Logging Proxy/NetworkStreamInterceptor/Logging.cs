﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace NetworkStreamInterceptor
{
    class Logging
    {
        private BinaryWriter output;
        private FileStream fileStream;
        private bool ready = false;
        private bool init = false;
        private uint ID = 0;

        private UInt32 packetOrder = 0; // used to count up and identify the order of packets

        public Logging()
        {
            // nothing~
        }

        public void Close() // Important to call
        {
            //output.Flush();
            //output.Close();
            End();

        }

        public void Activate(uint userID)
        {
            if (init) return;
            ID = userID;
            Directory.CreateDirectory("data");
            Directory.CreateDirectory("data\\" + userID.ToString());
            init = true;
            //fileStream = new FileStream("data\\" + userID.ToString() + "\\" + DateTime.Now.ToString("yyyy-MM-dd--HH-mm-ss") + ".binlog", FileMode.OpenOrCreate);
            //output = new BinaryWriter(fileStream);
            //this.ready = true;

        }

        public void saveData()
        {
            output.Flush();
        }

        private object WriteLock = new object();
        public void Write(Interceptor.Direction direction, PacketBuilder packet)
        {
            if (this.ready == false)
                return;
            lock (WriteLock)
            {
                if (direction == Interceptor.Direction.ToClient)
                    output.Write(ASCIIEncoding.ASCII.GetBytes("SERV"));
                else
                    output.Write(ASCIIEncoding.ASCII.GetBytes("CLNT"));

                output.Write(BitConverter.GetBytes(packetOrder));

                

                // Blank username/password in login packets
                if (packet.Command[0] == 0x02 && packet.Command[1] == 0x19 && packet.Command[2] == 0x02)
                {
                    PacketBuilder packet2 = new PacketBuilder(packet.RawPacket);
                    packet2.WriteASCIIString("                                                ", 0x2C);
                    output.Write(packet2.RawPacket);
                }
                else
                    output.Write(packet.RawPacket);
                
                
                packetOrder += 1;
            }
        }

        public bool isReady() { return this.ready; }
        public bool isInit()  { return this.init;  }

        public uint UserID()
        {
            return ID;
        }

        public void Start()
        {
            if (ID == 0) return;
            End();
            fileStream = new FileStream("data\\" + ID.ToString() + "\\" + DateTime.Now.ToString("yyyy-MM-dd--HH-mm-ss") + ".binlog", FileMode.OpenOrCreate);
            output = new BinaryWriter(fileStream);
            ready = true;
        }

        public void End()
        {
            if (this.ready == false) return;
            ready = false;
            output.Flush();
            output.Close();
        }


    }

    //public enum InDirection
    //{
    //    ToClient,
    //    ToServer
    //}
}
