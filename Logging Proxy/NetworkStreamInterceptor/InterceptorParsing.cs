﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace NetworkStreamInterceptor
{
    public partial class Interceptor
    {
        static bool LogRaw = false;
        
        static bool firstRead = false;  // Set this to 'false' to record packets from login/char select
                                        // Don't forget to set end_test to stop logging!


        private PacketBuilder ParseMessage(PacketBuilder packet, Direction inDirection)
        {
            if (firstRead == false)
            {
                binaryLogger.Activate(10199046);
                binaryLogger.Start();
                firstRead = true;
                LogRaw = true;
            }



            // Toggle LogRaw
            // Turn on logging before packet is 
            if (packet.Command[0] == 3 && packet.Command[1] == 4 && packet.Command[2] == 2)
            {
                string tempstr = packet.ReadUnicodeString(0x84);
                // Typing "start_test" (no quotes) will enable logging. Type into any public or party chat
                if (tempstr == "start_test")
                {
                    LogRaw = true;
                    Console.WriteLine("LogRaw = TRUE");
                    if (!binaryLogger.isInit()) binaryLogger.Activate(packet.ReadUInt(0x30));
                    binaryLogger.Start();
                }
            }


            if (LogRaw && binaryLogger.isInit())
                binaryLogger.Write(inDirection, packet);


            // Turn off logging after packet is logged
            if (packet.Command[0] == 3 && packet.Command[1] == 4 && packet.Command[2] == 3)
            {
                string tempstr = packet.ReadUnicodeString(0x84);
                // Typing "end_test" (no quotes) will disable logging. Type into any public or party chat
                if (tempstr == "end_test")
                {
                    LogRaw = false;
                    Console.WriteLine("LogRaw = FALSE");
                    binaryLogger.End();
                }
            }

            if (inDirection == Direction.ToClient && packet.Command[0] == 0x1D && packet.Command[1] == 0x07 && packet.Command[2] == 0x03)
            {
                Console.WriteLine("Roulette Magic Number: " + packet.ReadUInt(0x34));
            }






            // Read Length

                int bytesRead = (int)packet.PacketSize;

                switch (packet.Command[0])
                {
                    #region Authentification

                    case (byte)PSUPCategory.Authentification:
                        // Command Category for Authentification?
                        //Console.WriteLine(" - Category: Authentification, login and checks");
                        // Check for function
                        switch (packet.Command[1])
                        {
                            case (byte)PSUPAuthentificationFunction.ServerHello:
                                //Roots.Logging.Add("* Recieved Server Hello", tempMessage, inDirection, inLength, LogLevelEnum.Status);
                                //Console.WriteLine(" - Function: Server Hello");
                                break;

                            case (byte)PSUPAuthentificationFunction.Login:
                                //Roots.Logging.Add("* Sending Login Request", tempMessage, inDirection, inLength, LogLevelEnum.Status);
                                //Console.WriteLine(" - Function: Sending login request");
                                break;

                            case (byte)PSUPAuthentificationFunction.Redirect: // ### Redirect function
                                if (inDirection == Direction.ToClient)
                                {
                                    // Read IP Address and Port and tell the PSUClient to reconnect to this proxy
                                    byte[] IPAdress = packet.ReadQuadByte(44);
                                    packet.WriteQuadByte(localIP, 44);

                                    int Port = packet.ReadInt(48);
                                    packet.WriteUInt((uint)PSUServerPort, 48);

                                    // Point Proxy to new server
                                    PSUServerIP = IPAdress[0] + "." + IPAdress[1] + "." + IPAdress[2] + "." + IPAdress[3];
                                    PSUServerPort = Port;

                                    //Roots.Logging.Add("* Redirect to new server at " + PSUServerIP + ":" + PSUServerPort, tempMessage, inDirection, inLength, LogLevelEnum.Status);
                                    //Console.WriteLine(" - Function: Redirect to new server {0}:{1}", PSUServerIP, PSUServerPort);
                                }
                                //else
                                //Roots.Logging.Add("Recieved illegal packet from Client: Redirect", tempMessage, inDirection, inLength, LogLevelEnum.Errors);
                                break;


                            case (byte)PSUPAuthentificationFunction.GameGuardCheck:
                                //Roots.Logging.Add("GameGuard Check", tempMessage, inDirection, inLength, LogLevelEnum.Warnings);
                                break;

                            case (byte)PSUPAuthentificationFunction.GameGuardCkeckResponse:
                                //Roots.Logging.Add("GameGuard Check Response", tempMessage, inDirection, inLength, LogLevelEnum.Warnings);
                                break;

                            case (byte)PSUPAuthentificationFunction.MOTD:
                                //Roots.Logging.Add("Message of the Day", tempMessage, inDirection, inLength, LogLevelEnum.Notes);
                                break;

                            case (byte)PSUPAuthentificationFunction.RecieveSession:
                                Roots.SetGCNumber(packet.ReadUInt(0x2C));
                                break;

                            default:
                                //Roots.Logging.Add("Function Unknown", tempMessage, inDirection, inLength, LogLevelEnum.Unimplemented);
                                break;
                        }

                        break;
                    // Authentification end
                    #endregion

                    case (byte)PSUPCategory.CharacterData:
                        if ((packet.Command[1] == 0x0E) && (packet.Command[2] == 0x03))
                        {
                            Roots.SetLocationName(packet.ReadUnicodeString(0x38));
                        }
                        break;

                    case (byte)PSUPCategory.SceneSetup:
                        if (packet.Command[1] == 0x03)
                        {
                            if ((packet.ReadUInt(0x0c) == (uint)0) && test(packet.ReadQuadByte(0x14), new byte[4] { 0x02, 0x0F, 0x03, 0x00 }))
                            {
                                nblFileDumper.RegisterFile(packet.ReadUInt(0x08), packet.ReadInt(0x08));
                            }

                            byte[] temp = new byte[packet.PacketSize - 0x10];
                            Array.Copy(packet.RawPacket, 0x10, temp, 0, packet.PacketSize - 0x10);
                            nblFileDumper.AddFile(temp, packet.ReadUInt(0x08), packet.ReadInt(0x0C));

                            /*if( packet.ReadQuadByte(0x24) == new byte[4] { 0x02, 0x0F, 0x03, 0x00 }))
                            {
                            }*/
                        }

                        break;

                    case (byte)PSUPCategory.GameGuard:
                        if ((packet.Command[1] == (byte)0x0E) && (packet.Command[2] == (byte)0x02))
                        {
                            packet.WriteByte(0x00, 0x2C); // Check Disable
                            packet.WriteByte(clientLang, 0x2D); // Language setting - 0x00 JP - 0x01 EN - 0x03 FR - 0x04 DE
                            packet.WriteByte(0x01, 0x30); // System - 0x00 PS2 - 0x01 PC
                        }
                        //Roots.Logging.Add("Function Unknown", tempMessage, inDirection, inLength, LogLevelEnum.Unimplemented);
                        break;

                    case (byte)PSUPCategory.Chat:
                        switch (packet.Command[1])
                        {
                            case (byte)PSUPChatFunction.ChatMessage:
                                if (packet.Command[2] == 0x03)
                                {
                                    string ChatName = packet.ReadUnicodeString(0x44);
                                    string ChatMessage = packet.ReadUnicodeString(0x84);

                                    Roots.AddChatMessage(ChatName, ChatMessage);
                                }
                                //else
                                //Roots.Logging.Add("Sent chat message", tempMessage, inDirection, inLength, LogLevelEnum.Chat);
                                break;

                            default:
                                //Roots.Logging.Add("Function Unknown", tempMessage, inDirection, inLength, LogLevelEnum.Unimplemented);
                                break;
                        }
                        break;

                    case (byte)PSUPCategory.GuildCard:
                        switch (packet.Command[1])
                        {
                            case (byte)PSUPGuildCard.Nothing:
                                Roots.SetCharacterName(packet.ReadUnicodeString(0x2C));
                                break;

                            case (byte)PSUPGuildCard.SimpleMailRecieve:
                                //Roots.Logging.Add("SimpleMail Recieved", tempMessage, inDirection, inLength, LogLevelEnum.Notes);
                                break;
                            case (byte)PSUPGuildCard.SimpleMailSend:
                                //Roots.Logging.Add("SimpleMail Send", tempMessage, inDirection, inLength, LogLevelEnum.Notes);
                                break;
                            case (byte)PSUPGuildCard.SimpleMailMarkAsRead:
                                //Roots.Logging.Add("SimpleMail marked as read", tempMessage, inDirection, inLength, LogLevelEnum.Notes);
                                break;
                            default:
                                //Roots.Logging.Add("Function Unknown", tempMessage, inDirection, inLength, LogLevelEnum.Unimplemented);
                                break;
                        }
                        break;

                    case (byte)PSUPCategory.Music:
                        switch (packet.Command[1])
                        {
                            case (byte)PSUPMusic.MusicChangeRequest:
                                //Roots.Logging.Add("Music Change Requested", tempMessage, inDirection, inLength, LogLevelEnum.Notes);
                                break;
                            case (byte)PSUPMusic.MusicChangeAction:
                                //Roots.Logging.Add("Music Changed", tempMessage, inDirection, inLength, LogLevelEnum.Notes);
                                break;
                            default:
                                //Roots.Logging.Add("Function Unknown", tempMessage, inDirection, inLength, LogLevelEnum.Unimplemented);
                                break;
                        }
                        break;




                    case (byte)PSUPCategory.Movement:
                    case (byte)PSUPCategory.ModelAnimation:
                        //Roots.Logging.Add("Function Unknown - Movement and Animation related", tempMessage, inDirection, inLength, LogLevelEnum.Everything);
                        break;

                    default:
                        // Unknown Command Category
                        //Roots.Logging.Add("Function Unknown", tempMessage, inDirection, inLength, LogLevelEnum.Unimplemented);
                        break;
                }
            
            //Console.WriteLine(" - Raw Packet Stream: " + BitConverter.ToString(inMessage, 0, bytesRead));

            //Roots.AddChatMessage(0,BitConverter.ToString(inMessage,0,bytesRead));

            return packet;
        }

        public enum PSUPCategory : byte
        {
            Authentification = 0x02,
            Chat = 0x03,
            Movement = 0x05,
            ModelAnimation = 0x01,
            GameGuard = 0x08,
            GuildCard = 0x15,
            Music = 0x13,
            CharacterData = 0x10,
            SceneSetup = 0x0B,
            Nothing = 0x00
        }

        public enum PSUPGuildCard : byte
        {
            SimpleMailSend = 0x0B,
            SimpleMailRecieve = 0x0F,
            SimpleMailMarkAsRead = 0x0D,
            Nothing = 0x00
        }

        public enum PSUPMusic : byte
        {
            MusicChangeRequest = 0x0D,
            MusicChangeAction = 0x0E,
            Nothing = 0x00
        }

        public enum PSUPAuthentificationFunction : byte
        {
            ServerHello = 0x02,
            Login = 0x19,
            Redirect = 0x16,
            MOTD = 0x25,
            GameGuardCheck = 0x2D,
            GameGuardCkeckResponse = 0x2E,
            RecieveSession = 0x23,
            Nothing = 0x00
            // 0x00
        }

        public enum PSUPGameGuardFunction : byte
        {
            GameGuardCheckResponse = 0x0D,
            Nothing = 0x00
        }

        public enum PSUPChatFunction : byte
        {
            ChatMessage = 0x04,
            Nothing = 0x00
        }

        public static bool test(byte[] A, byte[] B)
        {
            if (A.Length != B.Length)
                return false;
            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] != B[i])
                    return false;
            }
            return true;
        }

    }
}
