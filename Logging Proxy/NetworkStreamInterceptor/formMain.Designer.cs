﻿namespace NetworkStreamInterceptor
{
    partial class formMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "00000000",
            ""}, -1);
            this.textChatlog = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageCharacter = new System.Windows.Forms.TabPage();
            this.textCharacterName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPageRoom = new System.Windows.Forms.TabPage();
            this.tabPageOthers = new System.Windows.Forms.TabPage();
            this.buttonButtonCommandRemove = new System.Windows.Forms.Button();
            this.buttonCommandEdit = new System.Windows.Forms.Button();
            this.buttonCommandAdd = new System.Windows.Forms.Button();
            this.listViewCommands = new System.Windows.Forms.ListView();
            this.columnHeaderCommand = new System.Windows.Forms.ColumnHeader();
            this.columnHeaderDescription = new System.Windows.Forms.ColumnHeader();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonPacketSendToClient = new System.Windows.Forms.Button();
            this.buttonPacketSend = new System.Windows.Forms.Button();
            this.textManualPacket = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPageProxy = new System.Windows.Forms.TabPage();
            this.checkRawPackets = new System.Windows.Forms.CheckBox();
            this.buttonSetCurrentServer = new System.Windows.Forms.Button();
            this.textCurrentServer = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelCurrentLocation = new System.Windows.Forms.Label();
            this.textCurrentLocation = new System.Windows.Forms.TextBox();
            this.textChatMessage = new System.Windows.Forms.TextBox();
            this.buttonSendChat = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPageCharacter.SuspendLayout();
            this.tabPageOthers.SuspendLayout();
            this.tabPageProxy.SuspendLayout();
            this.SuspendLayout();
            // 
            // textChatlog
            // 
            this.textChatlog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textChatlog.BackColor = System.Drawing.SystemColors.Window;
            this.textChatlog.Location = new System.Drawing.Point(12, 38);
            this.textChatlog.Multiline = true;
            this.textChatlog.Name = "textChatlog";
            this.textChatlog.ReadOnly = true;
            this.textChatlog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textChatlog.Size = new System.Drawing.Size(414, 383);
            this.textChatlog.TabIndex = 5;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPageCharacter);
            this.tabControl1.Controls.Add(this.tabPageRoom);
            this.tabControl1.Controls.Add(this.tabPageOthers);
            this.tabControl1.Controls.Add(this.tabPageProxy);
            this.tabControl1.Location = new System.Drawing.Point(432, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(240, 438);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPageCharacter
            // 
            this.tabPageCharacter.Controls.Add(this.textCharacterName);
            this.tabPageCharacter.Controls.Add(this.label2);
            this.tabPageCharacter.Location = new System.Drawing.Point(4, 22);
            this.tabPageCharacter.Name = "tabPageCharacter";
            this.tabPageCharacter.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCharacter.Size = new System.Drawing.Size(232, 412);
            this.tabPageCharacter.TabIndex = 0;
            this.tabPageCharacter.Text = "Character";
            this.tabPageCharacter.UseVisualStyleBackColor = true;
            // 
            // textCharacterName
            // 
            this.textCharacterName.BackColor = System.Drawing.SystemColors.Window;
            this.textCharacterName.Location = new System.Drawing.Point(47, 6);
            this.textCharacterName.Name = "textCharacterName";
            this.textCharacterName.ReadOnly = true;
            this.textCharacterName.Size = new System.Drawing.Size(179, 20);
            this.textCharacterName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Name";
            // 
            // tabPageRoom
            // 
            this.tabPageRoom.Location = new System.Drawing.Point(4, 22);
            this.tabPageRoom.Name = "tabPageRoom";
            this.tabPageRoom.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRoom.Size = new System.Drawing.Size(232, 412);
            this.tabPageRoom.TabIndex = 1;
            this.tabPageRoom.Text = "Room";
            this.tabPageRoom.UseVisualStyleBackColor = true;
            // 
            // tabPageOthers
            // 
            this.tabPageOthers.Controls.Add(this.buttonButtonCommandRemove);
            this.tabPageOthers.Controls.Add(this.buttonCommandEdit);
            this.tabPageOthers.Controls.Add(this.buttonCommandAdd);
            this.tabPageOthers.Controls.Add(this.listViewCommands);
            this.tabPageOthers.Controls.Add(this.label4);
            this.tabPageOthers.Controls.Add(this.buttonPacketSendToClient);
            this.tabPageOthers.Controls.Add(this.buttonPacketSend);
            this.tabPageOthers.Controls.Add(this.textManualPacket);
            this.tabPageOthers.Controls.Add(this.label3);
            this.tabPageOthers.Location = new System.Drawing.Point(4, 22);
            this.tabPageOthers.Name = "tabPageOthers";
            this.tabPageOthers.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOthers.Size = new System.Drawing.Size(232, 412);
            this.tabPageOthers.TabIndex = 2;
            this.tabPageOthers.Text = "Others";
            this.tabPageOthers.UseVisualStyleBackColor = true;
            // 
            // buttonButtonCommandRemove
            // 
            this.buttonButtonCommandRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonButtonCommandRemove.Location = new System.Drawing.Point(6, 383);
            this.buttonButtonCommandRemove.Name = "buttonButtonCommandRemove";
            this.buttonButtonCommandRemove.Size = new System.Drawing.Size(36, 23);
            this.buttonButtonCommandRemove.TabIndex = 8;
            this.buttonButtonCommandRemove.Text = "Del.";
            this.buttonButtonCommandRemove.UseVisualStyleBackColor = true;
            this.buttonButtonCommandRemove.Click += new System.EventHandler(this.buttonButtonCommandRemove_Click);
            // 
            // buttonCommandEdit
            // 
            this.buttonCommandEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCommandEdit.Location = new System.Drawing.Point(70, 383);
            this.buttonCommandEdit.Name = "buttonCommandEdit";
            this.buttonCommandEdit.Size = new System.Drawing.Size(75, 23);
            this.buttonCommandEdit.TabIndex = 7;
            this.buttonCommandEdit.Text = "Edit";
            this.buttonCommandEdit.UseVisualStyleBackColor = true;
            this.buttonCommandEdit.Click += new System.EventHandler(this.buttonCommandEdit_Click);
            // 
            // buttonCommandAdd
            // 
            this.buttonCommandAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCommandAdd.Location = new System.Drawing.Point(151, 383);
            this.buttonCommandAdd.Name = "buttonCommandAdd";
            this.buttonCommandAdd.Size = new System.Drawing.Size(75, 23);
            this.buttonCommandAdd.TabIndex = 6;
            this.buttonCommandAdd.Text = "Add";
            this.buttonCommandAdd.UseVisualStyleBackColor = true;
            this.buttonCommandAdd.Click += new System.EventHandler(this.buttonCommandAdd_Click);
            // 
            // listViewCommands
            // 
            this.listViewCommands.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.listViewCommands.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderCommand,
            this.columnHeaderDescription});
            this.listViewCommands.FullRowSelect = true;
            listViewItem1.StateImageIndex = 0;
            this.listViewCommands.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1});
            this.listViewCommands.Location = new System.Drawing.Point(6, 168);
            this.listViewCommands.MultiSelect = false;
            this.listViewCommands.Name = "listViewCommands";
            this.listViewCommands.Size = new System.Drawing.Size(220, 209);
            this.listViewCommands.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listViewCommands.TabIndex = 5;
            this.listViewCommands.UseCompatibleStateImageBehavior = false;
            this.listViewCommands.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderCommand
            // 
            this.columnHeaderCommand.Tag = "tagCommand";
            this.columnHeaderCommand.Text = "Command";
            // 
            // columnHeaderDescription
            // 
            this.columnHeaderDescription.Tag = "tagDescription";
            this.columnHeaderDescription.Text = "Description";
            this.columnHeaderDescription.Width = 139;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Commands";
            // 
            // buttonPacketSendToClient
            // 
            this.buttonPacketSendToClient.Location = new System.Drawing.Point(70, 126);
            this.buttonPacketSendToClient.Name = "buttonPacketSendToClient";
            this.buttonPacketSendToClient.Size = new System.Drawing.Size(75, 23);
            this.buttonPacketSendToClient.TabIndex = 3;
            this.buttonPacketSendToClient.Text = "to Client";
            this.buttonPacketSendToClient.UseVisualStyleBackColor = true;
            this.buttonPacketSendToClient.Click += new System.EventHandler(this.buttonPacketSendToClient_Click);
            // 
            // buttonPacketSend
            // 
            this.buttonPacketSend.Location = new System.Drawing.Point(151, 126);
            this.buttonPacketSend.Name = "buttonPacketSend";
            this.buttonPacketSend.Size = new System.Drawing.Size(75, 23);
            this.buttonPacketSend.TabIndex = 2;
            this.buttonPacketSend.Text = "to Server";
            this.buttonPacketSend.UseVisualStyleBackColor = true;
            this.buttonPacketSend.Click += new System.EventHandler(this.buttonPacketSend_Click);
            // 
            // textManualPacket
            // 
            this.textManualPacket.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textManualPacket.Location = new System.Drawing.Point(9, 19);
            this.textManualPacket.Multiline = true;
            this.textManualPacket.Name = "textManualPacket";
            this.textManualPacket.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textManualPacket.Size = new System.Drawing.Size(217, 101);
            this.textManualPacket.TabIndex = 1;
            this.textManualPacket.Text = "00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Manual packet send";
            // 
            // tabPageProxy
            // 
            this.tabPageProxy.Controls.Add(this.checkRawPackets);
            this.tabPageProxy.Controls.Add(this.buttonSetCurrentServer);
            this.tabPageProxy.Controls.Add(this.textCurrentServer);
            this.tabPageProxy.Controls.Add(this.label1);
            this.tabPageProxy.Location = new System.Drawing.Point(4, 22);
            this.tabPageProxy.Name = "tabPageProxy";
            this.tabPageProxy.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageProxy.Size = new System.Drawing.Size(232, 412);
            this.tabPageProxy.TabIndex = 3;
            this.tabPageProxy.Text = "Proxy";
            this.tabPageProxy.UseVisualStyleBackColor = true;
            // 
            // checkRawPackets
            // 
            this.checkRawPackets.AutoSize = true;
            this.checkRawPackets.Checked = true;
            this.checkRawPackets.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkRawPackets.Location = new System.Drawing.Point(6, 45);
            this.checkRawPackets.Name = "checkRawPackets";
            this.checkRawPackets.Size = new System.Drawing.Size(142, 17);
            this.checkRawPackets.TabIndex = 8;
            this.checkRawPackets.Text = "Show raw packets in log";
            this.checkRawPackets.UseVisualStyleBackColor = true;
            this.checkRawPackets.CheckedChanged += new System.EventHandler(this.checkRawPackets_CheckedChanged);
            // 
            // buttonSetCurrentServer
            // 
            this.buttonSetCurrentServer.Location = new System.Drawing.Point(151, 17);
            this.buttonSetCurrentServer.Name = "buttonSetCurrentServer";
            this.buttonSetCurrentServer.Size = new System.Drawing.Size(75, 23);
            this.buttonSetCurrentServer.TabIndex = 7;
            this.buttonSetCurrentServer.Text = "Set";
            this.buttonSetCurrentServer.UseVisualStyleBackColor = true;
            // 
            // textCurrentServer
            // 
            this.textCurrentServer.Location = new System.Drawing.Point(6, 19);
            this.textCurrentServer.Name = "textCurrentServer";
            this.textCurrentServer.Size = new System.Drawing.Size(136, 20);
            this.textCurrentServer.TabIndex = 5;
            this.textCurrentServer.Text = "000.000.000.000";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Current Server:";
            // 
            // labelCurrentLocation
            // 
            this.labelCurrentLocation.AutoSize = true;
            this.labelCurrentLocation.Location = new System.Drawing.Point(12, 15);
            this.labelCurrentLocation.Name = "labelCurrentLocation";
            this.labelCurrentLocation.Size = new System.Drawing.Size(88, 13);
            this.labelCurrentLocation.TabIndex = 3;
            this.labelCurrentLocation.Text = "Current Location:";
            // 
            // textCurrentLocation
            // 
            this.textCurrentLocation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textCurrentLocation.BackColor = System.Drawing.SystemColors.Window;
            this.textCurrentLocation.Location = new System.Drawing.Point(106, 12);
            this.textCurrentLocation.Name = "textCurrentLocation";
            this.textCurrentLocation.ReadOnly = true;
            this.textCurrentLocation.Size = new System.Drawing.Size(320, 20);
            this.textCurrentLocation.TabIndex = 4;
            // 
            // textChatMessage
            // 
            this.textChatMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textChatMessage.Location = new System.Drawing.Point(12, 429);
            this.textChatMessage.MaxLength = 100;
            this.textChatMessage.Name = "textChatMessage";
            this.textChatMessage.Size = new System.Drawing.Size(333, 20);
            this.textChatMessage.TabIndex = 0;
            this.textChatMessage.WordWrap = false;
            // 
            // buttonSendChat
            // 
            this.buttonSendChat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSendChat.Location = new System.Drawing.Point(351, 427);
            this.buttonSendChat.Name = "buttonSendChat";
            this.buttonSendChat.Size = new System.Drawing.Size(75, 23);
            this.buttonSendChat.TabIndex = 1;
            this.buttonSendChat.Text = "Send";
            this.buttonSendChat.UseVisualStyleBackColor = true;
            this.buttonSendChat.Click += new System.EventHandler(this.buttonSendChat_Click);
            // 
            // formMain
            // 
            this.AcceptButton = this.buttonSendChat;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 462);
            this.Controls.Add(this.buttonSendChat);
            this.Controls.Add(this.textChatMessage);
            this.Controls.Add(this.textCurrentLocation);
            this.Controls.Add(this.labelCurrentLocation);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.textChatlog);
            this.MinimumSize = new System.Drawing.Size(700, 500);
            this.Name = "formMain";
            this.Text = "NeSI for Phantasy Star Universe";
            this.Load += new System.EventHandler(this.formMain_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.formMain_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formMain_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.tabPageCharacter.ResumeLayout(false);
            this.tabPageCharacter.PerformLayout();
            this.tabPageOthers.ResumeLayout(false);
            this.tabPageOthers.PerformLayout();
            this.tabPageProxy.ResumeLayout(false);
            this.tabPageProxy.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textChatlog;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageCharacter;
        private System.Windows.Forms.TabPage tabPageRoom;
        private System.Windows.Forms.TabPage tabPageOthers;
        private System.Windows.Forms.TabPage tabPageProxy;
        private System.Windows.Forms.Button buttonSetCurrentServer;
        private System.Windows.Forms.TextBox textCurrentServer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelCurrentLocation;
        private System.Windows.Forms.TextBox textCurrentLocation;
        private System.Windows.Forms.TextBox textChatMessage;
        private System.Windows.Forms.Button buttonSendChat;
        private System.Windows.Forms.TextBox textCharacterName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonPacketSend;
        private System.Windows.Forms.TextBox textManualPacket;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkRawPackets;
        private System.Windows.Forms.Button buttonPacketSendToClient;
        private System.Windows.Forms.ListView listViewCommands;
        private System.Windows.Forms.ColumnHeader columnHeaderCommand;
        private System.Windows.Forms.ColumnHeader columnHeaderDescription;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonButtonCommandRemove;
        private System.Windows.Forms.Button buttonCommandEdit;
        private System.Windows.Forms.Button buttonCommandAdd;
    }
}