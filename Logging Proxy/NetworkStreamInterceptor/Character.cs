﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetworkStreamInterceptor
{
    class Character
    {
        // Define basic Fields
        public string Name;
        public GenderEnum Gender;
        public RaceEnum Race;
        public JobsEnum CurrentJob;

        // Base Stats
        public uint HitPoints;
        public uint AttackPower;
        public uint DefensivePower;
        public uint AttackAccuracy;
        public uint EvasionPower;
        public uint Stamina;
        public uint TechniquePower;
        public uint MentalStrength;

        // Level and experience points
        public uint Level;
        public uint ExperiencePoints;

        // Appearance - TODO: Fill with things

        // ItemsBoxes - TODO: Fill with Arrays :O





        #region Enums for races, genders and jobs
        public enum RaceEnum
        {
            Human,
            Newman,
            Cast,
            Beast
        }

        public enum GenderEnum
        {
            Male,
            Female
        }

        public enum JobsEnum
        {
            Hunter,
            Ranger,
            Force
        }
        #endregion
    }
}
