﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Diagnostics;
using System.Threading;

namespace GameServer_R
{
    public class Scene
    {

        // lock 
        private object sceneLock = new object();

        // Map data
        // - todo: Map data itself
        private string areaName = "";
        public string AreaName { get { return areaName; } }

        public byte[] Identification
        {
            get
            {
                byte[] temp = new byte[8];
                Array.Copy(BitConverter.GetBytes(AreaID), 0, temp, 0, 4);
                Array.Copy(BitConverter.GetBytes(configurationID), 0, temp, 4, 4);
                return temp;
            }
        }


        private uint areaID;
        public uint AreaID { get { return areaID; } }
        private uint configurationID = 0;
        public uint ConfigurationID { get { return configurationID; } }

        private uint additionalID = 0;
        public uint AdditionalID { get { return additionalID; } }


        // Client handling
        public Dictionary<int, uint> ConnectedClientReferences = new Dictionary<int, uint>();
        public int ConnectedClients { get { return ConnectedClientReferences.Count; } }

        // Scene properties
        private SceneType type;
        private SceneLocation location;

        private string mapFilePath = "";
        public string MapFilePath { get { return mapFilePath; } }

        public Scene(string scenefile, SceneType sceneType, SceneLocation sceneLocation)
        {
            try
            {
                // set type and location
                type = sceneType;
                location = sceneLocation;

                // load configuration from specified xml file
                XDocument xmlFile = XDocument.Load(scenefile);

                areaName = (string)xmlFile.Element("scene").Element("name");
                areaID = (uint)xmlFile.Element("scene").Element("areaid");
                mapFilePath = "data\\scenes\\" + (string)xmlFile.Element("scene").Element("file");

                // configuration related stuff
                configurationID = (uint)xmlFile.Element("scene").Element("configurations").Element("configuration").Attribute("id");
                additionalID = (uint?)xmlFile.Element("scene").Element("configurations").Element("configuration").Attribute("additionalid") ?? 0;


                // cache map
                if (Program.sceneManager.CachedMaps.ContainsKey(mapFilePath))
                    Program.sceneManager.CachedMaps[mapFilePath] = new MapData(File.ReadAllBytes(mapFilePath));
                else
                    Program.sceneManager.CachedMaps.Add(mapFilePath, new MapData(File.ReadAllBytes(mapFilePath)));
            }
            catch (Exception e)
            {
                Program.log.Add(e.ToString(), Logger.LogLevel.Error);
            }
                        
            
            
            
            
            // Setup Events
            this.PlayerJoinedEvent = (sender, e) =>
            {
                // get list of gameclients
                var tempGameClientList = Program.gameClientManager.connectedClients.Where(x => x.CurrentScene.SequenceEqual(this.Identification)).ToArray();

                int newLobbyObjectID = this.ConnectedClientReferences.Single(x => x.Value == e.helper.UserID).Key;
                uint newUserID = e.helper.UserID;

                // prepare packets
                // Prepare packet 1 11 3 (Lobby Object Creation)
                Packet packetLobbyObjectCreation = new Packet((uint)0x3C, new byte[4] { 0x01, 0x11, 0x03, 0x00 });
                packetLobbyObjectCreation.WriteUInt(newUserID, 0x2C);
                packetLobbyObjectCreation.WriteInt(newLobbyObjectID, 0x30);//lobby object id
                packetLobbyObjectCreation.WriteUInt(6, 0x34);

                Server.DebugPacketData(packetLobbyObjectCreation);
                

                // Prepare packet 1 d 3 (Character Data Transfer)
                Packet packetCharacterDataTransfer = new Packet((uint)0x2E0, new byte[4] { 0x01, 0x0D, 0x03, 0x00 });

                packetCharacterDataTransfer.WriteUInt(1, 0x2C);
                //packet.WriteUInt(0x030065, 0x34);
                //packet.WriteUInt(0xFF00FF, 0x38);
                packetCharacterDataTransfer.WriteUInt(newUserID, 0x40);


                packetCharacterDataTransfer.WriteUInt(newUserID, 0x5C);

                packetCharacterDataTransfer.WriteInt(newLobbyObjectID, 0x60); // lobby object id
                packetCharacterDataTransfer.WriteQuadByte(new byte[4] { 0xFF, 0xFF, 0xFF, 0xFF }, 0x64);


                packetCharacterDataTransfer.WriteByteArray(e.gameClient.CurrentCharacterData.DataCharLobby, 0x68);

            

                // iterate through each client
                foreach (var tempGameClient in tempGameClientList)
                {
                    Server.SendPacketAsync(packetLobbyObjectCreation, tempGameClient.UserID);
                    Server.SendPacketAsync(packetCharacterDataTransfer, tempGameClient.UserID);

                }
            };
            




            this.PlayerLoadedMapEvent = (sender, e) =>
            {
                // get list of gameclients
                var tempGameClientList = Program.gameClientManager.connectedClients.Where(x => x.CurrentScene.SequenceEqual(this.Identification)).ToArray();

                int newLobbyObjectID = this.ConnectedClientReferences.Single(x => x.Value == e.helper.UserID).Key;
                uint newUserID = e.helper.UserID;

                // Prepare packet 2 3 3 (assing object id with player id)
                Packet packet1 = new Packet((uint)0x344, new byte[4] { 0x02, 0x03, 0x03, 0x00 });

                packet1.WriteUInt(newUserID, 0x2C);
                packet1.WriteInt(newLobbyObjectID, 0x30); // lobby object id



                // Prepare packet 2 5 3 (Spawn Object at specific position)
                // Should only be called after the client has successfully loaded
                Packet packet2 = new Packet((uint)0x44, new byte[4] { 0x02, 0x05, 0x03, 0x00 });
                //packet.WriteQuadByte(new byte[4] { 0x00, 0x00, 0x8D, 0x09 }, 0x08);

                packet2.WriteQuadByte(new byte[4] { 0xFF, 0xFF, 0xFF, 0xFF }, 0x2C);
                packet2.WriteByteArray(new byte[16] {
                        0x00, 0x00, 0x00, 0x00,// idunno
                        0x04, 0x00, 0x00, 0x00, // configuration id
                        0x00, 0x00, 0x00, 0x00,// spawn slot
                        0x00, 0x00, 0x00, 0x00  
                    }, 0x30);

                packet2.WriteUInt(additionalID, 0x30); // this made the door bug D:
                packet2.WriteUInt(this.configurationID, 0x34);
                //packet.WriteUInt(Program.Scenes.Single(x => x.uniqueID == e.Client.CurrentScene)., 0x34);
                packet2.WriteInt(e.gameClient.SpawnSlot, 0x38);


                packet2.WriteInt(newLobbyObjectID, 0x40); // lobby object id



                // iterate through each client
                foreach (var tempGameClient in tempGameClientList)
                {
                    Server.SendPacketAsync(packet1, tempGameClient.UserID);
                    Server.SendPacketAsync(packet2, tempGameClient.UserID);
                }
                e.gameClient.ClientState = GameClientState.Idle;
            };



            this.PlayerSpawnedEvent = (sender, e) =>
            {
                // get list of gameclients
                var tempGameClientList = Program.gameClientManager.connectedClients.Where(x => x.CurrentScene.SequenceEqual(this.Identification)).ToArray();

                int newLobbyObjectID = this.ConnectedClientReferences.Single(x => x.Value == e.helper.UserID).Key;
                uint newUserID = e.helper.UserID;

                // Prepare packet 2 1 3 (spawning client appearance etc.)
                // Should only be sent after the client is really ready
                Packet packet = new Packet((uint)0x344, new byte[4] { 0x02, 0x01, 0x03, 0x00 });

                packet.WriteQuadByte(new byte[4] { 0x00, 0x00, 0x12, 0x00 }, 0x0C);
                packet.WriteUInt(newUserID, 0x10);

                packet.WriteQuadByte(new byte[4] { 0x00, 0x00, 0x12, 0x00 }, 0x2C);
                packet.WriteUInt(newUserID, 0x30);

                packet.WriteInt(newLobbyObjectID, 0x3C); //lobby object id


                packet.WriteUInt(0xFFFF0000, 0x40);
                packet.WriteUInt(this.areaID, 0x44); // idunnowatsdis
                packet.WriteUInt(additionalID, 0x48); // DOORBUG!!!! :3
                //packet.WriteUInt(1, 0x4C); 
                packet.WriteUInt(this.configurationID, 0x4C);



                //packet.WriteUInt(0x2, 0x50); // slot?


                packet.WriteByteArray(e.gameClient.CurrentCharacterData.DataCharLobby, 0x7C);
                packet.WriteUInt(0x1, 0x18C);

                if ((e.gameClient.Status == AccountStatus.Administrator) || (e.gameClient.Status == AccountStatus.GameMaster))
                    packet.WriteUInt(13, 0x2F4); // make gm color





                Packet packet2 = new Packet((uint)0x30, new byte[4] { 0x02, 0x2C, 0x03, 0x00 });
                packet2.WriteInt(newLobbyObjectID, 0x2C); // lobby object id
                packet2.WriteByte(0x10, 0x2E);

                // iterate through each client
                foreach (var tempGameClient in tempGameClientList)
                {
                    Server.SendPacketAsync(packet, tempGameClient.UserID);
                    //Program.server.SendPacketAsync(packet2, tempGameClient.UserID);
                }

                // Send final lobby state
                if (ConnectedClients > 1) // Fix this workaround plz :3
                {
                    Packet lobbyState = new Packet((uint)(0x30 + (ConnectedClients - 1) * 0x2CC), new byte[4] { 0x02, 0x33, 0x03, 0x00 });
                    int i = 0;

                    foreach (var client in ConnectedClientReferences)
                    {
                        if (client.Value != newUserID)
                        {
                            GameClient temp = Program.gameClientManager.RetrieveGameClient(client.Value);
                            lobbyState.WriteByteArray(new byte[4] { 0x00, 0x00, 0x12, 0x00 }, 0x30 + i * 0x2cc);
                            lobbyState.WriteUInt(client.Value, 0x34 + i * 0x2Cc); // client number

                            lobbyState.WriteInt(client.Key, 0x40 + i * 0x2CC);
                            lobbyState.WriteUInt(this.areaID, 0x48 + i * 0x2CC);
                            lobbyState.WriteUInt(this.configurationID, 0x50 + i * 0x2CC);
                            //lobbyState.WriteUInt(this.AreaID, 0x70 + i * 0x2Cc); // areaid
                            lobbyState.WriteUInt(this.configurationID, 0x78 + i * 0x2CC); // configuration id
                            //lobbyState.WriteInt(client.Key, 0x7c+ i * 0x2Cc);  // lobby object id


                            lobbyState.WriteByteArray(temp.PlayerState, 0x54 + i * 0x2Cc);
                            lobbyState.WriteByteArray(temp.CurrentCharacterData.DataCharLobby, 0x80 + i * 0x2Cc);
                            /*if ((temp.Status == AccountStatus.Administrator) || (temp.Status == AccountStatus.GameMaster))
                                packet.WriteUInt(8, 0x2F8 + i * 0x2Cc); // make gm color
                            if (temp.Invisible == true)
                                packet.WriteUInt(2, 0x2F9); // make invisible if invisible flag is set*/


                            i++;
                        }
                    }

                    lobbyState.WriteInt(i, 0x2C);

                    e.helper.SendPacketAsync(lobbyState);
                }
                
            };


            this.PlayerBroadcastEvent = (sender, e) =>
            {
                // get list of gameclients
                var tempGameClientList = Program.gameClientManager.connectedClients.Where(x => x.CurrentScene.SequenceEqual(this.Identification) && e.gameClient.ClientState == GameClientState.Idle).ToArray();

                // iterate through each client
                foreach (var tempGameClient in tempGameClientList)
                {
                    Server.SendPacketAsync(e.packet, tempGameClient.UserID);
                }
            };

            this.PlayerBroadcastExcludingSelfEvent = (sender, e) =>
            {
                // get list of gameclients
                var tempGameClientList = Program.gameClientManager.connectedClients.Where(x => x.CurrentScene.SequenceEqual(this.Identification) && e.helper.UserID != x.UserID && e.gameClient.ClientState == GameClientState.Idle).ToArray();

                // iterate through each client
                foreach (var tempGameClient in tempGameClientList)
                {
                    Server.SendPacketAsync(e.packet, tempGameClient.UserID);
                }
            };

            this.PlayerLeftEvent = (sender, e) =>
            {
                // get list of gameclients
                var tempGameClientList = Program.gameClientManager.connectedClients.Where(x => x.CurrentScene.SequenceEqual(this.Identification)).ToArray();

                int newLobbyObjectID = this.ConnectedClientReferences.Single(x => x.Value == e.helper.UserID).Key;
                uint newUserID = e.helper.UserID;


                Packet leavePacket = new Packet((uint)0x38, new byte[4] { 0x02, 0x04, 0x03, 0x00 });
                leavePacket.WriteUInt(newUserID, 0x2C);
                leavePacket.WriteInt(newLobbyObjectID, 0x30);
                leavePacket.WriteUInt(1, 0x32);
                leavePacket.WriteUInt(5, 0x34);

                // iterate through each client
                try
                {
                    PlayerBroadcastExcludingSelfEvent(sender, new PlayerEventArgs(e.helper, e.gameClient, leavePacket));
                }
                catch { }
                ConnectedClientReferences.Remove(ConnectedClientReferences.Single(x=>x.Value==e.helper.UserID).Key);
            };
        
        }


        // Events
        public EventHandler<PlayerEventArgs> PlayerJoinedEvent;     // Player joined the lobby
        public EventHandler<PlayerEventArgs> PlayerLoadedMapEvent;  // Player loaded the map
        public EventHandler<PlayerEventArgs> PlayerSpawnedEvent;    // Player spawned in the map
        public EventHandler<PlayerEventArgs> PlayerLeftEvent;       // Player left the lobby
        public EventHandler<PlayerEventArgs> PlayerBroadcastEvent;  // used to broadcast movement etc.
        public EventHandler<PlayerEventArgs> PlayerBroadcastExcludingSelfEvent;  // used to broadcast movement etc.


        public void AddPlayer(CommunicationHelper helper)
        {
            lock (sceneLock)
            {
                if (ConnectedClientReferences.ContainsValue(helper.UserID))
                    throw new Exception("Client is already in this scene. Aborting!");
                else
                {
                    int newLobbyObjectID = 1;
                    do
                    {
                        if (ConnectedClientReferences.ContainsKey(newLobbyObjectID) != true)
                            break;
                        newLobbyObjectID++;
                    } while (true);

                    ConnectedClientReferences.Add(newLobbyObjectID, helper.UserID);

                    PlayerJoinedEvent(this, new PlayerEventArgs(helper, Program.gameClientManager.RetrieveGameClient(helper.UserID), Program.dummyPacket));

                }
            }
            Thread.Sleep(300);
        }

    }

    public class PlayerEventArgs : EventArgs
    {
        public CommunicationHelper helper;
        public GameClient gameClient;
        public Packet packet;

        public PlayerEventArgs(CommunicationHelper inHelper, GameClient inGameClient, Packet inPacket)
        {
            helper = inHelper;
            gameClient = inGameClient;
            packet = inPacket;
        }
    }

    public enum SceneType
    {
        Lobby,
        Mission,
        nothing
    }

    public enum SceneLocation
    {
        Colony,
        Parum,
        Neudaiz,
        Moatoob,
        nothing
    }

}
