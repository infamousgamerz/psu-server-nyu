﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace GameServer_R
{
    public class CommandParser
    {

        public static void ParseCommand(string input, CommunicationHelper helper)
        {
           // retrieve game client to work on it
           GameClient gameClient = Program.gameClientManager.RetrieveGameClient(helper.UserID);

           ParseCommand(input, helper, gameClient);

        }

        // this list contains commands
        private static List<Command> Commands = new List<Command>();

        // setup commands
        public static void SetupCommands()
        {
            Commands.Add(new Command("help",
                AccountStatus.User,
                (input, helper, gameClient) =>
                {
                    string temp = "Available Commands:\n";
                    var cmds = Commands.OrderBy(x => x.CommandName).ToArray();

                    foreach (var cmd in cmds)
                    {
                        if (cmd.AllowedGroups != AccountStatus.User)
                        {
                            if (cmd.AllowedGroups.IsSet(gameClient.Status))
                                temp += "(non user) /" + cmd.CommandName + "\n";
                        }
                        else 
                        {
                            temp += "/" + cmd.CommandName + "\n";
                        }

                    }

                    Server.SendBroadcastMessage(helper, temp, BroadcastMessageType.SideText);
                }));

            Commands.Add(new Command("test",
                AccountStatus.User,
                (input, helper, gameClient) =>
                {
                    Program.log.Add(helper.UserIDString + " used /test - " + BitConverter.ToString(UnicodeEncoding.Unicode.GetBytes(input[0])));
                }));

            Commands.Add(new Command("reload",
                AccountStatus.Administrator,
                (input, helper, gameClient) =>
                {
                    Program.sceneManager.ReloadSettings();
                    Program.log.Add("Reloaded Settings");
                }));

            Commands.Add(new Command("listareas",
                AccountStatus.User,
                (input, helper, gameClient) =>
                {
                    string tempstring = "List of accessible areas:";
                    foreach (var item in Program.sceneManager.AreaDefinitions.Keys)
                    {
                        tempstring += "\nAreaID - " + item + "\n";
                        foreach (var temp in Program.sceneManager.scenes.Where(x => x.AreaID == item).OrderBy(x => x.ConfigurationID))
                        {
                            tempstring += temp.ConfigurationID + " - " + temp.AreaName + "\n";
                        }
                    }
                    Server.SendBroadcastMessage(helper, tempstring, BroadcastMessageType.SideText);
                }));

            Commands.Add(new Command("kick",
                AccountStatus.Administrator | AccountStatus.GameMaster,
                (input, helper, gameClient) =>
                {
                    Server.connectedClients.Single(x => x.UserID == uint.Parse(input[0])).sslStream.Write(new byte[8] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }, 0, 8);
                }));

            Commands.Add(new Command("bc",
                AccountStatus.Administrator | AccountStatus.GameMaster,
                (input, helper, gameClient) =>
                {
                    // rebuild string
                    string temp = "";
                    foreach (var item in input)
                        temp += " " + item;

                    // send reconstructed string
                    foreach (var client in Server.connectedClients)
                        Server.SendBroadcastMessage(client, temp, BroadcastMessageType.RunningText);
                }));

            Commands.Add(new Command("save",
                AccountStatus.User,
                (input, helper, gameClient) =>
                {
                    gameClient.SaveAccountData();
                    Server.SendBroadcastMessage(helper, "Your data was saved.", BroadcastMessageType.MessageBox);
                }));

            Commands.Add(new Command("teleport",
                AccountStatus.User,
                (input, helper, gameClient) =>
                {
                    uint targetAreaID = uint.Parse(input[0]);
                    uint targetConfigurationID = uint.Parse(input[1]);
                    var tempScenes = Program.sceneManager.scenes.Where(x => x.AreaID == targetAreaID && x.ConfigurationID == targetConfigurationID).ToArray();

                    if (tempScenes.Count() == 1)
                    {
                        if (input.Count() == 3)
                            gameClient.SpawnSlot = int.Parse(input[2]);
                        Server.PlayerTransfer(helper, gameClient, uint.Parse(input[0]), int.Parse(input[1]), 0);
                    }
                    else
                        throw new Exception("Area not found!");
                }));

            Commands.Add(new Command("about",
                AccountStatus.User,
                (input, helper, gameClient) =>
                {
                    Server.SendBroadcastMessage(helper, "\nPrivate Universe - " + Assembly.GetExecutingAssembly().GetName().Version.ToString() + " Alpha" + Environment.NewLine
                        + "Contributors:\n"
                        + "jericho2442\n"
                        + "BrianB\n"
                        + "coomdoom\n"
                        + "ニューゼロ\n"
                        , BroadcastMessageType.SideText);
                }));


            Commands.Add(new Command("v", AccountStatus.User, (input, helper, gameClient) => { Server.SendLotsOfPackets(helper, gameClient); }));
            Commands.Add(new Command("l", AccountStatus.User, (input, helper, gameClient) => { Server.LoadMissionCounter(helper, gameClient); }));

        }

        public static void ParseCommand(string input, CommunicationHelper helper , GameClient gameClient)
        {
            if (input == null)
                return;
            
            // split up the input string
            //string[] arguments = input.Split(' ');

            // get command name
            string commandName = input.Split(' ').First().Substring(1);
            string commandParameters = "";
            if (input.Split(' ').Count() > 1)
                commandParameters = input.Substring(input.IndexOf(' ') + 1);

            try
            {

                if (Commands.Single(x => input.Substring(1).StartsWith(x.CommandName)).Execute(commandParameters, helper, gameClient) == true)
                    throw new Exception("Something went wrong");

                /*
                // huge switch case time nao
                switch (arguments[0])
                {
                    case "/test": // Test Command

                    // Admin only commands
                    case "/invisible": // make the client invisible (or a selected client)
                        if (Status != AccountStatus.Administrator) // pretend this command does not exist
                            goto default;
                        Program.log.Add("Client wants to go invisible");

                        if (arguments.Length == 1)  // makes self invisible
                            Program.Scenes.Single(x => x.UniqueID == this.CurrentScene).MakeInvisible(this);
                        else                        // makes others invisible
                            Program.Scenes.Single(x => x.UniqueID == this.CurrentScene).MakeInvisible(Server.GameClients.Single(x => x.UserID == int.Parse(arguments[1])));

                        break;



                    case "/mode":
                        // sets mode of current room/server
                        if ((gameClient.Status != AccountStatus.Administrator) && (gameClient.Status != AccountStatus.GameMaster))
                            goto default;
                        if (arguments[1] == "+m")
                        {
                            Server.GlobalMute = true;
                            break;
                        }
                        else if (arguments[1] == "-m")
                        {
                            Server.GlobalMute = false;
                            break;
                        }
                        else
                            SendBroadcastMessage("Unknown parameter " + arguments[1], BroadcastMessageType.SideText);
                        break;


                    // User Accessible Commands

                    case "/about":
                        // save account data
                        Server.SendBroadcastMessage(helper, "\n\nPrivate Universe - " + Assembly.GetExecutingAssembly().GetName().Version.ToString() + " Alpha" + Environment.NewLine
                            + "Contributors:\n"
                            + "jericho2442\n"
                            + "BrianB\n"
                            + "coomdoom\n"
                            + "ニューゼロ\n"
                            , BroadcastMessageType.SideText);
                        break;

                    case "/help":
                        // save account data
                        Server.SendBroadcastMessage(helper, "Available commands:\n/test\n/save\n/help\n/about", BroadcastMessageType.SideText);
                        break;

                    case "/shutdown":
                        if (gameClient.Status != AccountStatus.Administrator)
                            goto default;
                        Program.shouldRun = false;
                        break;


                    // when command was not recognized
                    default:
                        Server.SendBroadcastMessage(helper, "Command " + arguments[0] + " was not recognized.\nPlease use /help for a list of available commands.", BroadcastMessageType.SideText);
                        break;
                }*/
            }
            catch (Exception e)
            {
                // TODO, implement some basic exception handling and logging

                // Tell the player that some error occured
                Server.SendBroadcastMessage(helper, "\uF801An Error occured! \uF800Please check your input for any mistakes." + Environment.NewLine + e.Message, BroadcastMessageType.MessageBox);
                Program.log.Add(helper.UserIDString + " produced an error during command parsing" + Environment.NewLine + e.ToString(), Logger.LogLevel.Error);
            }

        }
    }

    public class Command
    {
        // fields
        private string commandName = "test";
        public string CommandName { get { return commandName; } }

        public Action<string[], CommunicationHelper, GameClient> commandMethod;

        private AccountStatus allowedGroups = AccountStatus.Administrator;
        public AccountStatus AllowedGroups { get { return allowedGroups; } }

        public Command(string inCommandName, AccountStatus inAllowedGroups, Action<string[], CommunicationHelper, GameClient> inCommandMethod)
        {
            commandName = inCommandName;
            commandMethod = inCommandMethod;
            allowedGroups = inAllowedGroups;
        }

        public bool Execute(string input, CommunicationHelper helper, GameClient gameClient)
        {
            // split up the input string
            string[] arguments = input.Split(' ');

            // check if the sender even is allowed to use this command
            if (allowedGroups == AccountStatus.User)
            {
                commandMethod(arguments, helper, gameClient);

                return false;
            }
            else
            {
                if (gameClient.Status != AccountStatus.User)
                {
                    if (allowedGroups.IsSet(gameClient.Status))
                    {
                        commandMethod(arguments, helper, gameClient);
                        return false;
                    }
                }
            }

            // execute delegate

            // return false if everything went okay
            return true;
        }
    }
}
