﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameServer_R
{
    /// <summary>
    /// Use this struct to create packets in a new way.
    /// Use .ToPacket() to get the final packet
    /// </summary>
    public struct Chain
    {
        // List of chain elements as bytes
        private List<byte> elements;
        private int minSize;

        /// <summary>
        /// Contains definitions of how to interpret and convert the added items.
        /// </summary>
        private static Dictionary<Type, Func<object, byte[]>> SwitchDictionary = new Dictionary<Type, Func<object, byte[]>>()
        {
            { typeof(int),      x => { return BitConverter.GetBytes((int)x); }          },
            { typeof(short),    x => { return BitConverter.GetBytes((short)x); }        },
            { typeof(long),     x => { return BitConverter.GetBytes((long)x); }         },
            { typeof(uint),     x => { return BitConverter.GetBytes((uint)x); }         },
            { typeof(ushort),   x => { return BitConverter.GetBytes((ushort)x); }       },
            { typeof(ulong),    x => { return BitConverter.GetBytes((ulong)x); }        },
            { typeof(float),    x => { return BitConverter.GetBytes((float)x); }        },
            { typeof(double),   x => { return BitConverter.GetBytes((double)x); }       },
            { typeof(string),   x => { return Encoding.Unicode.GetBytes((string)x); }   },
            { typeof(byte[]),   x => { return (byte[])x; }                              },
        };

        /// <summary>
        /// Gets the size of this chain.
        /// </summary>
        /// <value>The size.</value>
        public int Size
        {
            get
            {
                if (elements.Count > minSize)
                    return elements.Count;
                else
                    return minSize;
            }
        }


        // Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="Chain"/> struct.
        /// </summary>
        /// <param name="minimumSize">Minimum size of the final <see cref="Packet"/> size in byte.</param>
        public Chain(int minimumSize=0x2C)
        {
            // check if minimum size is at least 8 bytes
            if (minimumSize < 8)
                throw new ArgumentOutOfRangeException("minimumSize", minimumSize, "Size can not be less than 8 bytes long");
            if ((minimumSize > 8) && (minimumSize < 0x2C))
                throw new ArgumentOutOfRangeException("minimumSize", minimumSize, "Size can not be between 8 and 44 bytes long");

            // create the list
            elements = new List<byte>();

            // add at least 8 bytes
            if (minimumSize == 8)
                elements.AddRange(new byte[8]);
            else
                elements.AddRange(new byte[0x2C]);

            // set the minSize value which defines the "true" size of the packet
            minSize = minimumSize;
        }

        // To get this as a Packet
        /// <summary>
        /// Retrieve a fully working Packet
        /// Toes the packet. That was actually automagically generated :3
        /// </summary>
        /// <returns>Chain data wrapped in PSU compatible packet frame</returns>
        public Packet ToPacket()
        {
            if (minSize > elements.Count)
            {
                // set final size
                elements.InsertRange(4, BitConverter.GetBytes(minSize));
                elements.RemoveRange(0, 4);

                return new Packet(this.StartAt(minSize).elements.ToArray(), true);
            }
            else
            {
                // set final size
                elements.InsertRange(4, BitConverter.GetBytes(elements.Count));
                elements.RemoveRange(0, 4);

                return new Packet(this.elements.ToArray(), true);
            }
        }

        /// <summary>
        /// Specify the Command of this chain.
        /// </summary>
        /// <param name="Command">The command as byte[4]</param>
        /// <returns></returns>
        public Chain Command(byte[] Command)
        {
            if (Command.Count() != 4)
                throw new ArgumentOutOfRangeException("Command", Command, "Length of Array must be exact 4 bytes long");
            // actual implementation is hidden inside a dictionary :)
            elements[4] = Command[0];
            elements[5] = Command[1];
            elements[6] = Command[2];
            elements[7] = Command[3];
            return this;
        }

        // Methods
        /// <summary>
        /// Add a value to the chain.
        /// </summary>
        /// <typeparam name="T">Any value type or byte array.</typeparam>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public Chain Add<T>(T item)
        {
            // actual implementation is hidden inside a dictionary :)
            elements.AddRange(SwitchDictionary[typeof(T)].Invoke(item));
            return this;
        }

        /// <summary>
        /// Add a value to the chain.
        /// </summary>
        /// <typeparam name="T">Any value type or byte array.</typeparam>
        /// <param name="item">The item.</param>
        /// <param name="minimumSize">Minimum size of this element.
        /// Adds a somewhat "padding" effect as the chain gets extended by this if the item parameter is shorter than this parameter.</param>
        /// <returns></returns>
        public Chain Add<T>(T item, int minimumSize)
        {
            // get a list of that
            List<byte> temp = SwitchDictionary[typeof(T)].Invoke(item).ToList();

            // pad the list
            if (temp.Count < minimumSize)
                temp.AddRange(new byte[minimumSize-temp.Count]);

            // add it
            elements.AddRange(temp);

            // done :D
            return this;
        }

        /// <summary>
        /// Overwrites a specific area in the chain
        /// </summary>
        /// <typeparam name="T">Any value type or byte array.</typeparam>
        /// <param name="item">The item.</param>
        /// <param name="offset">The offset.</param>
        /// <returns></returns>
        public Chain Overwrite<T>(T item, int offset)
        {
            // check if offset is negative
            if (offset < 0)
                throw new ArgumentOutOfRangeException("offset", "Argument cannot be less than 0");

            // grab temporary list
            List<byte> temp = SwitchDictionary[typeof(T)].Invoke(item).ToList();

            // check if the new content even fits in here
            if (offset + temp.Count > elements.Count)
                throw new IndexOutOfRangeException("Source array size + offset exceeded destinations size. This is 'Overwrite' afterall");

            // replace it
            elements.RemoveRange(offset, temp.Count);
            elements.InsertRange(offset, temp);

            // yay :D
            return this;
        }

        /// <summary>
        /// Pads by bytes.
        /// </summary>
        /// <param name="count">Amount to pad in bytes</param>
        /// <returns></returns>
        public Chain Pad(int count)
        {
            elements.AddRange(new byte[count]);
            return this;
        }

        /// <summary>
        /// Jumps to a specific position in the chain.
        /// Note: You can only jump forward. Use .Overwrite() to replace existing bytes
        /// </summary>
        /// <param name="offset">The offset.</param>
        /// <returns></returns>
        public Chain StartAt(int offset)
        {
            // check if we already did exceeded that point
            if (offset < elements.Count)
                throw new ArgumentOutOfRangeException("offset", offset, "This offset was already written");

            // just a failsafe in case offset and actual size are the same
            if (offset == elements.Count)
                return this;

            // everything is okay, now lets add bytes to them :)
            elements.AddRange(new byte[offset - elements.Count]);

            return this;
        }
    }
}
