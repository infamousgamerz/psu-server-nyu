﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading;
using MultithreadHelper;

namespace GameServer_R
{
    class Program
    {
        public static Logger log;
        public static Server server;
        public static GameClientManager gameClientManager;
        public static SceneManager sceneManager;
        public static Packet dummyPacket = new Packet();

        static void Main(string[] args)
        {
            // initialize logger
            log = new Logger();
            log.Add("Private Universe - " + Assembly.GetExecutingAssembly().GetName().Name + " - " + Assembly.GetExecutingAssembly().GetName().Version.ToString(), Logger.LogLevel.Important);

            // load server configuration
            //TODO: Add code to load configurations

            ThreadPool.SetMinThreads(200, 50);
            log.Add("Configuration loaded");

            // add commands
            CommandParser.SetupCommands();

            // load content files (the whole data directory)
            //TODO: Add code to load content data

            // initialize the GameClient manager
            gameClientManager = new GameClientManager();
            log.Add("Initialized GameClient Manager");

            // initialize the Scene Manager
            sceneManager = new SceneManager();
            sceneManager.ReloadSettings();
            log.Add("Initialized Scene Manager");


            /*// initialize CLI for manually controlling the server as an administrator
            SetupConsoleEvents();
            //TODO: add actual code to do this

            // Open the server and attach the CLI to the main thread
            GameClient dummy = new GameClient(0x1234);
            while (shouldRun == true)
            {
                try
                {
                    Console.Write("ADMIN>");
                    string input = Console.ReadLine();
                    CommandParser.ParseCommand(input, new CommunicationHelper(), dummy);
                }
                catch (Exception e)
                {
                    log.Add(e.ToString(), Logger.LogLevel.Error);
                }
            }*/

            // initialize server and start listening for incoming connections
            server = new Server();
            log.Add("Initialized network");

            // handle shutdown
            InitializeShutdown();

            // and we are done :3
        }

        private static void SetupConsoleEvents()
        {
            // the magic closing handle
            Console.CancelKeyPress += delegate { shouldRun = false; };
        }

        public static bool shouldRun = true;

        private static void InitializeShutdown()
        {
            log.Add("Initializing Shutdown...", Logger.LogLevel.Important);

            // dont accept new connections
            try
            {
                Server.tcpListener.Stop();
                // tell everyone on the server that they are going to get disconnected in 30 seconds
                foreach (var client in Server.connectedClients)
                {
                    Server.SendBroadcastMessage(client, "Warning! This server is going to shut down in 30 seconds!", BroadcastMessageType.RunningText);
                }

                // sleep for 30 seconds
                //Thread.Sleep(30000);

                log.Add("Dropping clients", Logger.LogLevel.Important);
                // disconnect everyone
                var temp = Server.connectedClients.ToArray();
                foreach (var client in temp)
                {
                    log.Add("Processing " + client.UserIDString);
                    // packet which makes the client to disconnect
                    try
                    {
                        client.DisconnectClient();
                    }
                    catch
                    {
                        log.Add(client.UserIDString + " does not want to disconnect.", Logger.LogLevel.Error);
                    }
                }
                log.Add("All clients dropped, waiting for disconnect", Logger.LogLevel.Important);
                Thread.Sleep(10000);
                while (Server.connectedClients.Count > 0)
                    Thread.Sleep(1000);
                log.Add("Successfully shutdown.", Logger.LogLevel.Important);
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR DURING SHUTDOWN!!!" + Environment.NewLine + e.ToString());
            }

        }
    }
}
