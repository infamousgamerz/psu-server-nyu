﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameServer_R
{
    public struct PartnerCard
    {
        // fields
        private uint userID;
        private string username;
        private string comment;

        // constructor
        public PartnerCard(uint newUserID, string newUsername, string newComment)
        {
            // filling out the fields
            userID = newUserID;
            username = newUsername;
            comment = newComment;
        }

        // properties
        public uint UserID { get { return userID; } }
        public string Username { get { return username; } set { username = value; } }
        public string Comment { get { return comment; } set { comment = value; } }

        // special properties
        // - Preformatted card
        public byte[] Formatted
        {
            get
            {
                // create buffer
                byte[] temp = new byte[0x1D4];

                // fill in fields
                Array.Copy(UnicodeEncoding.Unicode.GetBytes(username), 0, temp, 0, UnicodeEncoding.Unicode.GetByteCount(username)); // username
                Array.Copy(BitConverter.GetBytes(userID), 0, temp, 0x48, 4);
                Array.Copy(UnicodeEncoding.Unicode.GetBytes(comment), 0, temp, 0x68, UnicodeEncoding.Unicode.GetByteCount(comment)); // comment
                Array.Copy(new byte[4] { 0x01, 0x04, 0x01, 0x03 }, 0, temp, 0x1C8, 4);
                // return result
                return temp;
            }
        }
    }
}
