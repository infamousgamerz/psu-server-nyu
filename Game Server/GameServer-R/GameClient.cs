﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace GameServer_R
{
    public class GameClient
    {
        // Constructor
        public GameClient(uint NewUserID)
        {
            // set userid to this
            userID = NewUserID;
            Program.log.Add("Client " + UserIDString + " authenticated, loading Data");
            // load account data

            if (userID == 0x1234)
                status = AccountStatus.Administrator;
            else
                LoadAccountData();
            Program.log.Add(UserIDString + " loaded Data successfully");
        }

        // Most important thing: STATE HANDLER!!!!
        private GameClientState clientState = GameClientState.InitialSetup;
        public GameClientState ClientState { get { return clientState; } set { clientState = value; } }
                
        // Important handle
        private uint userID;
        public uint UserID { get { return userID; } }
        public string UserIDString { get { return UserID.ToString("D8"); } }

        // Partnercard
        private PartnerCard card;
        public PartnerCard Card { get { return card; } }

        // account status
        private AccountStatus status = AccountStatus.User;
        public AccountStatus Status { get { return status; } }

        private bool invisible = false;
        public bool Invisible { get { return invisible; } set { invisible = value; } }

        // special notice flag
        public bool SpecialNoticeSent = false;


        // Fields about the client itself
        // 0x00 PS2, 0x01 PC
        private ClientSystem system;
        public ClientSystem System { get { return system; } }

        // 0x00 Japanese, 0x01 English, 0x03 French, 0x04 German
        private ClientLanguage language;
        public ClientLanguage Language { get { return language; } }

        // 0x00 checks enabled, 0x01 checks disabled
        private ClientRuntimeChecks clientChecks;
        public ClientRuntimeChecks ClientChecks { get { return clientChecks; } }

        // PC Only Fields
        private string gpu = "n/a";
        public string GPU { get { return gpu; } }
        private string cpu = "n/a";
        public string CPU { get { return cpu; } }


        // Current Scene
        public byte[] CurrentScene = new byte[8];

        public int SpawnSlot = 0;

        public bool IsSpawning { get; set; }



        public enum ClientSystem : byte
        {
            PS2 = 0x00,
            PC = 0x01
        }
        public enum ClientLanguage : byte
        {
            Japanese = 0x00,
            English = 0x01,
            French = 0x03,
            German = 0x04
        }
        public enum ClientRuntimeChecks : byte
        {
            NoState = 0x00,
            Deactivated = 0x01
        }

        // === ClientData xml stuff
        public XDocument clientData;

        private string username;
        public string Username { get { return username; } }

        private int selectedCharacterID = -1;
        public int SelectedCharacterID { get { return selectedCharacterID; } set { selectedCharacterID = value; } }

        public CharacterData currentCharacterData;
        public CharacterData CurrentCharacterData { get { return currentCharacterData; } set { currentCharacterData = value; } }

        private void LoadAccountData()
        {
            // check if already loaded
            if (clientData != null)
                return;

            // load account data from <userID>.xml
            if (File.Exists("data\\accounts\\" + UserIDString + ".xml"))
                clientData = XDocument.Load("data\\accounts\\" + UserIDString + ".xml");
            else
                clientData = XDocument.Load("data\\accounts\\default.xml");


            username = (string)clientData.Descendants("username").First().Value;

            // set account status
            if (clientData.Root.Element("status") !=null)
                switch (clientData.Root.Element("status").Value)
                {
                    case "administrator":
                        status = AccountStatus.Administrator;
                        break;
                    case "gamemaster":
                        status = AccountStatus.GameMaster;
                        break;
                    case "hacker":
                        status = AccountStatus.Hacker;
                        break;
                    default:
                        status = AccountStatus.User;
                        break;
                }

            // load partner cards
            LoadPartnerCards(clientData.Root.Element("partnercards"));

            // Set own partnercard
            card = new PartnerCard(userID, "none", "");


            // DEBUG CODE!!!
            // Fill in temporary PartnerCards
            /*PartnerCards.Add(new PartnerCard(1, "test", "IMATEST"));
            PartnerCards.Add(new PartnerCard(2, "test2", "IMAawfdasfaTEST"));
            PartnerCards.Add(new PartnerCard(3, "test3", "IMdsxdvfzxsdfvsefATEST"));
            PartnerCards.Add(new PartnerCard(4, "test4", "IMATEasdaST"));
            PartnerCards.Add(new PartnerCard(5, "test5", "IMATasdasdasdEST"));
            PartnerCards.Add(new PartnerCard(6, "test6", "IMATsxgsdrgbdsrgbhsdgd  edr ge se te seEST"));
            PartnerCards.Add(new PartnerCard(7, "test7", "IMsg er ery es e rtw wATEST"));
            PartnerCards.Add(new PartnerCard(8, "test8", "IMAafdaswfrwaserTEST"));
            PartnerCards.Add(new PartnerCard(9, "test9", "IMAsedfsegdsgbdrghedrgsdgbvdsrgdergsegTEST"));*/
        }

        public void SaveAccountData()
        {
            // prepare the new data
            XDocument document = new XDocument(
                new XElement("account",

                    new XElement("userid", userID),
                    new XElement("username", username),
                    new XElement("status", status.ToString().ToLower()),

                    // insert time tracking stuff here //lastlogin (in unix time please)

                    // still to be done
                    new XElement("settings",""), 
 
                    // shortcuts
                    new XElement("shortcuts",
                        new XElement("shortcut",new XAttribute("key",1),new XAttribute("message","test"))
                        ),

                    // mails
                    new XElement("mails",""), 

                    // character data, not much has to be done here, its already getting changed at that part up there
                    clientData.Root.Element("characters"), 

                    // item storage (currently only planned as global
                    new XElement("accountstorageitems",new XAttribute("max",100),new XElement("item",new XAttribute("name","Trimate"),new XAttribute("type","item"),new XAttribute("quantity",1))),

                    // Partnercards (its just a method :3)
                    SavePartnerCards()

                    // DONE!!! :3
                    )
                );

            // and save it
            document.Save("data\\accounts\\" + UserIDString + ".xml");
            Program.log.Add(UserIDString + "'s data saved!");
            // was that so hard? i think not :3
        }

        public void LoadCharacterData()
        {
            var temp = from chardata in clientData.Descendants("characters").First().Descendants("character")
                       where (int)chardata.Attribute("id") == selectedCharacterID
                       select chardata;

            currentCharacterData = new CharacterData(0x01);
            currentCharacterData.DeserializeXML(temp.First());

            // set partnercard from loaded data
            card.Username = currentCharacterData.CharacterName;
        }

        // PartnerCard
        public void SetNewPartnerCardComment(string newComment)
        {
            card.Comment = newComment;
        }

        public PartnerCard? TemporaryPartnerCard = null;

        private XElement SavePartnerCards()
        {
            XElement temp = new XElement("partnercards");

            foreach (var PartnerCard in PartnerCards)
            {
                temp.Add(new XElement("partnercard", 
                    new XAttribute("userid", PartnerCard.UserID), 
                    new XAttribute("username", PartnerCard.Username), 
                    new XAttribute("type","player"),
                    new XAttribute("description", PartnerCard.Comment), 
                    new XAttribute("comment", "changeme")
                    ));
            }

            return temp;
        }

        private void LoadPartnerCards(XElement workingItem)
        {
            // check if there is any item in that
            if (workingItem.Descendants("partnercard").Count() > 0)
            {
                foreach (var item in workingItem.Descendants("partnercard"))
                {
                    PartnerCard card = new PartnerCard((uint)item.Attribute("userid"), (string)item.Attribute("username"), (string)item.Attribute("description"));
                    PartnerCards.Add(card);
                }
            }
        }

        // Lobby State
        public byte[] PlayerState = new byte[0x10];

        // Partner Cards
        public List<PartnerCard> PartnerCards = new List<PartnerCard>();
    }

    [Flags]
    public enum AccountStatus : uint
    {
        
        Administrator =     0x0008,
        Developer =         0x0004,
        GameMaster =        0x0002,
        Hacker =            0x0001,
        User =              0x0000

    }

    public static class EnumExtensions
    {
        public static bool IsSet<T>(this T value, T flags) where T : struct
        {
            Type type = typeof(T);

            // only works with enums  
            if (!type.IsEnum) throw new ArgumentException(
                "The type parameter T must be an enum type.");

            // handle each underlying type  
            Type numberType = Enum.GetUnderlyingType(type);

            if (numberType.Equals(typeof(int)))
            {
                return BoxUnbox<int>(value, flags, (a, b) => (a & b) == b);
            }
            else if (numberType.Equals(typeof(sbyte)))
            {
                return BoxUnbox<sbyte>(value, flags, (a, b) => (a & b) == b);
            }
            else if (numberType.Equals(typeof(byte)))
            {
                return BoxUnbox<byte>(value, flags, (a, b) => (a & b) == b);
            }
            else if (numberType.Equals(typeof(short)))
            {
                return BoxUnbox<short>(value, flags, (a, b) => (a & b) == b);
            }
            else if (numberType.Equals(typeof(ushort)))
            {
                return BoxUnbox<ushort>(value, flags, (a, b) => (a & b) == b);
            }
            else if (numberType.Equals(typeof(uint)))
            {
                return BoxUnbox<uint>(value, flags, (a, b) => (a & b) == b);
            }
            else if (numberType.Equals(typeof(long)))
            {
                return BoxUnbox<long>(value, flags, (a, b) => (a & b) == b);
            }
            else if (numberType.Equals(typeof(ulong)))
            {
                return BoxUnbox<ulong>(value, flags, (a, b) => (a & b) == b);
            }
            else if (numberType.Equals(typeof(char)))
            {
                return BoxUnbox<char>(value, flags, (a, b) => (a & b) == b);
            }
            else
            {
                throw new ArgumentException("Unknown enum underlying type " +
                    numberType.Name + ".");
            }
        }

        /// <summary>  
        /// Helper function for handling the value types. Boxes the params to  
        /// object so that the cast can be called on them.  
        /// </summary>  
        private static bool BoxUnbox<T>(object value, object flags, Func<T, T, bool> op)
        {
            return op((T)value, (T)flags);
        }
    }

    public enum GameClientState
    {
        InitialSetup,
        CharacterSelection,
        Spawning,
        Idle
    }

    
}
