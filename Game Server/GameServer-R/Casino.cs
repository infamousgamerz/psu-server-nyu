﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace GameServer_R
{
    class Casino
    {
        private static Timer RouletteBetPeriodTimer;
        private static bool casinoStarted = false;
        private static byte winningNumber;

        public static void StartRoulette(CommunicationHelper helper, GameClient gameClient)
        {
            if (casinoStarted) return;  // Only run this once. First person to visit the casino sets it rolling

            Random random = new Random();
            Packet packet = new Packet();

            int RouletteState = 2;      // Start by sending the "Start Betting!" trigger
            uint startuptime = 10000;   // 10 second delay ensures casino loads before packet is sent

            RouletteBetPeriodTimer = new Timer(new TimerCallback((x) =>
            {

                switch (RouletteState)
                {
                    case 0:
                        // Triggers "No more bets, please" and starts the roll. Byte 0x34 contains winning number (0 to 36).
                        packet = new Packet(0x40, new byte[] { 0x1D, 0x07, 0x03, 0xBF });

                        packet.WriteUInt(7, 0x2C);                      // Casino sub-command ID?
                        packet.WriteUInt(0x0CB3, 0x30);                 // Unknown value
                        winningNumber = (byte)random.Next(0, 37);       // Select a random winning number
                        packet.WriteByte(winningNumber, 0x34);          // Set winning number so client can pace the spinning properly
                        RouletteBetPeriodTimer.Change(20000, 20000);    // Wait 20 seconds for spinning to stop
                        RouletteState = 1;                              // Stop spin on next cycle

                        Console.WriteLine("Winning number is: " + winningNumber);
                        break;

                    case 1:
                        // This packet resends the winning number and forces the spin to end. Probably used to keep the clients synced up.
                        packet = new Packet(0x40, new byte[] { 0x1D, 0x07, 0x03, 0xBF });

                        packet.WriteUInt(8, 0x2C);                      // Casino sub-command ID?
                        packet.WriteUInt(0x09EA, 0x30);                 // Unknown value
                        packet.WriteByte(winningNumber, 0x34);          // Re-send winning number
                        RouletteBetPeriodTimer.Change(10000, 10000);    // Wait 10 seconds before starting the next betting cycle
                        RouletteState = 2;                              // Restart betting on next cycle

                        Console.WriteLine("Again, winning number is: " + winningNumber);
                        break;

                    case 2:
                        // Triggers "Start betting" phase and countdown.
                        packet = new Packet(0x40, new byte[] { 0x1D, 0x07, 0x03, 0xBF });

                        packet.WriteUInt(6, 0x2C);                      // Casino sub-command ID?
                        packet.WriteUInt(0x0CB3, 0x30);                 // Unknown value
                        packet.WriteByte(36, 0x34);                     // Normally the winning number, but always 36 in this phase
                        //packet.WriteUInt(0x03, 0x38);                   // Unknown value
                        //packet.WriteUInt(0x02, 0x3C);                   // Unknown value
                        RouletteBetPeriodTimer.Change(59500, 59500);    // Wait just under 1 minute for countdown before starting the spin
                        RouletteState = 0;                              // Start the spin on the next cycle
                        break;
                }

                // Broadcast the packet modified above
                Program.sceneManager.RetrieveScene(gameClient.CurrentScene).PlayerBroadcastEvent(null, new PlayerEventArgs(helper, gameClient, packet));

            }), null, startuptime, startuptime);


            casinoStarted = true;
        }





    }
}
