﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MultithreadHelper;
using System.Threading.Tasks;

namespace GameServer_R
{
    partial class Server
    {
        // important values
        public static int MaximumPacketSize = 10240;

        // methods for splitting up packets while sending
        public static void SendPacket(Packet packet, uint userID)
        {
            // retrieve sslstream object from userID
            CommunicationHelper helper = connectedClients.Single(x => x.UserID == userID);

            SendPacket(packet, helper);
        }

        public static void SendPacket(Packet packet, CommunicationHelper helper)
        {
            //DebugPacketData(packet);

            DebugPacketData(packet);
            Program.log.Add("<-----   " + packet.ReadByte(0x04).ToString("X2") + packet.ReadByte(0x05).ToString("X2") + packet.ReadByte(0x06).ToString("X2"));
            

            if (packet.PacketSize >= 0x2C)
            {
                // apply this line in the actual packets! This is the source of the request so that should not be done globally ;)
                //packet.WriteUInt(helper.UserID, 0x10);  //Sometimes this is here. I wonder if it breaks
                                                        //anything if it's present but doesn't need to be? -Smidge

                packet.WriteQuadByte(new byte[4] { 0x00, 0x01, 0x13, 0x00 }, 0x1C);
                packet.WriteUInt(helper.UserID, 0x20); // destination the packet
            }

            

            // lock everything now :3
            try
            {
                    lock (helper.SendLock)
                    {
                        object locker = new object();

                        //Console.WriteLine(BitConverter.ToString(packet.RawPacket));
                        // check if packet size is bigger than the maximum allowed split packet size
                        if (packet.PacketSize > MaximumPacketSize)
                        {
                            // split up packet and send it
                            for (int i = 0; i < packet.PacketSize; i += MaximumPacketSize)
                            {
                                int tempSize = 0;

                                if ((packet.PacketSize - i) < MaximumPacketSize)
                                    tempSize = (int)packet.PacketSize - i;
                                else
                                    tempSize = MaximumPacketSize;

                                Packet tempPacket = new Packet((uint)tempSize + 0x10, new byte[4] { 0x0B, 0x03, 0x00, 0x00 });

                                tempPacket.WriteUInt((uint)packet.PacketSize, 0x08);
                                tempPacket.WriteUInt((uint)i, 0x0C);

                                tempPacket.WriteByteArray(packet.ReadByteArray(i, tempSize), tempSize, 0x10);

                                // reset the wait handle
                                helper.waitHandle.Reset();

                                // write to the client
                                Safe.Lock(locker, 30000, () =>{
                                    helper.sslStream.Write(tempPacket.RawPacket, 0, (int)tempPacket.PacketSize);
                                });

                                // do a beginread to fix the problem which follows
                                //if (tempSize == MaximumPacketSize)
                                //helper.sslStream.BeginRead(helper.Buffer, 0, 2048, new AsyncCallback(BeginReadCallback), helper);


                                // wait until the client has received the data
                                /*if (!helper.waitHandle.WaitOne(10000))
                                    Program.log.Add(helper.UserIDString + " did not answer SplitPacket. Assuming data was fully received.");*/
                            }
                        }
                        else
                        {
                            // send without any modification
                            Safe.Lock(locker, 30000, () =>
                            {
                                helper.sslStream.Write(packet.RawPacket);
                            });
                        }
                    }
            }
            catch (TimeoutException e)
            {
                Program.log.Add(helper.UserIDString + " Timeout Exception " + helper.waitHandle.ToString() + Environment.NewLine + e.ToString(), Logger.LogLevel.Error);
            }
            catch (Exception e)
            {
                Program.log.Add(helper.UserIDString + " Generic Exception " + Environment.NewLine + e.ToString(), Logger.LogLevel.Error);
            } // ignore errors if they happen in here for now :D only possible errors are when the client has disconnected or some silly things like this :O

        }

        delegate void MethodDelegate(Packet packet, CommunicationHelper helper);

        public static void SendPacketAsync(Packet packet, uint userID)
        {
            Task.Factory.StartNew(() => { SendPacket(packet, userID); });
            //MethodDelegate temp = new MethodDelegate(SendPacket);
            //temp.BeginInvoke(packet, connectedClients.Single(x => x.UserID == userID), null, null);
        }

        public static void SendPacketAsync(Packet packet, CommunicationHelper helper)
        {
            Task.Factory.StartNew(() => { SendPacket(packet, helper); });
            //MethodDelegate temp = new MethodDelegate(SendPacket);
            //temp.BeginInvoke(packet, helper, null, null);
        }
    }
}
