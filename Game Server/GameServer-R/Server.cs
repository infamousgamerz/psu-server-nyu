﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Security.Authentication;
using System.Net;
using System.Threading;
using System.Xml.Linq;
using System.Diagnostics;
using System.Threading.Tasks;

namespace GameServer_R
{
    partial class Server
    {
        // Global objects only accessible through this server object through public methods
        public static TcpListener tcpListener;

        // List containing a list of direct connections associated using the connection number (maximum number of actually made connections is at the maximum value of an UINT)
        public static List<CommunicationHelper> connectedClients = new List<CommunicationHelper>();
        private static uint connectionID = 0;

        // Certificate for authentication
        private static X509Certificate serverCertificate;

        public Server()
        {
            Program.log.Add("Initializing network");

            // load certificate
            if (serverCertificate == null)
                serverCertificate = new X509Certificate2("certificate.pfx", "alpha");
            Program.log.Add("Loaded certificate");

            DefinePackets();
            Program.log.Add("Initialized Packet Definitions from the Server class");

            // start listening to new clients
            tcpListener = new TcpListener(IPAddress.Any, 12231); // make this more dynamic through configuration files
            tcpListener.Start();
            //tcpListener.BeginAcceptTcpClient(new AsyncCallback(BeginListeningCallback), tcpListener);

            StartRegularPingCheck();


            // start listening loop
            while (true)
            {
                // wait for new clients and accept them
                TcpClient client = tcpListener.AcceptTcpClient();

                // pass the rest as a task
                Task.Factory.StartNew(() => ConnectionLoop(client), TaskCreationOptions.LongRunning);
            }
        }

        public void BeginListeningCallback(IAsyncResult AsyncCall)
        {
            try
            {
                // Recieve Listener from parameter
                TcpListener tcpListener = (TcpListener)AsyncCall.AsyncState;

                // Objects for this method
                TcpClient tcpClient = tcpListener.EndAcceptTcpClient(AsyncCall);

                Program.log.Add("A new client has connected");

                // create SslStream
                SslStream sslStream = new SslStream(tcpClient.GetStream(), false);
                // set timeouts
                sslStream.ReadTimeout = 30000;
                sslStream.WriteTimeout = 30000;

                // authenticate as server
                sslStream.AuthenticateAsServer(serverCertificate, false, SslProtocols.Default, false); // Has to be SSL3, psu does not accept anything else

                // create a new communication helper instance
                CommunicationHelper helper = new CommunicationHelper(sslStream);

                // add client to the list
                connectedClients.Add(helper);

                // start reading from the client
                sslStream.BeginRead(helper.Buffer, 0, 2048, new AsyncCallback(BeginReadCallback), helper);

                // send a server hello
                Packet serverHello = new Packet((uint)176, new byte[4] { 0x02, 0x02, 0x03, 0x00 });
                serverHello.WriteUInt(helper.ConnectionID, 0x2C);
                helper.sslStream.Write(serverHello.RawPacket);

                // accept more clients now :D
                tcpListener.BeginAcceptTcpClient(new AsyncCallback(BeginListeningCallback), tcpListener);
            }
            catch (Exception e)
            {
                Program.log.Add(e.Message, Logger.LogLevel.Error);
            }
        }
        
        public void ConnectionLoop(TcpClient tcpClient)
        {
            using (SslStream sslStream = new SslStream(tcpClient.GetStream(),false))
            {
                Program.log.Add("A new client has connected");
                try
                {
                    // set timeouts
                    sslStream.ReadTimeout = 30000;
                    sslStream.WriteTimeout = 30000;

                    // authenticate as server
                    sslStream.AuthenticateAsServer(serverCertificate, false, SslProtocols.Default, false); // Has to be SSL3, psu does not accept anything else

                    // create a new communication helper instance
                    CommunicationHelper helper = new CommunicationHelper(sslStream);

                    // add client to the list
                    connectedClients.Add(helper);

                    /*// start reading from the client
                    sslStream.BeginRead(helper.Buffer, 0, 2048, new AsyncCallback(BeginReadCallback), helper);*/

                    try
                    {
                        // send a server hello
                        Packet serverHello = new Packet((uint)176, new byte[4] { 0x02, 0x02, 0x03, 0x00 });
                        serverHello.WriteUInt(helper.ConnectionID, 0x2C);
                        helper.sslStream.Write(serverHello.RawPacket);

                        // server hello sent, begin the read loop
                        while (ReadLoop(helper))
                            Thread.Sleep(100); // actually some code should be put in here :O
                    }
                    catch(Exception e)
                    {
                        //connectedClients.Remove(helper); // this is important!
                        throw e;
                    }

                    DisconnectClient(helper);
                    //connectedClients.Remove(helper); // this is important!
                }
                catch (Exception e)
                {
                    Program.log.Add("Error inside ConnectionLoop()" + Environment.NewLine + 
                        e.Message, Logger.LogLevel.Error);
                }

            }

        }

        public void BeginReadCallback(IAsyncResult AsyncCall)
        {
            // retrieve helper to work with
            CommunicationHelper helper = (CommunicationHelper)AsyncCall.AsyncState;
            SslStream sslStream = helper.sslStream;
            // initialize variables
            int bytesRead = 0;

            try
            {
                // retrieve packet
                bytesRead = sslStream.EndRead(AsyncCall);

                // check if client has disconnected
                if (bytesRead > 0)
                {
                    // copy buffer to a temporary one
                    var temporaryBuffer = helper.Buffer;
                    Array.Resize(ref temporaryBuffer, bytesRead);

                    // read more data
                    sslStream.BeginRead(helper.Buffer, 0, 2048, new AsyncCallback(BeginReadCallback), helper);

                    // client is still connected, read data from buffer
                    ProcessPacket(temporaryBuffer, temporaryBuffer.Length, helper);
                }
                else
                {
                    // client disconnected, do everything to disconnect the client
                    DisconnectClient(helper);
                }
            }
            catch (Exception e)
            {
                // encountered an error, closing connection
                Program.log.Add(e.ToString(), Logger.LogLevel.Error);
                DisconnectClient(helper);
            }
        }

        /// <summary>
        /// Reads the loop.
        /// </summary>
        /// <param name="helper">Direct link to a Client.</param>
        /// <returns>Returns false if Client has disconnected (either by itself or due to an error) otherwise true.</returns>
        public bool ReadLoop(CommunicationHelper helper)
        {
            // initialize variables
            int bytesRead = -1;

            try
            {
                // retrieve packet
                bytesRead = helper.sslStream.Read(helper.Buffer, 0, 2048);

                // check if client has disconnected
                if (bytesRead > 0)
                {
                    // copy buffer to a temporary one
                    var temporaryBuffer = helper.Buffer;
                    Array.Resize(ref temporaryBuffer, bytesRead);

                    // client is still connected, read data from buffer
                    ProcessPacket(temporaryBuffer, temporaryBuffer.Length, helper);
                }
                else
                {
                    return false;

                    // client disconnected, do everything to disconnect the client
                    DisconnectClient(helper);
                }

                return true;
            }
            catch (Exception e)
            {
                // encountered an error, closing connection
                Program.log.Add(e.ToString(), Logger.LogLevel.Error);
                //DisconnectClient(helper);
            }
            return false;
        }


        private void ProcessPacket(byte[] buffer, int bytesRead, CommunicationHelper helper)
        {
            int processedbytes = 0;

            while (processedbytes < bytesRead)
            {
                processedbytes += ParsePacket(new Packet(buffer, processedbytes), helper);
            }
        }



        public void DisconnectClient(CommunicationHelper helper)
        {
            try
            {
                Program.log.Add("Client " + helper.UserID.ToString("D8") + " disconnected");

                // TODO: add some more remove handling like removing player from lobby
                // get gameClient object
                GameClient gameClient = Program.gameClientManager.RetrieveGameClient(helper.UserID);

                try
                {                
                    // do more removal things like saving
                    gameClient.SaveAccountData();

                    // remove from lobbies

                    Program.sceneManager.RetrieveScene(gameClient.CurrentScene).PlayerLeftEvent(null, new PlayerEventArgs(helper, gameClient, default(Packet)));


                    // remove from GameClient Manager
                    Program.gameClientManager.RemoveGameClient(gameClient);
                }
                catch { }

                // remove helper from list
                connectedClients.Remove(helper);
            }
            catch (Exception e)
            {
                Trace.WriteLine(
                    "Error while disconnecting" + Environment.NewLine + 
                    e.ToString()
                    );
            }
            finally
            {
                // finally destroy object
                helper = null;
            }
        }

        private static Timer periodicPingCheckTimer;

        public static void StartRegularPingCheck()
        {
            periodicPingCheckTimer = new Timer(new TimerCallback((x) =>
            {
                Packet packet = new Packet((uint)0x2C, new byte[4] { 0x02, 0x0c, 0x02, 0x00 });
                var clients = connectedClients.Where(y => Program.gameClientManager.RetrieveGameClient(y.UserID).ClientState == GameClientState.Idle || Program.gameClientManager.RetrieveGameClient(y.UserID).ClientState == GameClientState.CharacterSelection).ToArray();
                foreach (var client in clients)
                {

                    try
                    {
                        client.SendPacketAsync(packet);
                    }
                    catch { }

                }
            }), null, 25000, 25000);
        }


    }

    public class CommunicationHelper
    {
        public SslStream sslStream;
        private uint connectionID = 0;
        public uint ConnectionID { get { return connectionID; } } // read only because it should NEVER change
        private uint userID = 0;
        public uint UserID { get { return userID; } } // read only because it should NEVER change
        public string UserIDString { get { return userID.ToString("D8"); } }

        public byte[] Buffer = new byte[2048];


        // lock for sending things
        public object SendLock = new object();
        public object ProcessingLock = new object();
        public EventWaitHandle waitHandle = new ManualResetEvent(false);

        private static uint runningID = 1;

        public CommunicationHelper(SslStream stream)
        {
            sslStream = stream;
            connectionID = runningID++;
        }

        public CommunicationHelper()
        {
            connectionID = 0;
            userID = 0x1234;
        }

        public void SetUserID(uint newID)
        {
            // method to change the UserID ONCE!!!
            if (UserID == 0)
            {
                userID = newID;
            }
            else
            {
                throw new Exception("You are doing it wrong! One simply cannot change the ID into mordor!");
                // maybe mark as a hacker if someone attempts this :3
            }
        }

        public void SendPacketAsync(Packet input)
        {
            Server.SendPacketAsync(input, this);
        }
        public void SendPacket(Packet input)
        {
            Server.SendPacket(input, this);
        }

        public void DisconnectClient()
        {
            this.sslStream.Close();

        }

    }
}
