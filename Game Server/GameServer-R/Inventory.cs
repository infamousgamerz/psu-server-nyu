﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

// Item and inventory based data and functions

namespace GameServer_R
{
    public class Inventory
    {
        public static List<Item> InventoryList = new List<Item>();
        public static int ItemCount;
        public static uint MySeedNumber;
        public static int MaxItems = 60; // Maybe more for other inventory lists

        

        public Item GetItem(byte ItemID)
        {
            IEnumerable<Item> selectedItem =
                 from temp in ItemDB.MainItemList
                 where temp.ItemID == ItemID
                 select temp;

                 return selectedItem.First();
        }

        public Packet GeneratePacket_0A0A(uint UserID, uint SeedNumber)
        {
            // Sneaky!
            Item tempitem = new Item();
            tempitem.forceitem();
            InventoryList.Add(tempitem);

            MySeedNumber = SeedNumber;

            Packet packet = new Packet(0x23D8, new byte[] { 0x0A, 0x0A, 0x03, 0x00 });
            packet.WriteUInt(0x0000FFFF, 0x08);
            packet.WriteQuadByte(new byte[4] { 0x00, 0x01, 0x13, 0x00 }, 0x1C);
            packet.WriteUInt(UserID, 0x20);

            
            packet.WriteByte((byte)InventoryList.Count, 0x2C);

            packet.WriteByte(0x06, 0x2E); // Unknown?

            int j = 0;
            foreach (var i in InventoryList)
            {
                i.ItemID = (uint)(SeedNumber + j);
                packet.WriteByteArray(i.GetDataA, 0x174 + j*36);
                packet.WriteByteArray(i.GetDataB, 0x9E0 + j*72);
                j++;
            }

            //for (int i = 60 - InventoryList.Count; i < 60; i += 4)
            //    packet.WriteUInt(0xFFFFFF, i * 4);

            // ...and a whole bunch of unknown shit at the end!
            packet.WriteByteArray(new byte[] {
                0x64, 0x00, 0x64, 0x00, 0x05, 0x00, 0x04, 0x00, 0x04, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00,
                0x00, 0x00, 0x70, 0x17, 0x32, 0x00, 0x92, 0x09, 0x44, 0x04, 0x89, 0x08, 0x64, 0x00, 0x64, 0x00,
                0x05, 0x00, 0x03, 0x00, 0x09, 0x02, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x03, 0x00, 0x44, 0x16,
                0x37, 0x00, 0x5A, 0x05, 0x9F, 0x14, 0xAB, 0x0A, 0x64, 0x00, 0x64, 0x00, 0x05, 0x00, 0x01, 0x08,
                0x08, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x40, 0x1F, 0x3C, 0x00, 0x88, 0x09,
                0x44, 0x04, 0x89, 0x08, 0x64, 0x00, 0x64, 0x00, 0x0A, 0x00, 0x0A, 0x00, 0x0F, 0x04, 0x00, 0x00,
                0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x7D, 0x00, 0x5E, 0x01, 0x84, 0x03, 0xE4, 0x18, 0x39, 0x0E,
                0x64, 0x00, 0x64, 0x00, 0x05, 0x00, 0x04, 0x40, 0x0C, 0x02, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00,
                0x00, 0x00, 0x08, 0x07, 0x46, 0x00, 0x1C, 0x07, 0x66, 0x06, 0xAB, 0x0A, 0x64, 0x00, 0x64, 0x00,
                0x02, 0x00, 0x02, 0x00, 0x08, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x7C, 0x15,
                0x32, 0x00, 0x0E, 0x06, 0x66, 0x06, 0xAB, 0x0A, 0x64, 0x00, 0x64, 0x00, 0x02, 0x00, 0x01, 0x00,
                0x05, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x70, 0x17, 0x37, 0x00, 0x0E, 0x06,
                0xFA, 0x04, 0x17, 0x0C, 0x64, 0x00, 0x64, 0x00, 0x02, 0x00, 0x01, 0x00, 0x05, 0x01, 0x00, 0x00,
                0x00, 0x01, 0x00, 0x00, 0x01, 0x00, 0x50, 0x14, 0x3C, 0x00, 0x14, 0x05, 0x66, 0x06, 0x17, 0x0C,
                0x64, 0x00, 0x64, 0x00, 0x02, 0x00, 0x03, 0x00, 0x06, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00,
                0x02, 0x19, 0xF4, 0x01, 0x46, 0x00, 0x72, 0x06, 0x83, 0x0D, 0x00, 0x20, 0x64, 0x00, 0x64, 0x00,
                0x03, 0x00, 0x03, 0x00, 0x05, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00,
                0x41, 0x00, 0xC9, 0x03, 0xFA, 0x04, 0x89, 0x08, 0x55, 0x15, 0x00, 0x00, 0x64, 0x00, 0x19, 0x08,
                0x55, 0x15, 0x00, 0x00, 0x64, 0x00, 0x19, 0x08, 0x55, 0x15, 0x00, 0x00, 0x64, 0x00, 0x14, 0x08,
                0x55, 0x15, 0x00, 0x00, 0x64, 0x00, 0x14, 0x08, 0x00, 0x40, 0x00, 0x00, 0xC8, 0x00, 0x07, 0x0F,
                0x00, 0x80, 0x00, 0x00, 0x2C, 0x01, 0x1E, 0x0A, 0x55, 0x15, 0x00, 0x00, 0x64, 0x00, 0x1C, 0x08,
                0x55, 0x15, 0x00, 0x00, 0x64, 0x00, 0x1C, 0x08, 0x55, 0x15, 0x00, 0x00, 0x64, 0x00, 0x19, 0x08,
                0x55, 0x15, 0x00, 0x00, 0x64, 0x00, 0x19, 0x08, 0x00, 0x40, 0x00, 0x00, 0xC8, 0x00, 0x07, 0x0F,
                0x00, 0x80, 0x00, 0x00, 0x2C, 0x01, 0x1E, 0x0A, 0x02, 0x02, 0x12, 0x00, 0x00, 0x02, 0x02, 0x00,
                0x22, 0x12, 0x02, 0x00, 0x00, 0x20, 0x20, 0x00, 0x20, 0x00, 0x20, 0x22, 0x00, 0x00, 0x21, 0x00,
                0x00, 0x30, 0x30, 0x31, 0x40, 0x00, 0x40, 0x42, 0x30, 0x42, 0x42, 0x30, 0x00, 0x30, 0x00, 0x00,
                0x02, 0x00, 0x00, 0x40, 0x42, 0x30, 0x00, 0x41, 0x02, 0x02, 0x30, 0x40, 0x30, 0x33, 0x00, 0x30,
                0x40, 0x40, 0x33, 0x43, 0x30, 0x30, 0x00, 0x03, 0x03, 0x34, 0x00, 0x03, 0x04, 0x34, 0x04, 0x33,
                0x04, 0x00, 0x44, 0x04, 0x04, 0x34, 0x00, 0x04, 0x04, 0x03, 0x04, 0x34, 0x33, 0x04, 0x00, 0x03,
                0x40, 0x40, 0x00, 0x40, 0x40, 0x30, 0x43, 0x30, 0x00, 0x30, 0x00, 0x00, 0x40, 0x40, 0x43, 0x00,
                0x40, 0x00, 0x03, 0x00, 0x43, 0x03, 0x00, 0x03, 0x00, 0x00, 0x40, 0x44, 0x40, 0x40, 0x00, 0x43,
                0x00, 0x04, 0x00, 0x04, 0x04, 0x00, 0x03, 0x00, 0x00, 0x00, 0x03, 0x33, 0x03, 0x44, 0x44, 0x04,
                0x40, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x33, 0x44, 0x00, 0x43, 0x30, 0x00, 0x40, 0x04, 0x00,
                0x04, 0x04, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x40, 0x00, 0x00, 0x40,
                0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40,
                0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x44, 0x44,
                0x10, 0x27, 0x10, 0x27, 0x10, 0x27, 0x90, 0x01, 0x00, 0x22, 0x02, 0x32, 0x33, 0x34, 0x00, 0x30,
                0x03, 0x00, 0x40, 0x34, 0x00, 0x00, 0x44, 0x40
            }, 0x2180);


            //Server.DebugPacketData(packet);
            return packet;
        }

        public Packet GeneratePacket_0A06(uint UserID)
        {
            return GeneratePacket_0A06(UserID, MySeedNumber);
        }

        public Packet GeneratePacket_0A06(uint UserID, uint SeedNumber)
        {
            //GeneratePacket_0A0A(1);
            //SeedNumber = 1;
            Packet packet = new Packet((uint)0x11C, new byte[4] { 0x0A, 0x06, 0x03, 0x00 });

            packet.WriteQuadByte(new byte[4] { 0x00, 0x01, 0x13, 0x00 }, 0x1C);
            packet.WriteUInt(UserID, 0x10);
            packet.WriteUInt(UserID, 0x20);

            // Write numbers which increase over time. Each is a unique
            // ID used to reference that weapon. One number per slot.
            for (int i = 0; i < InventoryList.Count; i++)
                packet.WriteUInt((uint)i + SeedNumber, 0x2C + i * 4);

            // Fill unused slots with 0xFFFFFFFF
            for (int i = InventoryList.Count; i < 60;  i++)
                packet.WriteUInt((uint)0xFFFFFFFF, 0x2C + i * 4);

             //Server.DebugPacketData(packet);
             return packet;
        }

    }



    // Basic Item Class
    public class Item
    {
        // Main data storage
        private byte[] DataA = new byte[36];
        private byte[] DataB = new byte[72];

        // Internal use flags
        private bool isequipped = false;
        private Int16 atpvariance = 0;

        // Helper functions
        private void  SetUInt(byte[] storage, int offset, uint value)   { Array.Copy(BitConverter.GetBytes(value), 0, storage, offset, 4);   }
        private uint  GetUInt(byte[] storage, int offset)               { return BitConverter.ToUInt32(storage, offset);                     }

        private void  SetInt16(byte[] storage, int offset, Int16 value) { Array.Copy(BitConverter.GetBytes(value), 0, storage, offset, 2); }
        private Int16 GetInt16(byte[] storage, int offset)              { return BitConverter.ToInt16(storage, offset);                    }

        private void  SetByte(byte[] storage, int offset, byte value)   { storage[offset] = value;                                         }
        private byte  GetByte(byte[] storage, int offset)               { return storage[offset];                                          }

        public byte[] GetDataA { get { return DataA; } }
        public byte[] GetDataB { get { return DataB; } }

        // Constructors
        public Item()
        {
        }

        public void forceitem()
        {
            
            //DataA = new byte[] { 0x60, 0x55, 0x00, 0x00, 0x01, 0x09, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB4, 0x00, 0xB4, 0x00, 0x00, 0x00, 0x01, 0x41, 0x02, 0x0E, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            //DataB = new byte[] { 0xD6, 0x30, 0xE9, 0x30, 0xF3, 0x30, 0xC9, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0xA0, 0x00, 0x00, 0x00, 0xB4, 0x00, 0x8A, 0x00, 0x4A, 0x00, 0x25, 0x00, 0xFF, 0x8F, 0x3F, 0x02, 0x15, 0x01, 0x00, 0x01, 0x00, 0x0A, 0x00, 0x00 };

        }

        // In order as found in the packet data...
        //0x000			4 Bytes						Unique Item ID (Possibly used for future reference? Check equip/grind/etc packets!)
        //0x004			1 Byte						Item Class
        //0x005			1 Byte						Weapon/Item Icon - See Tables Below
        //0x006			1 Byte						Unknown
        //0x007			1 Byte						Manufacturer
        //0x008			1 Bytes						Quantity (stackable items only) - Item limits set by client
        //0x009			1 Byte						Unknown
        //0x00A			1 Byte						Unknown
        //0x00B			1 Byte						Unknown
        //0x00C			2 Bytes						Current Weapon PP
        //0x00E			2 Bytes						Maximum Weapon PP (w/ Grinds?)
        //0x010			2 Bytes						Weapon Att. Modifier (Added to Att from second data set)
        //0x012			1 Byte						Weapon rank icon (0=None, 1=C, 2=B, 3=A, 4=S) - Shows in list only!
        //0x013			1 Byte						Stars (0=1 star, 0x0E=15 stars max)
        //0x014			1 Byte						Element Type (Weapon/Shield) Or Level (PAs, 0=Lv 1)
        //0x015			1 Byte						Element % (Weapon/Shield)
        //0x016			1 Byte						Unknown
        //0x017			1 Byte						Unknown
        //0x018			4 Bytes						Unknown
        //0x01C			4 Bytes						Unknown
        //0x020			4 Bytes						Unknown

        public uint  ItemID
        {
            get { return GetUInt(DataA, 0x000); }
            set { SetUInt(DataA, 0x000, value); }
        }
        public byte  ItemClass
        {
            get { return GetByte(DataA, 0x004); }
            set { SetByte(DataA, 0x004, value); }
        }
        public byte  ItemIcon
        {
            get { return GetByte(DataA, 0x005); }
            set { SetByte(DataA, 0x005, value); }
        }
        public byte  ItemMaker
        {
            get { return GetByte(DataA, 0x007); }
            set { SetByte(DataA, 0x007, value); }
        }
        public byte  ItemCount
        {
            get { return GetByte(DataA, 0x008); }
            set { SetByte(DataA, 0x008, value); }
        }
        public Int16 ItemCurrentPP
        {
            get { return GetInt16(DataA, 0x00C); }
            set { SetInt16(DataA, 0x00C, value); }
        }
        public Int16 ItemMaxPP
        {
            get { return GetInt16(DataA, 0x00E); }
            set { SetInt16(DataA, 0x00E, value); }
        }
        public Int16 ItemAttModifier
        {
            get { return GetInt16(DataA, 0x010); }
            set { SetInt16(DataA, 0x010, value); }
        }
        //public byte  ItemRankIcon
        //{
        //    get { return GetByte(DataA, 0x012); }
        //    set { SetByte(DataA, 0x012, value); SetUInt(DataB, 0x043, value); }
        //}
        public byte  ItemStars
        {
            get { return GetByte(DataA, 0x013); }
            set
            {   // Optomize this mess later...
                SetByte(DataA, 0x013, value);
                byte weaponIcon = 0;
                switch (value)
                {
                    case 1:
                    case 2:
                    case 3:
                        weaponIcon = 1;
                        break;
                    case 4:
                    case 5:
                    case 6:
                        weaponIcon = 2;
                        break;
                    case 7:
                    case 8:
                    case 9:
                        weaponIcon = 3;
                        break;
                    default:
                        weaponIcon = 4;
                        break;
                }
                SetByte(DataA, 0x012, weaponIcon);
                SetUInt(DataB, 0x043, weaponIcon);
            }
        }
        public byte ItemElement
        {
            get { return GetByte(DataA, 0x014); }
            set { SetByte(DataA, 0x014, value); }
        }
        public byte ItemPALevel
        {
            get { return GetByte(DataA, 0x014); }
            set { SetByte(DataA, 0x014, value); }
        }
        public byte  ItemElementLevel
        {
            get { return GetByte(DataA, 0x015); }
            set { SetByte(DataA, 0x015, value); }
        }



        //0x000			46 Bytes Max				23 Character Unicode String: Item Name
        //0x02E			2 Bytes						Unknown
        //0x030			4 Bytes						Unknown
        //0x034			2 Bytes						Weapon Base Max PP (No grinds?)
        //0x036			2 Bytes						Weapon Base Att (No grinds?)
        //0x038			2 Bytes						Weapon Base Acc (signed 16bit)
        //0x03A			2 Bytes						Weapon ATA/ATP Required
        //0x03C			2 Bytes						Unknown
        //0x040			2 Bytes						Unknown
        //0x042			1 Byte						Unknown
        //0x043			1 Byte						Weapon rank icon (0=None, 1=C, 2=B, 3=A, 4=S) - Shows in info box only!
        //0x044			1 Byte						Unknown
        //0x045			1 Byte						Max Grind Level
        //0x046			1 Byte						Item Verb
        //0x047			1 Byte						Unknown

        public string ItemName
        {
            get
            {
                
                string temp = Encoding.Unicode.GetString(DataB, 0, 23);
                return temp.Substring(0, temp.IndexOf((char)0x00));
            }
            set
            {
                byte[] temp = Encoding.Unicode.GetBytes(value);
                byte[] buffer = new byte[46];
                if (temp.Length > 46)
                    Array.Copy(temp, buffer, 46);
                else
                    Array.Copy(temp, buffer, temp.Length);

                Array.Copy(buffer, 0, DataB, 0, 46);
            }
        }
        public Int16 ItemBaseMaxPP
        {
            get { return GetInt16(DataB, 0x034); }
            set { SetInt16(DataB, 0x034, value); }
        }
        public Int16 ItemBaseAtt
        {
            get { return GetInt16(DataB, 0x036); }
            set { SetInt16(DataB, 0x036, value); }
        }
        public Int16 ItemBaseAta
        {
            get { return GetInt16(DataB, 0x038); }
            set { SetInt16(DataB, 0x038, value); }
        }
        public Int16 ItemBaseTech                   
        {
            get { return GetInt16(DataB, 0x036); }
            set { SetInt16(DataB, 0x036, value); }
        }
        public Int16 ItemRequiredStat
        {
            get { return GetInt16(DataB, 0x03A); }
            set { SetInt16(DataB, 0x03A, value); }
        }
        public Int16 ItemAtpVariance                  // TODO; Figure out where this is stored, if anywhere?
        {
            get
            {
                return atpvariance;
                //    return GetInt16(DataB, 0x038);
            }
            set
            {
                //    SetInt16(DataB, 0x038, value);
                atpvariance = value;
            }
        }
        public byte ItemMaxGrind
        {
            get { return GetByte(DataB, 0x045); }
            set { SetByte(DataB, 0x045, value); }
        }
        public byte ItemVerb
        {
            get { return GetByte(DataA, 0x046); }
            set { SetByte(DataA, 0x046, value); }
        }

        public bool Equipped
        {
            get { return isequipped; }
            set { isequipped = value; }
        }

    }
    // Item Class




    // Centralized Item Database
    class ItemDB
    {
        public static List<Item> MainItemList = new List<Item>();

        // Main function to load item data from the *.xml storage files into Item classes
        public static void LoadItemDB()
        {
            XDocument data = XDocument.Load("data\\GameData\\WeaponData.xml");
            byte weaponicon;
            Item tempItem;

            foreach (var weapontype in data.Descendants("WeaponType"))
            {
                weaponicon = System.Convert.ToByte(weapontype.Attribute("type").Value);

                foreach (var weapon in weapontype.Descendants("Weapon"))
                {
                    // TODO: Add remaining data to XML file
                    // TODO: Streamline this with a constructor like I did with the Enemy class
                    tempItem = new Item();

                    tempItem.ItemName =  weapon.Attribute("name").Value;
                    tempItem.ItemMaker = System.Convert.ToByte(weapon.Attribute("mfr").Value);
                    tempItem.ItemMaxPP = System.Convert.ToInt16(weapon.Attribute("pp").Value);
                    tempItem.ItemBaseAtt = System.Convert.ToInt16(weapon.Attribute("att").Value);
                    tempItem.ItemBaseAta = System.Convert.ToInt16(weapon.Attribute("ata").Value);
                    tempItem.ItemBaseTech = System.Convert.ToInt16(weapon.Attribute("tech").Value);
                    tempItem.ItemRequiredStat = System.Convert.ToInt16(weapon.Attribute("req").Value);
                    tempItem.ItemAtpVariance = System.Convert.ToInt16(weapon.Attribute("var").Value);
                    tempItem.ItemStars = System.Convert.ToByte(weapon.Attribute("stars").Value);

                    MainItemList.Add(tempItem);
                }
            }


        }



    }







    // Item related enumerations
    public enum ItemRank : byte
    {
        C_Rank = 0x01,
        B_Rank = 0x02,
        A_Rank = 0x03,
        S_Rank = 0x04
    }
    public enum Manufacturer : byte
    {
        GRM = 0x00,
        Yohmei = 0x01,
        Tenora = 0x02,
        Kubara = 0x03
    }
    public enum ClothingLine : byte
    {

    }   // Empty: Needs data!
    public enum ItemType : byte
    {
        Weapon = 0x01,
        Useable = 0x02,
        Shield = 0x03,
        Unit = 0x04,
        Clothing = 0x05,
        CASTPart = 0x06,
        Material = 0x07
    }
    public enum IconType : byte
    {
        // For Weapons
        Sword = 0x01,
        Knuckles = 0x02,
        Spear = 0x03,
        DoubleSaber = 0x04,
        Axe = 0x05,
        TwinSaber = 0x06,
        TwinDagger = 0x07,
        TwinClaws = 0x08,
        Saber = 0x09,
        Dagger = 0x0A,
        Claw = 0x0B,
        Whip = 0x0C,
        Slicer = 0x0D,
        Rifle = 0x0E,
        Shotgun = 0x0F,
        Bow = 0x10,
        Grenade = 0x11,
        Laser = 0x12,
        TwinHandgun = 0x13,
        Handgun = 0x14,
        Crossbow = 0x15,
        Card = 0x16,
        Machinegun = 0x17,
        Rod = 0x18,
        Wand = 0x19,
        TCSM = 0x1A,
        RCSM = 0x1B,

        // For Usable Items

        // For Line Shields / Units
        
        // For Clothing & Parts

        // For Materials
        Photon = 0x01,
        Metal = 0x02,
        Biomaterial = 0x07,
        Ore = 0x08,
        HardMetal = 0x09,
        Food = 0x0A,
        Chemical = 0x0B,
        GrinderBase = 0x0C,
        RoomDeco = 0x0D,
        Gemstone = 0x0E,
        Unknown = 0x0F,
        Wood = 0x10

    }

}
