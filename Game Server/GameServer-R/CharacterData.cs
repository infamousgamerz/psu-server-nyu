﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace GameServer_R
{
    public enum Race : byte
    {
        Human = 0x00,
        Newman = 0x01,
        Cast = 0x02,
        Beast = 0x03
    }
    public enum Gender : byte
    {
        Male = 0x00,
        Female = 0x01
    }




    public class CharacterData
    {
        // Inventory list 
        public Inventory PlayerInventory = new Inventory();

        // data
        private byte[] container; // 344 byte data

        // initialize
        public CharacterData(byte feedMe)
        {
            // create new byte array
            container = new byte[0x158];

            // write some values wich never change
            // THIS IS WRONG!! >:(  -Smidge
            //WriteByte(0x00, 0x4B);
            //WriteByte(0x01, 0x4F);
            //WriteByte(0x02, 0x53);
            //WriteByte(0x03, 0x57);
            //WriteByte(0x04, 0x5B);
            //WriteByte(0x05, 0x5F);
        }

        // for creating new character
        public void WriteNewCharacterData(byte[] input)
        {
            container = new byte[0x158];

            Array.Copy(input, 0, container, 0, input.Count());
        }

        // methods
        public void DeserializeXML(XElement data)
        {
            #region Query
            // run a query to select the data easily
            var query = from character in data.Ancestors()
                        select new
                        {
                            // basics
                            characterName = (string)data.Attribute("name"),
                            characterRace = (string)data.Attribute("race"),
                            characterGender = (string)data.Attribute("gender"),

                            // appearance
                            voiceType = (uint)data.Element("looks").Element("voice").Attribute("type"),
                            voicePitch = (uint)data.Element("looks").Element("voice").Attribute("pitch"),

                            upperBodyClothing = (uint)data.Element("looks").Element("upperbody").Attribute("clothing"),
                            upperBodyType = (uint)data.Element("looks").Element("upperbody").Attribute("type"),
                            upperBodyVariation = (uint)data.Element("looks").Element("upperbody").Attribute("variation"),
                            upperBodyColor = (uint)data.Element("looks").Element("upperbody").Attribute("color"),

                            lowerBodyClothing = (uint)data.Element("looks").Element("lowerbody").Attribute("clothing"),
                            lowerBodyType = (uint)data.Element("looks").Element("lowerbody").Attribute("type"),
                            lowerBodyVariation = (uint)data.Element("looks").Element("lowerbody").Attribute("variation"),
                            lowerBodyColor = (uint)data.Element("looks").Element("lowerbody").Attribute("color"),

                            shoesClothing = (uint)data.Element("looks").Element("shoes").Attribute("clothing"),
                            shoesType = (uint)data.Element("looks").Element("shoes").Attribute("type"),
                            shoesVariation = (uint)data.Element("looks").Element("shoes").Attribute("variation"),
                            shoesColor = (uint)data.Element("looks").Element("shoes").Attribute("color"),


                            hairStyle = (uint)data.Element("looks").Element("hair").Attribute("style"),
                            hairType = (uint)data.Element("looks").Element("hair").Attribute("type"),
                            hairVariation = (uint)data.Element("looks").Element("hair").Attribute("variation"),
                            hairColor = (uint)data.Element("looks").Element("hair").Attribute("color"),
                            hairBrightness = (uint)data.Element("looks").Element("hair").Attribute("brightness"),

                            ears = (uint)data.Element("looks").Element("head").Attribute("ears"),
                            earsVariation = (uint)data.Element("looks").Element("head").Attribute("earsvariation"),
                            earsType = (uint)data.Element("looks").Element("head").Attribute("earstype"),
                            eyes = (uint)data.Element("looks").Element("head").Attribute("eyes"),
                            eyebrows = (uint)data.Element("looks").Element("head").Attribute("eyebrows"),
                            eyelashes = (uint)data.Element("looks").Element("head").Attribute("eyelashes"),
                            eyetype = (uint)data.Element("looks").Element("head").Attribute("eyetype"),
                            eyeColor = (uint)data.Element("looks").Element("head").Attribute("eyecolor"),
                            eyeBrightness = (uint)data.Element("looks").Element("head").Attribute("eyebrightness"),
                            lipColorIntensity = (uint)data.Element("looks").Element("head").Attribute("lipcolorintensity"),
                            lipColorValue = (uint)data.Element("looks").Element("head").Attribute("lipcolor"),
                            lipColorBrightness = (uint)data.Element("looks").Element("head").Attribute("lipbrightness"),
                            face = (uint)data.Element("looks").Element("head").Attribute("face"),
                            faceVariation = (uint)data.Element("looks").Element("head").Attribute("facevariation"),
                            faceType = (uint)data.Element("looks").Element("head").Attribute("facetype"),
                            headProportionsWE = (uint)data.Element("looks").Element("head").Attribute("proportionswe"),
                            headProportionsNS = (uint)data.Element("looks").Element("head").Attribute("proportionsns"),

                            bodyColor = (uint)data.Element("looks").Element("body").Attribute("color"),
                            bodyColorCast = (uint)data.Element("looks").Element("body").Attribute("castcolor"),
                            bodyUnderwear = (uint)data.Element("looks").Element("body").Attribute("underwear"),
                            bodyHeight = (uint)data.Element("looks").Element("body").Attribute("height"),
                            bodyProportionsWE = (uint)data.Element("looks").Element("body").Attribute("proportionswe"),
                            bodyProportionsNS = (uint)data.Element("looks").Element("body").Attribute("proportionsns"),

                            // stats
                            playerLevel = (uint)data.Element("stats").Attribute("level"),
                            playerEXP = (uint)data.Element("stats").Attribute("exp"),
                            playerMeseta = (uint)data.Element("stats").Attribute("meseta"),
                            playerPlaytime = (uint)data.Element("stats").Attribute("playtime"),

                            // current level range (base)
                            baseNextLevelEXP = (uint)data.Element("stats").Element("base").Attribute("nextlevelexp"),
                            baseCurrentLevelEXP = (uint)data.Element("stats").Element("base").Attribute("currentlevelexp"),
                            baseHP = (uint)data.Element("stats").Element("base").Attribute("hp"),
                            baseATP = (uint)data.Element("stats").Element("base").Attribute("atp"),
                            baseDFP = (uint)data.Element("stats").Element("base").Attribute("dfp"),
                            baseATA = (uint)data.Element("stats").Element("base").Attribute("ata"),
                            baseEVP = (uint)data.Element("stats").Element("base").Attribute("evp"),
                            baseSTA = (uint)data.Element("stats").Element("base").Attribute("sta"),
                            baseSPD = (uint)data.Element("stats").Element("base").Attribute("spd"),
                            baseTP = (uint)data.Element("stats").Element("base").Attribute("tp"),
                            baseMST = (uint)data.Element("stats").Element("base").Attribute("mst"),

                            // current stats(runtime)
                            runtimeCurrentLevel = (uint)data.Element("stats").Element("runtime").Attribute("currentlevel"),
                            runtimeATP = (uint)data.Element("stats").Element("runtime").Attribute("atp"),
                            runtimeDFP = (uint)data.Element("stats").Element("runtime").Attribute("dfp"),
                            runtimeATA = (uint)data.Element("stats").Element("runtime").Attribute("ata"),
                            runtimeEVP = (uint)data.Element("stats").Element("runtime").Attribute("evp"),
                            runtimeSTA = (uint)data.Element("stats").Element("runtime").Attribute("sta"),
                            runtimeSPD = (uint)data.Element("stats").Element("runtime").Attribute("spd"),
                            runtimeTP = (uint)data.Element("stats").Element("runtime").Attribute("tp"),
                            runtimeMST = (uint)data.Element("stats").Element("runtime").Attribute("mst"),
                            runtimeHPCurrent = (uint)data.Element("stats").Element("runtime").Attribute("hpcurrent"),
                            runtimeHPMax = (uint)data.Element("stats").Element("runtime").Attribute("hpmax")

                        };

            #endregion

            // select the first element
            var charData = query.First();

            CharacterName = charData.characterName;



            #region Basics
            if (charData.characterRace == "human")
            {
                CharacterRace = Race.Human;
            }
            else if (charData.characterRace == "newman")
            {
                CharacterRace = Race.Newman;
            }
            else if (charData.characterRace == "cast")
            {
                CharacterRace = Race.Cast;
            }
            else if (charData.characterRace == "beast")
            {
                CharacterRace = Race.Beast;
            }

            if (charData.characterGender == "male")
            {
                CharacterGender = Gender.Male;
            }
            else if (charData.characterGender == "female")
            {
                CharacterGender = Gender.Female;
            }
            #endregion

            #region Looks
            VoiceType = (byte)charData.voiceType;
            VoicePitch = (byte)charData.voicePitch;

            UpperBodyClothing = (byte)charData.upperBodyClothing;
            UpperBodyType = (byte)charData.upperBodyType;
            UpperBodyVariation = (byte)charData.upperBodyVariation;
            UpperBodyColor = (byte)charData.upperBodyColor;

            LowerBodyClothing = (byte)charData.lowerBodyClothing;
            LowerBodyType = (byte)charData.lowerBodyType;
            LowerBodyVariation = (byte)charData.lowerBodyVariation;
            LowerBodyColor = (byte)charData.lowerBodyColor;

            ShoesClothing = (byte)charData.shoesClothing;
            ShoesType = (byte)charData.shoesType;
            ShoesVariation = (byte)charData.shoesVariation;
            ShoesColor = (byte)charData.shoesColor;

            HairStyle = (byte)charData.hairStyle;
            HairType = (byte)charData.hairType;
            HairVariation = (byte)charData.hairVariation;
            HairColorBrightness = charData.hairBrightness;
            HairColorValue = charData.hairColor;

            Ears = (byte)charData.ears;
            EarsType = (byte)charData.earsType;
            EarsVariation = (byte)charData.earsVariation;
            Eyes = (byte)charData.eyes;
            Eyebrows = (byte)charData.eyebrows;
            Eyelashes = (byte)charData.eyelashes;
            EyeType = (byte)charData.eyetype;
            EyeColorValue = charData.eyeColor;
            EyeColorBrightness = charData.eyeBrightness;

            LipIntensity = charData.lipColorIntensity;
            LipColorValue = charData.lipColorValue;
            LipColorBrightness = charData.lipColorBrightness;

            Face = (byte)charData.face;
            FaceType = (byte)charData.faceType;
            FaceVariation = (byte)charData.faceVariation;
            FaceProportionsNS = charData.headProportionsNS;
            FaceProportionsWE = charData.headProportionsWE;

            SkinColor = charData.bodyColor;
            SkinColorCast = charData.bodyColorCast;
            Underwear = (byte)charData.bodyUnderwear;
            Height = charData.bodyHeight;
            ProportionsWE = charData.bodyProportionsWE;
            ProportionsNS = charData.bodyProportionsNS;

            #endregion

            // stats
            Level = charData.playerLevel;
            EXP = charData.playerEXP;
            Meseta = charData.playerMeseta;
            Playtime = charData.playerPlaytime;

            //base
            BaseNextLevelEXP = charData.baseNextLevelEXP;
            BaseCurrentLevelEXP = charData.baseCurrentLevelEXP;
            BaseATP = (UInt16)charData.baseATP;
            BaseDFP = (UInt16)charData.baseDFP;
            BaseATA = (UInt16)charData.baseATA;
            BaseEVP = (UInt16)charData.baseEVP;
            BaseSTA = (UInt16)charData.baseSTA;
            BaseSPD = (UInt16)charData.baseSPD;
            BaseTP = (UInt16)charData.baseTP;
            BaseMST = (UInt16)charData.baseMST;

            //runtime
            RuntimeLevel = charData.runtimeCurrentLevel;
            RuntimeATP = (UInt16)charData.runtimeATP;
            RuntimeDFP = (UInt16)charData.runtimeDFP;
            RuntimeATA = (UInt16)charData.runtimeATA;
            RuntimeEVP = (UInt16)charData.runtimeEVP;
            RuntimeSTA = (UInt16)charData.runtimeSTA;
            RuntimeSPD = (UInt16)charData.runtimeSPD;
            RuntimeTP = (UInt16)charData.runtimeTP;
            RuntimeMST = (UInt16)charData.runtimeMST;
            RuntimeCurrentHP = charData.runtimeHPCurrent;
            RuntimeMaxHP = charData.runtimeHPMax;

        }

        public XElement SerializeXML(GameClient e)
        {
            XElement root = new XElement("character",
                new XAttribute("id", e.SelectedCharacterID),
                new XAttribute("name", CharacterName),
                new XAttribute("gender", CharacterGender.ToString().ToLower()),
                new XAttribute("race", CharacterRace.ToString().ToLower()),

                new XElement("looks",
                    new XElement("voice",
                        new XAttribute("type", VoiceType), new XAttribute("pitch", VoicePitch)
                        ),
                    new XElement("upperbody",
                        new XAttribute("clothing", UpperBodyClothing), new XAttribute("variation", UpperBodyVariation),
                        new XAttribute("type", UpperBodyType), new XAttribute("color", UpperBodyColor)
                        ),
                    new XElement("lowerbody",
                        new XAttribute("clothing", LowerBodyClothing), new XAttribute("variation", LowerBodyVariation),
                        new XAttribute("type", LowerBodyType), new XAttribute("color", LowerBodyColor)
                        ),
                    new XElement("shoes",
                        new XAttribute("clothing", ShoesClothing), new XAttribute("variation", ShoesVariation),
                        new XAttribute("type", ShoesType), new XAttribute("color", ShoesColor)
                        ),
                    new XElement("hair",
                        new XAttribute("style", HairStyle), new XAttribute("variation", HairVariation),
                        new XAttribute("type", HairType), new XAttribute("color", HairColorValue), new XAttribute("brightness", HairColorBrightness)
                        ),
                    new XElement("head",
                        new XAttribute("ears", Ears), new XAttribute("earsvariation", EarsVariation), new XAttribute("earstype", EarsType),
                        new XAttribute("face", Face), new XAttribute("facevariation", FaceVariation), new XAttribute("facetype", FaceType),
                        new XAttribute("eyebrows", Eyebrows), new XAttribute("eyelashes", Eyelashes), new XAttribute("eyetype", EyeType),
                        new XAttribute("eyes", Eyes), new XAttribute("eyecolor", EyeColorValue), new XAttribute("eyebrightness", EyeColorBrightness),
                        new XAttribute("lipcolorintensity", LipIntensity), new XAttribute("lipcolor", LipColorValue),
                        new XAttribute("lipbrightness", LipColorBrightness),
                        new XAttribute("proportionswe", FaceProportionsWE), new XAttribute("proportionsns", FaceProportionsNS)
                        ),
                    new XElement("body",
                        new XAttribute("color", SkinColor), new XAttribute("castcolor", SkinColorCast), new XAttribute("underwear", Underwear), new XAttribute("height", Height),
                        new XAttribute("proportionswe", ProportionsWE), new XAttribute("proportionsns", ProportionsNS)
                        )
                    ),

                new XElement("stats",
                    new XAttribute("level", Level), new XAttribute("exp", EXP), new XAttribute("meseta", Meseta), new XAttribute("playtime", Playtime),

                    new XElement("base",
                        new XAttribute("nextlevelexp", BaseNextLevelEXP), new XAttribute("currentlevelexp", BaseCurrentLevelEXP),
                        new XAttribute("hp", BaseHP), new XAttribute("atp", BaseATP),
                        new XAttribute("dfp", BaseDFP), new XAttribute("ata", BaseATA),
                        new XAttribute("evp", BaseEVP), new XAttribute("sta", BaseSTA),
                        new XAttribute("spd", BaseSPD), new XAttribute("tp", BaseTP),
                        new XAttribute("mst", BaseMST)
                        ),

                    new XElement("runtime",
                        new XAttribute("currentlevel", RuntimeLevel), new XAttribute("hpmax", RuntimeCurrentHP),
                        new XAttribute("hpcurrent", RuntimeCurrentHP), new XAttribute("atp", RuntimeATP),
                        new XAttribute("dfp", RuntimeDFP), new XAttribute("ata", RuntimeATA),
                        new XAttribute("evp", RuntimeEVP), new XAttribute("sta", RuntimeSTA),
                        new XAttribute("spd", RuntimeSPD), new XAttribute("tp", RuntimeTP),
                        new XAttribute("mst", RuntimeMST)
                        )
                    )
                );

            return root;
        }

        // special properties
        public byte[] DataCharSelectionScreen
        {
            get
            {
                byte[] temp = new byte[0x114];
                Array.Copy(container, temp, 0x114);
                return temp;
            }
        }
        public byte[] DataCharLobby
        {
            get
            {
                byte[] temp = new byte[0x158];
                Array.Copy(container, temp, 0x158);
                return temp;
            }
        }

        #region Properties

        // Properties
        public string CharacterName
        {
            get { return ReadUnicodeString(0x00); }
            set { WriteUnicodeString(value, 0x00); }
        }
        public Race CharacterRace
        {
            get { return (Race)ReadByte(0x40); }
            set { WriteByte((byte)value, 0x40); }
        }
        public Gender CharacterGender
        {
            get { return (Gender)ReadByte(0x41); }
            set { WriteByte((byte)value, 0x41); }
        }
        public byte Type
        {
            get { return ReadByte(0x42); }
            set { WriteByte(value, 0x42); }
        }
        public byte VoiceType
        {
            get { return ReadByte(0x43); }
            set { WriteByte(value, 0x43); }
        }
        public byte VoicePitch
        {
            get { return ReadByte(0x44); }
            set { WriteByte(value, 0x44); }
        }

        // clothing

        // upper body
        public byte UpperBodyVariation
        {
            get { return ReadByte(0x48); }
            set { WriteByte(value, 0x48); }
        }
        public byte UpperBodyClothing
        {
            get { return ReadByte(0x49); }
            set { WriteByte(value, 0x49); }
        }
        public byte UpperBodyType
        {
            get { return ReadByte(0x4A); }
            set { WriteByte(value, 0x4A); }
        }
        public byte UpperBodyColor
        {
            get { return ReadByte(0x60); }
            set { WriteByte(value, 0x60); }
        }

        // lower body
        public byte LowerBodyVariation
        {
            get { return ReadByte(0x4C); }
            set { WriteByte(value, 0x4C); }
        }
        public byte LowerBodyClothing
        {
            get { return ReadByte(0x4D); }
            set { WriteByte(value, 0x4D); }
        }
        public byte LowerBodyType
        {
            get { return ReadByte(0x4E); }
            set { WriteByte(value, 0x4E); }
        }
        public byte LowerBodyColor
        {
            get { return ReadByte(0x61); }
            set { WriteByte(value, 0x61); }
        }

        // Shoes
        public byte ShoesVariation
        {
            get { return ReadByte(0x50); }
            set { WriteByte(value, 0x50); }
        }
        public byte ShoesClothing
        {
            get { return ReadByte(0x51); }
            set { WriteByte(value, 0x51); }
        }
        public byte ShoesType
        {
            get { return ReadByte(0x52); }
            set { WriteByte(value, 0x52); }
        }
        public byte ShoesColor
        {
            get { return ReadByte(0x62); }
            set { WriteByte(value, 0x62); }
        }

        // Ears
        public byte EarsVariation
        {
            get { return ReadByte(0x54); }
            set { WriteByte(value, 0x54); }
        }
        public byte Ears
        {
            get { return ReadByte(0x55); }
            set { WriteByte(value, 0x55); }
        }
        public byte EarsType
        {
            get { return ReadByte(0x56); }
            set { WriteByte(value, 0x56); }
        }

        // Face
        public byte FaceVariation
        {
            get { return ReadByte(0x58); }
            set { WriteByte(value, 0x58); }
        }
        public byte Face
        {
            get { return ReadByte(0x59); }
            set { WriteByte(value, 0x59); }
        }
        public byte FaceType
        {
            get { return ReadByte(0x5A); }
            set { WriteByte(value, 0x5A); }
        }
        public uint FaceProportionsWE
        {
            get { return ReadUInt(0x9C); }
            set { WriteUInt(value, 0x9C); }
        }
        public uint FaceProportionsNS
        {
            get { return ReadUInt(0xA0); }
            set { WriteUInt(value, 0xA0); }
        }

        // Hair
        public byte HairVariation
        {
            get { return ReadByte(0x5C); }
            set { WriteByte(value, 0x5C); }
        }
        public byte HairStyle
        {
            get { return ReadByte(0x5D); }
            set { WriteByte(value, 0x5D); }
        }
        public byte HairType
        {
            get { return ReadByte(0x5E); }
            set { WriteByte(value, 0x5E); }
        }
        public uint HairColorBrightness
        {
            get { return ReadUInt(0x88); }
            set { WriteUInt(value, 0x88); }
        }
        public uint HairColorValue
        {
            get { return ReadUInt(0x8C); }
            set { WriteUInt(value, 0x8C); }
        }


        // Eyes
        public byte Eyebrows
        {
            get { return ReadByte(0x65); }
            set { WriteByte(value, 0x65); }
        }
        public byte Eyelashes
        {
            get { return ReadByte(0x66); }
            set { WriteByte(value, 0x66); }
        }
        public byte EyeType
        {
            get { return ReadByte(0x67); }
            set { WriteByte(value, 0x67); }
        }
        public byte Eyes
        {
            get { return ReadByte(0x68); }
            set { WriteByte(value, 0x68); }
        }
        public uint EyeColorBrightness
        {
            get { return ReadUInt(0x6C); }
            set { WriteUInt(value, 0x6C); }
        }
        public uint EyeColorValue
        {
            get { return ReadUInt(0x70); }
            set { WriteUInt(value, 0x70); }
        }

        // Lips
        public uint LipIntensity
        {
            get { return ReadUInt(0x74); }
            set { WriteUInt(value, 0x74); }
        }
        public uint LipColorValue
        {
            get { return ReadUInt(0x78); }
            set { WriteUInt(value, 0x78); }
        }
        public uint LipColorBrightness
        {
            get { return ReadUInt(0x7C); }
            set { WriteUInt(value, 0x7C); }
        }
    

        // Body
        public byte Underwear
        {
            get { return ReadByte(0x68); }
            set { WriteByte(value, 0x68); }
        }
        public uint SkinColor
        {
            get { return ReadUInt(0x80); }
            set { WriteUInt(value, 0x80); }
        }
        public uint SkinColorCast
        {
            get { return ReadUInt(0x84); }
            set { WriteUInt(value, 0x84); }
        }
        public uint Height
        {
            get { return ReadUInt(0x90); }
            set { WriteUInt(value, 0x90); }
        }
        public uint ProportionsWE
        {
            get { return ReadUInt(0x94); }
            set { WriteUInt(value, 0x94); }
        }
        public uint ProportionsNS
        {
            get { return ReadUInt(0x98); }
            set { WriteUInt(value, 0x98); }
        }


        // Stats
        public uint Level
        {
            get { return ReadUInt(0xA4); }
            set { WriteUInt(value, 0xA4); }
        }

        public uint EXP
        {
            get { return ReadUInt(0xB0); }
            set { WriteUInt(value, 0xB0); }
        }

        public uint Meseta
        {
            get { return ReadUInt(0xB8); }
            set { WriteUInt(value, 0xB8); }
        }
        public uint Playtime // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt(0xBC); }
            set { WriteUInt(value, 0xBC); }
        }






        // Base Stats
        public uint BaseNextLevelEXP // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt(0x114); }
            set { WriteUInt(value, 0x114); }
        }
        public uint BaseCurrentLevelEXP // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt(0x118); }
            set { WriteUInt(value, 0x118); }
        }
        public uint BaseHP // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt(0x11C); }
            set { WriteUInt(value, 0x11C); }
        }
        public UInt16 BaseATP // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt16(0x120); }
            set { WriteUInt16(value, 0x120); }
        }
        public UInt16 BaseDFP // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt16(0x122); }
            set { WriteUInt16(value, 0x122); }
        }
        public UInt16 BaseATA // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt16(0x124); }
            set { WriteUInt16(value, 0x124); }
        }
        public UInt16 BaseEVP // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt16(0x126); }
            set { WriteUInt16(value, 0x126); }
        }
        public UInt16 BaseSTA // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt16(0x128); }
            set { WriteUInt16(value, 0x128); }
        }
        public UInt16 BaseSPD // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt16(0x12A); }
            set { WriteUInt16(value, 0x12A); }
        }
        public UInt16 BaseTP // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt16(0x12C); }
            set { WriteUInt16(value, 0x12C); }
        }
        public UInt16 BaseMST // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt16(0x12E); }
            set { WriteUInt16(value, 0x12E); }
        }

        // Runtime Stats
        public uint RuntimeLevel // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt(0x13C); }
            set { WriteUInt(value, 0x13C); }
        }
        public UInt16 RuntimeATP // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt16(0x140); }
            set { WriteUInt16(value, 0x140); }
        }
        public UInt16 RuntimeDFP // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt16(0x142); }
            set { WriteUInt16(value, 0x142); }
        }
        public UInt16 RuntimeATA // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt16(0x144); }
            set { WriteUInt16(value, 0x144); }
        }
        public UInt16 RuntimeEVP // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt16(0x146); }
            set { WriteUInt16(value, 0x146); }
        }
        public UInt16 RuntimeSTA // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt16(0x148); }
            set { WriteUInt16(value, 0x148); }
        }
        public UInt16 RuntimeSPD // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt16(0x14A); }
            set { WriteUInt16(value, 0x14A); }
        }
        public UInt16 RuntimeTP // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt16(0x14C); }
            set { WriteUInt16(value, 0x14C); }
        }
        public UInt16 RuntimeMST // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt16(0x14E); }
            set { WriteUInt16(value, 0x14E); }
        }
        public uint RuntimeCurrentHP // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt(0x150); }
            set { WriteUInt(value, 0x150); }
        }
        public uint RuntimeMaxHP // Maybe change it into something more convinient :3 -> native type
        {
            get { return ReadUInt(0x154); }
            set { WriteUInt(value, 0x154); }
        }




        #endregion

        #region Value Reading and Writing

        // == Value reading and writing

        // Integer
        public int ReadInt(int offset)
        {
            return BitConverter.ToInt32(container, offset);
        }
        public void WriteInt(int value, int offset)
        {
            Array.Copy(BitConverter.GetBytes(value), 0, container, offset, 4);
        }

        // unsigned Integer
        public uint ReadUInt(int offset)
        {
            return BitConverter.ToUInt32(container, offset);
        }
        public void WriteUInt(uint value, int offset)
        {
            Array.Copy(BitConverter.GetBytes(value), 0, container, offset, 4);
        }

        // unsigned Integer 16bit
        public UInt16 ReadUInt16(int offset)
        {
            return BitConverter.ToUInt16(container, offset);
        }
        public void WriteUInt16(UInt16 value, int offset)
        {
            Array.Copy(BitConverter.GetBytes(value), 0, container, offset, 2);
        }

        // Bytes
        public byte ReadByte(int offset)
        {
            return container[offset];
        }
        public void WriteByte(byte value, int offset)
        {
            container[offset] = value;
        }

        // Quad Bytes
        public byte[] ReadQuadByte(int offset)
        {
            byte[] bytes = new byte[4];
            Array.Copy(container, offset, bytes, 0, 4);
            return bytes;
        }
        public void WriteQuadByte(byte[] bytes, int offset)
        {
            Array.Copy(bytes, 0, container, offset, 4);
        }

        // Byte Array
        public byte[] ReadByteArray(int offset, int length)
        {
            byte[] bytes = new byte[4];
            Array.Copy(container, offset, bytes, 0, length);
            return bytes;
        }

        public void WriteByteArray(byte[] bytes, int offset)
        {
            Array.Copy(bytes, 0, container, offset, bytes.Length);
        }

        public void WriteByteArray(byte[] bytes, int length, int offset)
        {
            Array.Copy(bytes, 0, container, offset, length);
        }

        // Strings
        public string ReadUnicodeString(int offset)
        {
            string tempString = Encoding.Unicode.GetString(container, offset, container.Length - offset);
            return tempString.Substring(0, tempString.IndexOf((char)0x00));
        }
        public void WriteUnicodeString(string message, int offset)
        {
            byte[] temp = Encoding.Unicode.GetBytes(message);
            Array.Copy(temp, 0, container, offset, temp.Length);
        }
        public string ReadASCIIIString(int offset)
        {
            string tempString = Encoding.ASCII.GetString(container, offset, container.Length - offset);
            return tempString.Substring(0, tempString.IndexOf((char)0x00));
        }
        public void WriteASCIIString(string message, int offset)
        {
            byte[] temp = Encoding.ASCII.GetBytes(message);
            Array.Copy(temp, 0, container, offset, temp.Length);
        }
        #endregion
    }

}
