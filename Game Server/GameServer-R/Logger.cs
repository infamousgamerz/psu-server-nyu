﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;

namespace GameServer_R
{
    class Logger
    {
        // Logging is done through the the Trace and Debug classes

        // Add some objects global to the logger class
        //private ConsoleTraceListener consoleOutput;
        private TextWriterTraceListener textOutput;

        public Logger()
        {
            // setup autoflus
            Trace.AutoFlush = true;

            // setup console output
            //TODO: decide if we actually want to use the trace class for this instead of the usual Console.WriteLine()

            // setup text output
            if (!Directory.Exists("logs"))
                Directory.CreateDirectory("logs");
            textOutput = new TextWriterTraceListener("logs\\log--" + DateTime.Now.ToString("dd-mm-yyyy--HH-mm-ss-UTCzzz").Replace(":","") + ".txt");

            // add listeners to the tracer
            Trace.Listeners.Add(textOutput); // logger file

            // done :D
        }

        // public methods for logging
        public void Add(string input)
        {
            Add(input, LogLevel.Message);
        }

        public void Add(string input, LogLevel level)
        {
            string message = level.ToString() + " - " + input;
            Trace.WriteLine(message);
            Console.WriteLine(message);
        }

        // log levels
        public enum LogLevel : byte
        {
            Error=0x00,
            Warning = 0x01,
            Important = 0x02,
            Notice = 0x03,
            Message = 0x04,
            
            Debug = 0xFF
        }

    }
}
