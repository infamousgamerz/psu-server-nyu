﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameServer_R
{
    public class GameClientManager
    {
        // generic list containing every player connected to this server
        public List<GameClient> connectedClients = new List<GameClient>();
        public int ClientsConnected { get { return connectedClients.Count; } }

        // lock for blocking access
        private object AccessLock = new object();
        /* It is important to do this to avoid race conditions */

        // constructor
        public GameClientManager()
        {
            // nothing to do here yet...
        }

        // Methods
        public void AddGameClient(GameClient gameClient)
        {
            lock (AccessLock)
                connectedClients.Add(gameClient);
        }

        public void RemoveGameClient(GameClient gameClient)
        {
            lock (AccessLock)
                connectedClients.Remove(gameClient);
        }

        public void RemoveGameClient(uint userID)
        {
            RemoveGameClient(RetrieveGameClient(userID));
        }

        public GameClient RetrieveGameClient(uint userID)
        {
            if (userID == 0)
                return default(GameClient);
            if (connectedClients.Where(x => x.UserID == userID).Count() != 1)
                return default(GameClient);
            lock (AccessLock)
                return connectedClients.SingleOrDefault(x => x.UserID == userID);
        }
    }
}
