﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;
using System.Threading;

namespace GameServer_R
{
    public static class PartyManager
    {
        // list of all parties
        public static ConcurrentDictionary<uint, Party> Parties = new ConcurrentDictionary<uint, Party>();

        // index start
        private static uint partyIndexStart = 50000000;
        private static object partyIndexStartLock = new object();

        public static void CreateParty(uint newLeaderID, string PartyName)
        {
            // create party
            Party party = new Party()
            {
                Name = PartyName,
                Description = "test",
                MemberIDs = new ConcurrentDictionary<byte, uint>(),
                LeaderID = 0,
            };

            // add it to the list
            for (uint i = partyIndexStart; i < uint.MaxValue; i++)
            {
                if (!Parties.ContainsKey(i))
                {
                    Parties.TryAdd(i, party);
                    break;
                }
            }

            // reply
        }

        public static void DestroyParty(uint partyID)
        {
            Party temp;
            while (!Parties.TryRemove(partyID, out temp))
            {
                Thread.Sleep(100);
            }

            // reply, if needed at all
        }

        public static void InviteToParty(uint UserID, uint PartyID)
        {
            // check if party exists and is full
            if (!Parties.ContainsKey(PartyID))
                return;
            if (Parties[PartyID].MemberIDs.Count >= 6)
                return;
            // does this player already exist?
            if (Parties[PartyID].MemberIDs.Where(x => x.Value == UserID).Count() > 0)
                return;
            // everything is okay, send out the invite (and maybe later add it to a temporary list for sanity checks and hacking prevention)

            // build packet
        }

        public static void AddToParty(uint UserID, uint PartyID)
        {
            // check if party exists and is full
            if (!Parties.ContainsKey(PartyID))
                return;
            if (Parties[PartyID].MemberIDs.Count >= 6)
                return;
            // does this player already exist?
            if (Parties[PartyID].MemberIDs.Where(x => x.Value == UserID).Count() > 0)
                return;

            // everying okay, add to the list :)

            // send reply to the whole party :D
            // - should be done inside the Party struct
        }

    }

    public struct Party
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public ConcurrentDictionary<byte,uint> MemberIDs;
        public byte LeaderID;

        // settings follow soon
    }
}
