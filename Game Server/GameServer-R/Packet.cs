﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GameServer_R
{
    public struct Packet
    {
        

                // Raw Bytes
        private byte[] packet;
        public byte[] RawPacket { get { return packet; } }


        // Used for creating packets on the server side
        public Packet(uint PacketSize, byte[] Command)
        {
            // Set Size of new packet
            packet = new byte[(int)PacketSize + (PacketSize % 4)];

            // Write Size of new Packet and command inside the byte array
            Array.Copy(BitConverter.GetBytes(PacketSize), 0, packet, 0, 4);
            Array.Copy(Command, 0, packet, 4, 4);
        }

        // Used for reading packets recieved from the client
        public Packet(byte[] rawPacket)
        {
            // Copy recieved Packet into this object 
            uint rawPacketSize = BitConverter.ToUInt32(rawPacket, 0);
            packet = new byte[(int)rawPacketSize];
            Array.Copy(rawPacket, 0, packet, 0, rawPacketSize);
        }

        // Used for reading packets recieved from the client
        public Packet(byte[] rawPacket, bool force)
        {
            // Copy recieved Packet into this object 
            uint rawPacketSize = (uint)rawPacket.Count();
            packet = new byte[(int)rawPacketSize];
            Array.Copy(rawPacket, 0, packet, 0, rawPacketSize);
        }

        public Packet(byte[] rawPacket, int offset)
        {
            // Copy recieved Packet into this object 
            uint rawPacketSize = BitConverter.ToUInt32(rawPacket, offset);
            packet = new byte[(int)rawPacketSize];
            Array.Copy(rawPacket, offset, packet, 0, rawPacketSize);
        }

        // :V
        public uint PacketSize { get { return BitConverter.ToUInt32(packet, 0); } }
        public PacketChannel Channel { get { return (PacketChannel)this.ReadByte(0x06); } }


        // Integer
        public int ReadInt(int offset)
        {
            return BitConverter.ToInt32(packet, offset);
        }
        public void WriteInt(int value, int offset)
        {
            Array.Copy(BitConverter.GetBytes(value), 0, packet, offset, 4);
        }

        // unsigned Integer
        public uint ReadUInt(int offset)
        {
            return BitConverter.ToUInt32(packet, offset);
        }
        public void WriteUInt(uint value, int offset)
        {
            Array.Copy(BitConverter.GetBytes(value), 0, packet, offset, 4);
        }

        // short
        public short ReadInt16(int offset)
        {
            return BitConverter.ToInt16(packet, offset);
        }
        public void WriteInt16(short value, int offset)
        {
            Array.Copy(BitConverter.GetBytes(value), 0, packet, offset, 2);
        }

        // Bytes
        public byte ReadByte(int offset)
        {
            return packet[offset];
        }
        public void WriteByte(byte value, int offset)
        {
            packet[offset] = value;
        }

        // Quad Bytes
        public byte[] ReadQuadByte(int offset)
        {
            byte[] bytes = new byte[4];
            Array.Copy(packet, offset, bytes, 0, 4);
            return bytes;
        }
        public void WriteQuadByte(byte[] bytes, int offset)
        {
            Array.Copy(bytes, 0, packet, offset, 4);
        }

        // Byte Array
        public byte[] ReadByteArray(int offset, int length)
        {
            byte[] bytes = new byte[length];
            Array.Copy(packet, offset, bytes, 0, length);
            return bytes;
        }

        public void WriteByteArray(byte[] bytes, int offset)
        {
            Array.Copy(bytes, 0, packet, offset, bytes.Length);
        }

        public void WriteByteArray(byte[] bytes, int length, int offset)
        {
            Array.Copy(bytes, 0, packet, offset, length);
        }

        // Strings
        public string ReadUnicodeString(int offset)
        {
            string tempString = Encoding.Unicode.GetString(packet, offset, (int)this.PacketSize - offset);
            return tempString.Substring(0, tempString.IndexOf((char)0x00));
        }
        public void WriteUnicodeString(string message, int offset)
        {
            byte[] temp = Encoding.Unicode.GetBytes(message);
            Array.Copy(temp, 0, packet, offset, temp.Length);
        }
        public string ReadASCIIIString(int offset)
        {
            string tempString = Encoding.ASCII.GetString(packet, offset, (int)this.PacketSize - offset);
            return tempString.Substring(0, tempString.IndexOf((char)0x00));
        }
        public void WriteASCIIString(string message, int offset)
        {
            byte[] temp = Encoding.ASCII.GetBytes(message);
            Array.Copy(temp, 0, packet, offset, temp.Length);
        }



        // Append file to packet -- Experimental!
        public void AppendFile(string fileName)
        {
            // This function simply appends the contents of the given filename

            try
            {
                byte[] fileData = File.ReadAllBytes("filename");
                byte[] tempBuff = new byte[packet.Length + fileData.Length];

                Array.Copy(packet, 0, tempBuff, 0, packet.Length);
                Array.Copy(fileData, 0, tempBuff, packet.Length, fileData.Length);

                packet = tempBuff;

                // Update the "size" value of the packet
                WriteUInt((uint)packet.Length, 0);

            }
            catch (Exception e)
            {
                Program.log.Add("Failed to load: " + fileName + Environment.NewLine + e.ToString(), Logger.LogLevel.Error);
            }

        }



        // Enums
        public enum PacketChannel : byte
        {
            Unknown,
            Broadcast = 0x01,
            Requests = 0x02,
            Posts = 0x03
        }


    }
}
