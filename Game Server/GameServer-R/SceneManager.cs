﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace GameServer_R
{
    public class SceneManager
    {
        // list of scenes
        public List<Scene> scenes = new List<Scene>();
        private object ScenesLock = new object();

        // list of "areadefinitions" ordered by their ID
        public Dictionary<uint, byte[]> AreaDefinitions = new Dictionary<uint, byte[]>();
        private object AreaDefinitionsLock = new object();

        // list of cached maps
        public Dictionary<string, MapData> CachedMaps = new Dictionary<string, MapData>();
        private object CachedMapsLock = new object();

        // byte signature of spawning lobby
        public byte[] spawnLobby = new byte[8] { 0xe0, 0xc8, 0x10, 0x00, 0x01, 0x00, 0x00, 0x00 };

        // constructor
        public SceneManager()
        {
            // some code maybe?
            //ReloadSettings();
        }

        public void ReloadSettings()
        {
            LoadAreaDefinitions();
            LoadScenes();
        }

        public void LoadAreaDefinitions()
        {
            lock (AreaDefinitions)
            {
                foreach (var item in Directory.GetFiles("data\\areadefinitions\\", "*.nbl", SearchOption.AllDirectories).AsParallel().ToArray())
                //foreach (var item in Directory.GetFiles("data\\areadefinitions\\", "*.nbl", SearchOption.AllDirectories))
                {
                    uint id = uint.Parse(item.Split('\\').Last().Split('.').First());
                    if (AreaDefinitions.ContainsKey(id))
                        AreaDefinitions[id] = File.ReadAllBytes(item);
                    else
                        AreaDefinitions.Add(id, File.ReadAllBytes(item));
                }
            }
        }
        
        private void LoadScenes()
        {
            lock (ScenesLock)
            {
                foreach (var item in Directory.GetFiles("data\\scenes\\", "*.xml", SearchOption.AllDirectories))
                {
                    Scene temp = new Scene(item, SceneType.Lobby, SceneLocation.Colony);
                    Program.log.Add("Adding Scene " + temp.AreaName);

                    if (scenes.Where(x => x.Identification.SequenceEqual(temp.Identification)).Count() > 0)
                        Program.log.Add("Scene " + scenes.Single(x => x.Identification.SequenceEqual(temp.Identification)).AreaName + " was already found, skipping");
                    else
                        scenes.Add(new Scene(item, SceneType.Lobby, SceneLocation.Colony));
                }
            }
        }

        public Scene RetrieveScene(byte[] identification)
        {
            lock (ScenesLock)
                return scenes.Single(x => x.Identification.SequenceEqual(identification));
        }
    }
}
