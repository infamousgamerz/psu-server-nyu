﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameServer_R
{
    // A collection of functions that serve the core game mechanics.
    // Damage and healing, status effects, grinding and synthing, casino functions.
    class CoreMechanics
    {


        public enum ElementTypes : uint
        {
            // Placeholder until actual element values are determined
            Neutral     = 0x00000000,
            Fire        = 0x00000001,
            Ice         = 0x00000002,
            Lightning   = 0x00000003,
            Ground      = 0x00000004,
            Light       = 0x00000005,
            Dark        = 0x00000006
        }

        [Flags]
        public enum StatusEffects : uint
        {
            Burn         = 0x00000001,
            Poison       = 0x00000002,
            Infection    = 0x00000004,
            Shock        = 0x00000008,
            Silence      = 0x00000010,
            Freeze       = 0x00000020,
            Sleep        = 0x00000040,
            Stun         = 0x00000080,
            Confusion    = 0x00000100,
            Charm        = 0x00000200,
          //None/Unknown = 0x00000400,
          //None/Unknown = 0x00000800,
          //None/Unknown = 0x00001000,
          //None/Unknown = 0x00002000,
            Jellen       = 0x00004000, // ATP down 
            Zalure       = 0x00008000, // DFP down
          //Unknown      = 0x00010000, // Opposite of Retier?
            Zoldeel      = 0x00020000, // (ATA/EVP down)
          //None/Unknown = 0x00040000,
            Shifta       = 0x00080000, // (ATP up)
            Deband       = 0x00100000, // (DFP up)
            Retier       = 0x00200000, // (TP/MST up)
            Zodial       = 0x00400000 // (ATA/EVP up)
          //None/Unknown = 0x00800000
        }

        [Flags]
        public enum HitEffects : uint
        {
            HitAnimation1      = 0x00000001,
          //None/Unknown       = 0x00000002
          //None/Unknown       = 0x00000004
          //None/Unknown       = 0x00000008
            BlockAnimation1    = 0x00000010,
            BlockAnimation2    = 0x00000020, // without extra graphics
            BlockAnimation3    = 0x00000040, // without extra graphics
            HitAnimation2      = 0x00000080,
            KnockdownEffect    = 0x00000100, // Graphic/Effect only - Player is not actually knocked down
            KnockawayEffect    = 0x00000200, // Graphic/Effect only - Player is not actually knocked away
            LaunchGraphic      = 0x00000400, // Graphic/Effect only - Player is not actually launched
          //None/Unknown       = 0x00000800
          //None/Unknown       = 0x00001000
          //None/Unknown       = 0x00002000
          //None/Unknown       = 0x00004000 
          //None/Unknown       = 0x00008000
          //None/Unknown       = 0x00010000
            DyingAnimation     = 0x00020000, // disables menus as if dead, but no option to respawn
            UnknownEffect      = 0x00040000, // Unknown sparkyl green glow from ground. Some kind of buff?
            HealEffect         = 0x00080000, // Graphic/Effect only
          //None/Unknown       = 0x00100000
          //None/Unknown       = 0x00200000
            CriticalHitEffect  = 0x00400000, // Graphic/Effect only
          //None/Unknown       = 0x00800000
          //None/Unknown       = 0x01000000
          //HitAnimation1Again = 0x02000000  // Looks EXACTLY like HitAnimation1
          //None/Unknown       = 0x04000000
          //None/Unknown       = 0x08000000
          //None/Unknown       = 0x10000000
          //None/Unknown       = 0x20000000
          //None/Unknown       = 0x40000000
          //None/Unknown       = 0x80000000
        }


    }
}
