﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameServer_R
{
    public struct MapData
    {
        // contents
        private byte[] contents;
        public byte[] Contents { get { return contents; } }

        // Properties
        public int Size { get { return contents.Length; } }

        // Methods
        public MapData(byte[] data)
        {
            // initialize field
            contents = new byte[data.Length + 0x34];

            // write content into field
            Array.Copy(data, 0, contents, 0x34, data.Length);

            // write header
            Array.Copy(BitConverter.GetBytes(contents.Length), 0, contents, 0x00, 4); // size
            Array.Copy(new byte[8] { 0x02, 0x0F, 0x03, 0x00, 0xFF, 0xFF, 0x00, 0x00 }, 0, contents, 0x04, 8); // type
            //Array.Copy(new byte[8] { 0x00, 0xFF, 0x00, 0x00, 0x00, 0x0D, 0x00, 0x00 }, 0, contents, 0x3C, 8); // type
            Array.Copy(new byte[4] { 0x00, 0xFF, 0x00, 0x00 }, 0, contents, 0x2C, 0x04);
            Array.Copy(BitConverter.GetBytes(data.Length), 0, contents, 0x30, 4); ;
        }

        public byte[] GetBytes(int offset, int length)
        {
            int tempSize = 0;
            if ((offset + length) > this.Size)
                tempSize = this.Size - offset;
            else
                tempSize = length;
            byte[] temp = new byte[tempSize];


            Array.Copy(contents, offset, temp, 0, tempSize);
            return temp;
        }
    }
}
