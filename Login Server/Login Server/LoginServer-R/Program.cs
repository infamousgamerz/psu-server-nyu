﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;
using System.IO;

namespace LoginServer_R
{
    class Program
    {
        // global objects
        private static TcpListener tcpListener;
        public static List<Client> ConnectedClients = new List<Client>();
        public static X509Certificate serverCertificate;
        
        static void Main(string[] args)
        {
            // load log settings
            string logFileName = String.Format("login-{0:d4}-{1:d2}-{2:d2}--{3:d2}-{4:d2}-{5:d2}.log", 
                DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 
                DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            Trace.Listeners.Add(new TextWriterTraceListener(logFileName));
            Trace.Listeners.Add(new ConsoleTraceListener());
            Trace.AutoFlush = true;
            // load configuration settings
            //TODO: SQL Connection Settings

            // load MOTD from SQL Server or configuration file
            //TODO: Decide between SQL or Configuration File

            // Load certificates
            serverCertificate = new X509Certificate2(File.ReadAllBytes("serverCert.p12"), "alpha");
            Trace.WriteLine("Certificate loaded.");

            // Start the tcpListener to listen on the fixed login server port 12230
            tcpListener = new TcpListener(IPAddress.Any, 12230);
            tcpListener.Start();
            Trace.WriteLine("LoginServer started, waiting for clients...");

            // Main Loop
            while (true)
            {
                // keep this thread idle in loop so other threads can work
                while (!tcpListener.Pending())
                    Thread.Sleep(1000);

                // A NEW CLIENT!!! lets see...
                // Start a new client instance
                Client client = new Client(tcpListener.AcceptTcpClient());

                // Add it to a list of connected Clients!
                ConnectedClients.Add(client);

                try
                {
                    // Accept the client (and encapsulate it inside a try catch thingy in case something goes wrong)
                    client.Accept();
                    
                    // Run it through the awesome new threading thing from the .net 4.0 framework :3
                    Task.Factory.StartNew(
                        () => { client.StartProcessing(); },
                        TaskCreationOptions.LongRunning);
                }
                catch (Exception exception)
                {
                    // output exception
                    Trace.WriteLine("An error occured." + Environment.NewLine + exception.ToString());

                    // remove client from the list
                    ConnectedClients.Remove(client);

                }


                // DONE!
                // wait a while for accepting new clients then start from the beginning
                Thread.Sleep(1000);
            }

            // Probably end things here

        }


        // SQL Connection Settings
        // TODO: Replace them with real things for the sql connection or rewrite the login server to not use an sql database :)
        public static string MySQLConnectionString
        {
            get;
        }

        public static string SettingsQuery
        {
            get;
        }

        // Special Characters for server select
        public static string BigSpace       { get { return "　"; } }
        public static string BigBulled      { get { return "＊"; } }
    }
}
