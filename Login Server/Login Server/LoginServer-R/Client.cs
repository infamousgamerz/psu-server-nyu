﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Diagnostics;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography;
using MySql.Data.MySqlClient;
using System.Data;
using System.Xml.Linq;
using System.Net;

namespace LoginServer_R
{
    class Client
    {
        private TcpClient tcpClient;
        private SslStream sslStream;

        // runtime things
        private string username;
        private uint userID = 0;
        private List<string> MOTD = new List<string>();
        private Dictionary<string, string> Servers = new Dictionary<string, string>();
        private int currentPage = 0;

        public Client(TcpClient tcpClient)
        {
            // TODO: Complete member initialization
            this.tcpClient = tcpClient;
        }

        public void Accept()
        {
            Trace.WriteLine("New client connected.");
            // get the network stream
            sslStream = new SslStream(tcpClient.GetStream(), false);

            // SSL Handshake stuff
            sslStream.AuthenticateAsServer(Program.serverCertificate, false, SslProtocols.Ssl3, false);

            // Send Hello Message
            SendPacket(new Chain(176)
                .Command(new byte[] { 0x02, 0x02, 0x03, 0x00 })
                .StartAt(0x2C).Add(userID)
                .ToPacket());
        }

        public void StartProcessing()
        {
            // do the stuff like listening and processing

            // initialize buffer
            int recievedBytes = -1;
            byte[] recieveBuffer = new byte[0xFFFF];
            byte[] recievedBuffer;
            // start the loop
            try
            {
                while (true)
                {
                    // recieve things
                    recievedBytes = sslStream.Read(recieveBuffer, 0, recieveBuffer.Count());

                    // did the client disconnect itself?
                    if (recievedBytes == 0)
                        break;

                    // copy to temp buffer
                    recievedBuffer = new byte[recievedBytes];
                    Array.Copy(recieveBuffer, recievedBuffer, recievedBytes);

                    // seems to be okay, now...parse things!
                    if (ParsePacket(new Packet(recievedBuffer)))
                        break; // break out if the client is ready to disconnect or so
                }
            }
            catch (Exception exception)
            {
                Trace.WriteLine("An error occured." + Environment.NewLine + exception.ToString());
            }
            finally // Tidy things up
            {
                Trace.WriteLine("Client (" + username + ") disconnected.");

                // close connection :)
                sslStream.Close();

                // at the end always remove this from the list, due to memory leaks :O
                Program.ConnectedClients.Remove(this);
            }
        }

        private void SendPacket(Packet packet)
        {
            sslStream.Write(packet.RawPacket, 0, packet.RawPacket.Count());

        }

        private bool ParsePacket(Packet packet)
        {
            packet.WriteByte(0x00, 0x07); // this is to remove that stupid thing there :v

            switch (packet.ReadUInt(0x04))
            {
                case 0x00021902: // Client Login Request
                    // read username
                    username = packet.ReadASCIIIString(0x2C);
                    Trace.WriteLine("Client tries to log in as " + username);

                    // generate the hash
                    SHA1 sha1 = new SHA1CryptoServiceProvider();
                    byte[] computedHash = sha1.ComputeHash(ASCIIEncoding.ASCII.GetBytes(username.ToLower() + packet.ReadASCIIIString(0x44)));

                    // check with database
                    if (CheckLogin(computedHash))
                    {
                        Trace.WriteLine("Client (" + username + ") logged in successfully.");
                        Send0223(); // Login Okay
                        return false;
                    }
                    else
                    {
                        Trace.WriteLine("Client (" + username + ") login failed.");
                        return true; // Login failed
                    }
                case 0x00022702: // Client accepts MOTD 
                    if (packet.ReadUInt(0x2C) == 1)
                    {
                        Trace.WriteLine("Client (" + username + ") accepted.");
                        Send0216();
                    }
                    else
                    {
                        Trace.WriteLine("Client (" + username + ") did not accept. Kick!");
                        return true;
                    }
                    return false;

                case 0x00023F02: // Client requests MOTD page
                    Trace.WriteLine("Client (" + username + ") requests page " + packet.ReadByte(0x2C).ToString() + ".");
                    Send0225(packet.ReadByte(0x2C));
                    return false;

                case 0x00020E08: // Client version check

                default: // Unknown packet, probably we should log this
                    return false;
            }
        }

        private bool CheckLogin(byte[] hash)
        { 
            bool loginSuccessfull = false;

            // prepare
            using(IDbConnection databaseConnection = new MySqlConnection(Program.MySQLConnectionString))
            using(IDbCommand command= databaseConnection.CreateCommand())
            {
                // connect
                databaseConnection.Open();

                // prepare command                       
                command.CommandText = 
                    "SELECT  `ID_MEMBER` , `ID_GROUP` ,  `memberName` ,  `passwd` ,  `passwordSalt`, `additionalGroups`" +
                    "FROM smf_members " +
                    "WHERE  UPPER(memberName) = '" + username + "'";

                // read data
                using (IDataReader reader = command.ExecuteReader())
                    while (reader.Read())
                    {
                        if (hash.SequenceEqual(ToByteArray((string)reader["passwd"]))
                            && 
                            (
                                (UInt16)reader["ID_GROUP"] ==     1 ||
                                (UInt16)reader["ID_GROUP"] ==     2 ||
                                (UInt16)reader["ID_GROUP"] ==     9 ||
                                (UInt16)reader["ID_GROUP"] ==    10)
                            )
                        {
                            userID = (uint)reader["ID_MEMBER"] + 10000000;
                            loginSuccessfull = true;
                            break;
                        }
                    }
                
                // if login is okay get more things
                if (loginSuccessfull)
                {
                    command.CommandText =
                        "SELECT  `ID_MSG` , `body` " +
                        "FROM smf_messages " +
                        "WHERE `ID_MSG` = 455";

                    using (IDataReader reader = command.ExecuteReader())
                        while (reader.Read())
                        {
                            ParseConfiguration((string)reader["body"]);
                            break;
                        }
                }
            }

            return loginSuccessfull;
        }

        private void ParseConfiguration(string configuration)
        {
            // clean up
            string clean = configuration.Replace(@"<br />", Environment.NewLine).Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;",'"'.ToString());

            XDocument settings = XDocument.Parse(clean);

            // parse it

            // set MOTD
            MOTD.Add(settings.Element("configuration").Element("motd").Value);

            // fill the server list
            foreach (var element in settings.Element("configuration").Element("gameservers").Descendants("server"))
            {
                Servers.Add(element.Value, element.Attribute("name").Value);
            }

            // build MOTD further
            BuildServerList();
        }

        private void BuildServerList()
        {
            foreach (KeyValuePair<string,string> entry in Servers)
            {
                string page = "Available Server:\n";
                foreach (var item in Servers)
                {
                    if (item.Key.Equals(entry.Key))
                        page += Program.BigBulled + item.Value + Environment.NewLine;
                    else
                        page += Program.BigSpace + item.Value + Environment.NewLine;

                }
                MOTD.Add(page);
            }

        }

        public static byte[] ToByteArray(String HexString)
        {
            int NumberChars = HexString.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
            {
                bytes[i / 2] = Convert.ToByte(HexString.Substring(i, 2), 16);
            }
            return bytes;
        }
        public static byte[] RetrieveIP(string input)
        {
            byte[] result = new byte[8];
            string[] temp = input.Split(':');

            // IP
            IPAddress[] hosts = Dns.GetHostEntry(temp[0]).AddressList;
            IPAddress ipAddress = hosts.First();
            Array.Copy(ipAddress.GetAddressBytes(), 0, result, 0, 4);

            // Port
            Array.Copy(BitConverter.GetBytes(uint.Parse(temp[1])), 0, result, 0x04, 0x04);

            return result;
        }

        // Replies
        private void Send0223() // login is okay
        {
            Packet packet = new Packet((uint)64, new byte[] { 0x02, 0x23, 0x03, 0x00 });

            packet.WriteUInt(0x000F0000, 0x1C);
            //packet.WriteUInt(100, 0x20);

            packet.WriteUInt(userID, 0x2C);
            packet.WriteUInt(0x06C5153C, 0x30);

            SendPacket(packet);
        }
        private void Send0225(int page) // MOTD
        {
            // Prepare strings (final size of packet needs to be determined afterwards)
            currentPage = page;
            string motd = MOTD[page];

            int padding = (motd.Length * 2) % 4;

            Packet packet = new Packet((uint)(0x30 + (motd.Length * 2) + 4 - padding), new byte[4] { 0x02, 0x25, 0x03, 0x00 });
            //packet.WriteUInt((uint)0x02360001, 0x2C);
            packet.WriteUInt((uint)MOTD.Count, 0x2C); // count!
            packet.WriteUInt((uint)page, 0x2D);
            //packet.WriteUInt((uint)0x0000FEFF, 0x30);
            packet.WriteUnicodeString(motd, 0x30);

            SendPacket(packet);
        }
        private void Send0216()
        {
            Packet packet = new Packet((uint)52, new byte[] { 0x02, 0x16, 0x03, 0x00 });

            if (currentPage > 0)
                packet.WriteByteArray(RetrieveIP(Servers.ElementAt(currentPage - 1).Key), 0x2C);
            else
                packet.WriteByteArray(RetrieveIP(Servers.First().Key), 0x2C);

            SendPacket(packet);
        }
    }
}
